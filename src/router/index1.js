import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: resolve => require(['@/views/login/index'],resolve),
    hidden: true
  },
  {
    path: '/index',
    component: resolve => require(['@/views/index/index'],resolve),
    // eslint-disable-next-line no-mixed-spaces-and-tabs
    hidden: true
  },
  {
    path: '/404',
    component: resolve => require(['@/views/404'],resolve),
    hidden: true
  },
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: resolve => require(['@/views/redirect/index'],resolve)
      }
    ]
  },
  {
    path: '/rad',
    component: Layout,
    redirect: '/rad/radBasicPair',
    name: 'radBasicPair',
    point: 'RAD',
    meta: { title: '配对信息', icon: 'example' },
    children: [
      {
        path: 'RadBasicPair',
        name: 'RadBasicPair',
        component: resolve => require(['@/views/rad/radBasicPair/DrugPair/index'],resolve),
        meta: { title: '药品配对', icon: 'spot' }
      },
      {
        path: 'Routematch',
        name: 'Routematch',
        component: resolve => require(['@/views/rad/radBasicPair/RouteMatch/index'],resolve),
        meta: { title: '给药途径配对', icon: 'spot' }
      },
      {
        path: 'Diseasematch',
        name: 'Diseasematch',
        component: resolve => require(['@/views/rad/radBasicPair/DiseaseMatch/index'],resolve),
        meta: { title: '疾病配对', icon: 'spot' }
      },
      {
        path: 'Allergenicpair',
        name: 'Allergenicpair',
        component: resolve => require(['@/views/rad/radBasicPair/AllergenicPair/index'],resolve),
        meta: { title: '过敏源配对', icon: 'spot' }
      },
      {
        path: 'FrequencySet',
        name: 'FrequencySet',
        component: resolve => require(['@/views/rad/radBasicPair/FrequencySet/index'],resolve),
        meta: { title: '频次设置', icon: 'spot' }
      }
    ]
  },
  {
    path: '/rad/radCustSetting',
    component: Layout,
    redirect: '/rad/radCustSetting',
    name: 'radCustSetting',
    point: 'RAD',
    meta: { title: '信息维护', icon: 'example' },
    children: [
      {
        path: 'Instruction',
        name: 'Instruction',
        component: resolve => require(['@/views/rad/radCustSetting/Instruction/index'],resolve),
        meta: { title: '说明书', icon: 'spot' }
      },
      {
        path: 'KeyTip',
        name: 'KeyTip',
        component: resolve => require(['@/views/rad/radCustSetting/KeyTip/index'],resolve),
        meta: { title: '重点提示', icon: 'spot' }
      },
      {
        path: 'OuterPack',
        name: 'OuterPack',
        component: resolve => require(['@/views/rad/radCustSetting/OuterPack/index'],resolve),
        meta: { title: '外包装图片', icon: 'spot' }
      },
      {
        path: 'Guidelines',
        name: 'Guidelines',
        component: resolve => require(['@/views/rad/radCustSetting/Guidelines/index'],resolve),
        meta: { title: '用药指导单', icon: 'spot' }
      }
    ]
  },
  {
    path: '/rad/radClassifiedmanage',
    component: Layout,
    redirect: '/rad/radClassifiedmanage',
    name: 'radClassifiedmanage',
    point: 'RAD',
    meta: { title: '分级管理', icon: 'example' },
    children: [
      {
        path: 'AntimicrobialSet',
        name: 'AntimicrobialSet',
        component: resolve => require(['@/views/rad/radClassifiedmanage/AntimicrobialSet/index'],resolve),
        meta: { title: '抗菌药物设置', icon: 'spot' }
      },
      {
        path: 'SectionList',
        name: 'SectionList',
        component:resolve => require(['@/views/rad/radClassifiedmanage/SectionList/index'],resolve),
        meta: { title: '科室列表', icon: 'spot' }
      },
      {
        path: 'DoctorList',
        name: 'DoctorList',
        component: resolve => require(['@/views/rad/radClassifiedmanage/DoctorList/index'],resolve),
        meta: { title: '医生列表', icon: 'spot' }
      },
      {
        path: 'DepartDrugCust',
        name: 'DepartDrugCust',
        component: resolve => require(['@/views/rad/radClassifiedmanage/DepartDrugCust/index'],resolve),
        meta: { title: '科室药品自定义', icon: 'spot' }
      },
      {
        path: 'DoctorMedicineCust',
        name: 'DoctorMedicineCust',
        component:resolve => require(['@/views/rad/radClassifiedmanage/DoctorMedicineCust/index'],resolve),
        meta: { title: '医生药品自定义', icon: 'spot' }
      }
    ]
  },
  {
    path: '/rad/radPerioperative',
    component: Layout,
    redirect: '/rad/radPerioperative',
    point: 'RAD',
    name: 'radPerioperative',
    meta: { title: '围术期', icon: 'example' },
    children: [
      {
        path: 'OperationList',
        name: 'OperationList',
        component: resolve => require(['@/views/rad/radPerioperative/OperationList/index'],resolve),
        meta: { title: '手术列表', icon: 'example' }
      }
    ]
  },
  {
    path: '/rad/radDrugResistanceRate',
    component: Layout,
    redirect: '/rad/radDrugResistanceRate',
    name: 'radDrugResistanceRate',
    point: 'RAD',
    meta: { title: '耐药率设置', icon: 'example' },
    children: [
      {
        path: 'ResistanceRateList',
        name: 'ResistanceRateList',
        component: resolve => require(['@/views/rad/radDrugResistanceRate/ResistanceRateList/index'],resolve),
        meta: { title: '耐药率列表', icon: 'example' }
      }
    ]
  },
  {
    path: '/rad/radReviewManage',
    component: Layout,
    redirect: '/rad/radReviewManage',
    name: 'radReviewManage',
    point: 'RAD',
    meta: { title: '审查管理', icon: 'example' },
    children: [
      {
        path: 'ReviewFilter',
        name: 'ReviewFilter',
        component: resolve => require(['@/views/rad/radReviewManage/ReviewFilter/index'],resolve),
        meta: { title: '审查滤镜', icon: 'spot' }
      },
      {
        path: 'ResultsReview',
        name: 'ResultsReview',
        component: resolve => require(['@/views/rad/radReviewManage/ResultsReview/index'],resolve),
        meta: { title: '审查结果列表', icon: 'spot' }
      }
    ]
  },
  {
    path: '/rad/radPersonalizedCustom',
    component: Layout,
    redirect: '/rad/radPersonalizedCustom',
    name: 'radPersonalizedCustom',
    point: 'RAD',
    meta: { title: '个性化自定义', icon: 'example' },
    children: [
      {
        path: 'GeneralCust',
        name: 'GeneralCust',
        component: resolve => require(['@/views/rad/radPersonalizedCustom/GeneralCust/index'],resolve),
        meta: { title: '通用自定义', icon: 'spot' }
      },
      {
        path: 'MultiConditionalCust',
        name: 'MultiConditionalCust',
        component:resolve => require(['@/views/rad/radPersonalizedCustom/MultiConditionalCust/index'],resolve),
        meta: { title: '多条件自定义', icon: 'spot' }
      },
      {
        path: 'PerioperativeDrug',
        name: 'PerioperativeDrug',
        component:resolve => require(['@/views/rad/radPersonalizedCustom/PerioperativeDrug/index'],resolve),
        meta: { title: '围术期用药', icon: 'spot' }
      }
    ]
  },
  {
    path: '/rad/radParameterSetting',
    component: Layout,
    redirect: '/rad/radParameterSetting',
    name: 'radParameterSetting',
    point: 'RAD',
    meta: { title: '参数设置', icon: 'example' },
    children: [
      {
        path: 'ReviewParameterSet',
        name: 'ReviewParameterSet',
        component: resolve => require(['@/views/rad/radParameterSetting/ReviewParameterSet/index'],resolve),
        meta: { title: '审查参数设置', icon: 'example' }
      }
    ]
  },
  {
    path: '/rad/radReviewPair',
    component: Layout,
    redirect: '/rad/radReviewPair',
    name: 'radReviewPair',
    point: 'RAD',
    meta: { title: '审查配对', icon: 'example' },
    children: [
      {
        path: 'SafeDrugMatch',
        name: 'SafeDrugMatch',
        component: resolve => require(['@/views/rad/radReviewPair/SafeDrugMatch/index'],resolve),
        meta: { title: '安全用药同步配对', icon: 'example' }
      }
    ]
  },
  {
    path: '/rad/radSystemData',
    component: Layout,
    redirect: '/rad/radSystemData',
    name: 'radReviewPair',
    point: 'RAD',
    meta: { title: '系统数据', icon: 'example' },
    children: [
      {
        path: 'DrugDictionary',
        name: 'DrugDictionary',
        component: resolve => require(['@/views/rad/radSystemData/DrugDictionary'],resolve),
        meta: { title: '药品字典', icon: 'example' }
      },
      {
        path: 'RouteDictionary',
        name: 'RouteDictionary',
        component: resolve => require(['@/views/rad/radSystemData/RouteDictionary'],resolve),
        meta: { title: '给药途径字典', icon: 'example' }
      },
      {
        path: 'DiagnoDictionary',
        name: 'DiagnoDictionary',
        component: resolve => require(['@/views/rad/radSystemData/DiagnoDictionary'],resolve),
        meta: { title: '诊断字典', icon: 'example' }
      },
      {
        path: 'AllerDictionary',
        name: 'AllerDictionary',
        component: resolve => require(['@/views/rad/radSystemData/AllerDictionary'],resolve),
        meta: { title: '过敏源字典', icon: 'example' }
      },
      {
        path: 'SurgeryDictionary',
        name: 'SurgeryDictionary',
        component: resolve => require(['@/views/rad/radSystemData/SurgeryDictionary'],resolve),
        meta: { title: '手术字典', icon: 'example' }
      },
      {
        path: 'ApplyProject',
        name: 'ApplyProject',
        component: resolve => require(['@/views/rad/radSystemData/ApplyProject'],resolve),
        meta: { title: '检验申请项目', icon: 'example' }
      },
      {
        path: 'ResultProject',
        name: 'ResultProject',
        component: resolve => require(['@/views/rad/radSystemData/ResultProject'],resolve),
        meta: { title: '检验结果项目', icon: 'example' }
      },
      {
        path: 'CheckApplyProject',
        name: 'CheckApplyProject',
        component: resolve => require(['@/views/rad/radSystemData/CheckApplyProject'],resolve),
        meta: { title: '检查申请项目', icon: 'example' }
      },
      {
        path: 'CheckResultProject',
        name: 'CheckResultProject',
        component: resolve => require(['@/views/rad/radSystemData/CheckResultProject'],resolve),
        meta: { title: '检查结果项目', icon: 'example' }
      }
    ]
  },
  // 点评中心 信息查看
  {
    path: '/prc/prcinformationsviewer',
    component: Layout,
    redirect: '/prc/prcinformationsviewer/outpatientcases',
    point: 'PRC',
    name: 'PrcInformationsViewer',
    meta: { title: '信息查看', icon: 'example' },
    children: [
      {
        path: 'outpatientcases',
        name: 'OutpatientCases',
        component: resolve => require(['@/views/prc/prcInformationsViewer/outpatientCases/OutpatientCases'],resolve),
        meta: { title: '门诊病历', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'emergencycases',
        name: 'EmergencyCases',
        component:resolve => require(['@/views/prc/prcInformationsViewer/emergencyCases/EmergencyCases'],resolve),
        meta: { title: '急症病历', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'incourtcases',
        name: 'IncourtCases',
        component: resolve => require(['@/views/prc/prcInformationsViewer/incourtCases/IncourtCases'],resolve),
        meta: { title: '在院病历', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'thehospitalcases',
        name: 'TheHospitalCases',
        component:resolve => require(['@/views/prc/prcInformationsViewer/theHospitalCases/TheHospitalCases'],resolve),
        meta: { title: '出院病历', icon: 'spot', background: '#e6edf5' }
      }
    ]
  },
  // 点评中心 门诊点评
  {
    path: '/prc/prcoutpatientreviews',
    component: Layout,
    redirect: '/prc/prcoutpatientreviews/outpatientreview',
    point: 'PRC',
    name: 'PrcOutpatientReviews',
    meta: { title: '门诊点评', icon: 'example' },
    children: [
      {
        path: 'outpatientreview',
        name: 'OutpatientReview',
        component: resolve => require(['@/views/prc/prcOutpatientReviews/outpatientReview/OutpatientReview'],resolve),
        meta: { title: '门诊病人点评', icon: 'spot', background: '#e6edf5' }
      },

      {
        path: 'prescription',
        name: 'Prescription',
        component:resolve => require(['@/views/prc/prcOutpatientReviews/prescription/Prescription'],resolve),
        meta: { title: '门诊处方点评', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'antibacterial ',
        name: 'Antibacterial ',
        component: resolve => require(['@/views/prc/prcOutpatientReviews/antibacterial/Antibacterial'],resolve),
        meta: {
          title: '门诊抗菌药物处方点评',
          icon: 'spot',
          background: '#e6edf5'
        }
      }
    ]
  },
  // 点评中心 住院点评
  {
    path: '/prc/prchospitalreviews',
    component: Layout,
    redirect: '/prc/prchospitalreviews/antibioticpatientreview',
    point: 'PRC',
    name: 'PrcHospitalReviews',
    meta: { title: '住院点评', icon: 'example' },
    children: [
      {
        path: 'antibioticpatientreview',
        name: 'AntibioticPatientReview',
        component:resolve => require(['@/views/prc/prcHospitalReviews/antibioticPatientReview/AntibioticPatientReview'],resolve),
        meta: {
          title: '住院抗菌药物病人点评',
          icon: 'spot',
          background: '#e6edf5'
        }
      },
      {
        path: 'inpatientreview',
        name: 'InpatientReview',
        component:resolve => require(['@/views/prc/prcHospitalReviews/inpatientReview/InpatientReview'],resolve),
        meta: { title: '住院病人点评', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'perioperativeperiodreview',
        name: 'PerioperativePeriodReview ',
        component:resolve => require(['@/views/prc/prcHospitalReviews/perioperativePeriodReview/PerioperativePeriodReview'],resolve),
        meta: { title: '围术期用药点评', icon: 'spot', background: '#e6edf5' }
      }
    ]
  },
  // 点评中心 电子药历
  {
    path: '/prc/prcelectronicmedicine',
    component: Layout,
    redirect: '/prc/prcelectronicmedicine/electronicmedicine',
    point: 'PRC',
    name: 'prcElectronicMedicine',
    meta: { title: '电子药历', icon: 'example' },
    children: [
      {
        path: 'electronicmedicine',
        name: 'ElectronicMedicine',
        component:resolve => require(['@/views/prc/prcElectronicMedicine/ElectronicMedicine/'],resolve),
        meta: { title: '电子药历', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  // 点评中心 数据中心
  {
    path: '/prc/prcDataCenter',
    component: Layout,
    redirect: '/prc/prcDataCenter/rationalityIndex',
    point: 'PRC',
    name: 'RrcDataCenter',
    meta: { title: '数据中心', icon: 'example' },
    children: [
      {
        path: 'antimicrobialStatistics',
        name: 'AntimicrobialStatistics',
        component: resolve => require(['@/views/prc/prcDataCenter/antimicrobialStatistics/AntimicrobialStatistics'],resolve),
        meta: { title: '抗菌药物统计', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'drugUseRanking',
        name: 'DrugUseRanking',
        component:resolve => require(['@/views/prc/prcDataCenter/drugUseRanking/DrugUseRanking'],resolve),
        meta: { title: '药品使用排名', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'otherRelated',
        name: 'OtherRelated',
        component:resolve => require(['@/views/prc/prcDataCenter/otherRelated/OtherRelated'],resolve),
        meta: { title: '其他相关', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'RationalityIndex',
        name: 'RationalityIndex',
        component:resolve => require(['@/views/prc/prcDataCenter/rationalityIndex/RationalityIndex'],resolve),
        meta: { title: '合理性指标', icon: 'spot', background: '#e6edf5' }
      }
    ]
  },

  // 点评中心 考核指标
  {
    path: '/prc/prcevaluation',
    component: Layout,
    redirect: '/prc/prcevaluation/departmentsetting',
    point: 'PRC',
    name: 'prcEvaluation',
    meta: { title: '考核指标', icon: 'example' },
    children: [
      {
        path: 'departmentsetting',
        name: 'DepartmentSetting',
        component: resolve => require(['@/views/prc/prcEvaluation/department/DepartmentSetting'],resolve),
        meta: { title: '科室目标值设置', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'doctorsetting',
        name: 'DoctorSetting',
        component: resolve => require(['@/views/prc/prcEvaluation/doctor/DoctorSetting'],resolve),
        meta: { title: '医生目标值设置', icon: 'spot', background: '#e6edf5' }
      }
    ]
  },
  // 点评中心 数据维护
  {
    path: '/prc/prcdatamaintenance',
    component: Layout,
    redirect: '/prc/prcdatamaintenance/commentstemplate',
    point: 'PRC',
    name: 'prcEvaluation',
    meta: { title: '数据维护', icon: 'example' },
    children: [
      {
        path: 'commentstemplate',
        name: 'CommentsTemplate',
        component:resolve => require(['@/views/prc/prcDataMaintenance/commentsTemplate/CommentsTemplate'],resolve),
        meta: { title: '点评模板', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'censorshiprules',
        name: 'CensorshipRules',
        component:resolve => require(['@/views/prc/prcDataMaintenance/censorshipRules/CensorshipRules'],resolve),
        meta: { title: '审查规则', icon: 'spot', background: '#e6edf5' }
      }
    ]
  },

  // 临床药师工作站
  {
    path: '/cpw/index',
    component: Layout,
    redirect: '/cpw/index/index',
    point: 'CPW',
    name: 'Index',
    meta: { title: '首页', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'index',
        name: 'Index',
        component: resolve => require(['@/views/cpw/index/index'],resolve),
        meta: { title: '首页', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/cpw/guardianshiplist',
    component: Layout,
    redirect: '/cpw/guardianshiplist/guardianshiplist',
    point: 'CPW',
    name: 'GuardianshipList',
    meta: { title: '监护列表', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'guardianshiplist',
        name: 'GuardianshipList',
        component: resolve => require(['@/views/cpw/guardianshipList/GuardianshipList'],resolve),
        meta: { title: '监护列表', icon: 'example', background: '#e6edf5' }
      },
      {
        path: 'patientdetail',
        name: 'PatientDetail',
        component: resolve => require(['@/views/cpw/guardianshipList/patientDetail/patientDetail'],resolve),
        hidden: true,
        meta: { title: '病人详细信息', icon: 'example', background: '#e6edf5' }
      }

    ]
  },
  // {
  //   path: '/cpw/patientrounds',
  //   component: Layout,
  //   redirect: '/cpw/patientrounds/patientinfo',
  //   point: 'CPW',
  //   name: 'Patientrounds',
  //   meta: { title: '病人查房', icon: 'example' },
  //   children: [
  //     {
  //       path: 'patientinfo',
  //       name: 'PatientInfo',
  //       component: resolve => require(['@/views/cpw/patientrounds/PatientInfo'],resolve),
  //       meta: {
  //         title: '患者信息正式查看',
  //         icon: 'example',
  //         background: '#e6edf5'
  //       }
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/patientviews',
  //   component: Layout,
  //   redirect: '/cpw/patientviews/patientviews',
  //   point: 'CPW',
  //   name: 'Patientrounds',
  //   meta: { title: '患者360视图', icon: 'example' },
  //   children: [
  //     {
  //       path: 'patientviews',
  //       name: 'PatientViews',
  //       component: resolve => require(['@/views/cpw/patientViews/PatientViews'],resolve),
  //       meta: { title: '患者全部信息', icon: 'example', background: '#e6edf5' }
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/pharmaceuticalEvaluate',
  //   component: Layout,
  //   redirect: '/cpw/pharmaceuticalEvaluate/commonevaluate',
  //   point: 'CPW',
  //   name: 'PharmaceuticalEvaluate',
  //   meta: { title: '药学评估', icon: 'example' },
  //   children: [
  //     {
  //       path: 'commonevaluate',
  //       name: 'CommonEvaluate',
  //       component:resolve => require(['@/views/cpw/pharmaceuticalEvaluate/commonEvaluate/CommonEvaluate'],resolve),
  //       meta: { title: '通用评估', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'specialistevaluate',
  //       name: 'SpecialistEvaluate',
  //       component:resolve => require(['@/views/cpw/pharmaceuticalEvaluate/specialistEvaluate/SpecialistEvaluate'],resolve),
  //       meta: { title: '专科评估', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'assessmentscale',
  //       name: 'AssessmentScale',
  //       component:resolve => require(['@/views/cpw/pharmaceuticalEvaluate/assessmentScale/AssessmentScale'],resolve),
  //       meta: { title: '评估量表自定义', icon: 'spot', background: '#e6edf5' }
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/drugintervention',
  //   component: Layout,
  //   redirect: '/cpw/drugintervention/consultingaudit',
  //   point: 'CPW',
  //   name: 'DrugIntervention',
  //   meta: { title: '用药干预', icon: 'example' },
  //   children: [
  //     {
  //       path: 'consultation',
  //       name: 'Consultation',
  //       component:resolve => require(['@/views/cpw/drugIntervention/consultation/Consultation'],resolve),
  //       meta: { title: '用药咨询', icon: 'example' }
  //     },
  //     {
  //       path: 'consultingaudit',
  //       name: 'ConsultingAudit',
  //       redirect: '/cpw/drugintervention/consultingaudit/approvalconsultation',
  //       component: resolve => require(['@/views/cpw/drugIntervention/consultingAudit/index'],resolve),
  //       children: [
  //         {
  //           path: 'approvalconsultation',
  //           name: 'ApprovalConsultation',
  //           component:resolve => require(['@/views/cpw/drugIntervention/consultingAudit/approvalConsultation/ApprovalConsultation'],resolve),
  //           meta: { title: '咨询问答审核', icon: 'spot', background: '#e6edf5' }
  //         }
  //       ],
  //       meta: { title: '咨询审核', icon: 'example' }
  //     },
  //     {
  //       path: 'doctorConsult',
  //       name: 'DoctorConsult',
  //       meta: { title: '医生咨询', icon: 'example' },
  //       component: resolve => require(['@/views/cpw/drugIntervention/doctorConsult/index'],resolve),
  //       children: [
  //         {
  //           path: 'specialConsultation',
  //           name: 'SpecialConsultation',
  //           component:resolve => require(['@/views/cpw/drugIntervention/doctorConsult/specialConsultation/SpecialConsultation'],resolve),
  //           meta: { title: '专项咨询', icon: 'spot', background: '#e6edf5' }
  //         },
  //         {
  //           path: 'commonConsultation',
  //           name: 'CommonConsultation',
  //           component: resolve => require(['@/views/cpw/drugIntervention/doctorConsult/commonConsultation/CommonConsultation'],resolve),
  //           meta: { title: '通用咨询', icon: 'spot', background: '#e6edf5' }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'pharmacistReply',
  //       name: 'PharmacistReply',
  //       meta: { title: '药师回复', icon: 'example' },
  //       component: resolve => require(['@/views/cpw/drugIntervention/pharmacistReply/index'],resolve),
  //       children: [
  //         {
  //           path: 'specialReply',
  //           name: 'SpecialReply',
  //           component:resolve => require(['@/views/cpw/drugIntervention/pharmacistReply/specialReply/SpecialReply'],resolve),
  //           meta: { title: '专项回复', icon: 'spot', background: '#e6edf5' }
  //         },
  //         {
  //           path: 'commonReply',
  //           name: 'CommonReply',
  //           component:resolve => require(['@/views/cpw/drugIntervention/pharmacistReply/commonReply/CommonReply'],resolve),
  //           meta: { title: '通用回复', icon: 'spot', background: '#e6edf5' }
  //         }
  //       ]
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/medicationeducation',
  //   component: Layout,
  //   redirect: '/cpw/medicationeducation/medicationeducation',
  //   point: 'CPW',
  //   name: 'MedicationEducations',
  //   meta: { title: '用药教育', icon: 'example' },
  //   children: [
  //     {
  //       path: 'medicationeducation',
  //       name: 'MedicationEducation',
  //       component:resolve => require(['@/views/cpw/medicationEducation/MedicationEducation'],resolve),
  //       meta: { title: '用药教育', icon: 'spot', background: '#e6edf5' }
  //     }
  //   ]
  // },
  // // 药师工作站 知识管理
  // {
  //   path: '/cpw/knowledgemanagement',
  //   component: Layout,
  //   redirect: '/cpw/knowledgemanagement/publicknowledge',
  //   point: 'CPW',
  //   name: 'KnowledgeManagement',
  //   meta: { title: '知识管理', icon: 'example' },
  //   children: [
  //     {
  //       path: 'publicKnowledge',
  //       name: 'PublicKnowledge',
  //       component:resolve => require(['@/views/cpw/knowledgeManagement/publicKnowledge/PublicKnowledge'],resolve),
  //       meta: { title: '共有知识', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'privateknowledge',
  //       name: 'PrivateKnowledge',
  //       component:resolve => require(['@/views/cpw/knowledgeManagement/privateKnowledge/PrivateKnowledge'],resolve),
  //       meta: { title: '私有知识', icon: 'spot', background: '#e6edf5' }
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/electronicmedicine',
  //   component: Layout,
  //   redirect: '/cpw/electronicmedicine/medicinetemplate',
  //   point: 'CPW',
  //   name: 'ElectronicMedicine',
  //   meta: { title: '电子药历', icon: 'example' },
  //   children: [
  //     {
  //       path: 'medicinetemplate',
  //       name: 'MedicineTemplate',
  //       component:resolve => require(['@/views/cpw/electronicMedicine/MedicineTemplate'],resolve),
  //       meta: { title: '药历模板', icon: 'example', background: '#e6edf5' }
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/pharmaceuticalcare',
  //   component: Layout,
  //   redirect: '/cpw/pharmaceuticalcare/monitoringplan',
  //   point: 'CPW',
  //   name: 'PharmaceuticalCare',
  //   meta: { title: '药学监护', icon: 'example' },
  //   children: [
  //     {
  //       path: 'monitoringplan',
  //       name: 'MonitoringPlan',
  //       component: resolve => require(['@/views/cpw/pharmaceuticalCare/monitoringPlan/MonitoringPlan'],resolve),
  //       meta: { title: '监护计划', icon: 'example', background: '#e6edf5' }
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/useraction',
  //   component: Layout,
  //   redirect: '/cpw/useraction/usermanagement',
  //   point: 'CPW',
  //   name: 'UserAction',
  //   meta: { title: '用户操作', icon: 'example' },
  //   children: [
  //     {
  //       path: 'datatestpage',
  //       name: 'DataTestPage',
  //       component: resolve => require(['@/views/cpw/userAction/dataTestPage/DataTestPage'],resolve),
  //       meta: { title: '用户测试页面', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'datamaintenance',
  //       name: 'DataMaintenance',
  //       component:resolve => require(['@/views/cpw/userAction/dataMaintenance/DataMaintenance'],resolve),
  //       meta: { title: '数据维护', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'usermanagement',
  //       name: 'UserManagement',
  //       component: resolve => require(['@/views/cpw/userAction/userManagement/UserManagement'],resolve),
  //       meta: { title: '用户管理', icon: 'spot', background: '#e6edf5' }
  //     }
  //   ]
  // },

  {
    path: '/ptc/outpatient',
    component: Layout,
    redirect: '/ptc/outpatient/check',
    point: 'PTC',
    name: 'Outpatient',
    meta: { title: '门诊审方', icon: 'example' },
    children: [
      {
        path: 'check',
        name: 'Check',
        component: resolve => require(['@/views/ptc/outpatient/check'],resolve),
        meta: { title: '门诊监测', icon: 'spot' }
      },
      {
        path: 'query',
        name: 'Query',
        component: resolve => require(['@/views/ptc/outpatient/query'],resolve),
        meta: { title: '门诊查询', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'department',
        name: 'Department',
        component: resolve => require(['@/views/ptc/outpatient/department'],resolve),
        meta: { title: '门诊科室', icon: 'spot', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/ptc/inhospital',
    component: Layout,
    redirect: '/ptc/inhospital/survey',
    point: 'PTC',
    name: 'Inhospital',
    meta: { title: '住院审方', icon: 'example' },
    children: [
      {
        path: 'survey',
        name: 'Survey',
        component: resolve => require(['@/views/ptc/inhospital/survey'],resolve),
        meta: { title: '住院监测', icon: 'spot' }
      },
      {
        path: 'query',
        name: 'Query',
        component: resolve => require(['@/views/ptc/inhospital/query'],resolve),
        meta: { title: '住院查询', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'department',
        name: 'Department',
        component: resolve => require(['@/views/ptc/inhospital/department'],resolve),
        meta: { title: '住院科室', icon: 'spot', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/ptc/parameter',
    component: Layout,
    redirect: '/ptc/parameter/outsystem',
    point: 'PTC',
    name: 'Parameter',
    meta: { title: '审方参数设置', icon: 'example' },
    children: [
      {
        path: 'outsystem',
        name: 'Outsystem',
        component: resolve => require(['@/views/ptc/parameter/outsystem'],resolve),
        meta: { title: '门诊系统设置', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'outsurvey',
        name: 'Outsurvey',
        component: resolve => require(['@/views/ptc/parameter/outsurvey'],resolve),
        meta: { title: '门诊监测标准', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'hospitalsystem',
        name: 'Hospitalsystem',
        component: resolve => require(['@/views/ptc/parameter/hospitalsystem'],resolve),
        meta: { title: '住院系统设置', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'hospitalsurvey',
        name: 'Hospitalsurvey',
        component: resolve => require(['@/views/ptc/parameter/hospitalsurvey'],resolve),
        meta: { title: '住院监测标准', icon: 'spot', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/ptc/report',
    component: Layout,
    redirect: '/ptc/report/today',
    point: 'PTC',
    name: 'Report',
    meta: { title: '审方报表', icon: 'example' },
    children: [
      {
        path: 'today',
        name: 'Today',
        component: resolve => require(['@/views/ptc/report/today'],resolve),
        meta: { title: '今日动态监测', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'gy',
        name: 'Gy',
        component: resolve => require(['@/views/ptc/report/gy'],resolve),
        meta: { title: '干预情况汇总表', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'pass',
        name: 'Pass',
        component: resolve => require(['@/views/ptc/report/pass'],resolve),
        meta: {
          title: '处方通过状态统计表',
          icon: 'spot',
          background: '#e6edf5'
        }
      }
    ]
  },
  {
    path: '/kb/instructions',
    component: Layout,
    redirect: '/kb/instructions/instructions',
    point: 'KB',
    name: 'Instructions',
    meta: { title: '药品说明书', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'instructions',
        name: 'Instructions',
        component: resolve => require(['@/views/kb/instructions/instructions'],resolve),
        meta: { title: '药品说明书', icon: 'example', background: '#e6edf5' }
      },
      {
        path: 'drug',
        name: 'Drug',
        component: resolve => require(['@/views/kb/drug/drug'],resolve),
        hidden: true,
        meta: { title: '药品厂家说明', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/kb/instructions',
    component: Layout,
    redirect: '/kb/instructions/instructions',
    point: 'KB',
    name: 'Instructions',
    meta: { title: '药品教育', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'education',
        name: 'Education',
        component: resolve => require(['@/views/kb/education/education'],resolve),
        meta: { title: '药品教育', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/kb/chineserecip',
    component: Layout,
    redirect: '/kb/chineserecip/chineserecip',
    point: 'KB',
    name: 'Chineserecip',
    meta: { title: '中药材', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'chineserecip',
        name: 'Chineserecip',
        component: resolve => require(['@/views/kb/chineserecip/chineserecip'],resolve),
        meta: { title: '中药材', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/kb/formula',
    component: Layout,
    redirect: '/kb/formula/formula',
    point: 'KB',
    name: 'Formula',
    meta: { title: '用药公式', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'formula',
        name: 'Formula',
        component: resolve => require(['@/views/kb/formula/formula'],resolve),
        meta: { title: '用药公式', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/kb/chinesemedi',
    component: Layout,
    redirect: '/kb/chinesemedi/chinesemedi',
    point: 'KB',
    name: 'Chinesemedi',
    meta: { title: '中药方剂', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'chinesemedi',
        name: 'Chinesemedi',
        component: resolve => require(['@/views/kb/chinesemedi/chinesemedi'],resolve),
        meta: { title: '中药方剂', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/kb/guide',
    component: Layout,
    redirect: '/kb/guide/guide',
    point: 'KB',
    name: 'Guide',
    meta: { title: '临床指南', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'guide',
        name: 'Guide',
        component: resolve => require(['@/views/kb/guide/guide'],resolve),
        meta: { title: '临床指南', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/kb/druginfo',
    point: 'KB',
    name: 'druginfo',
    hidden: true,
    component: resolve => require(['@/views/kb/druginfo/druginfo'],resolve),
    meta: { title: '药品', icon: 'example' }
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  })

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
