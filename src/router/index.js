import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/regist',
    component: () => import('@/views/regist/index'),
    hidden: true
  },
  {
    path: '/index',
    component: () => import('@/views/index/index'),
    // eslint-disable-next-line no-mixed-spaces-and-tabs
    hidden: true
  },
  {
    path: '/explainpage',
    component: () => import('@/views/rad/radSystemRules/ConfirmRule/Instructions'),
    // eslint-disable-next-line no-mixed-spaces-and-tabs
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/rad',
    component: Layout,
    redirect: '/rad/radBasicPair',
    name: 'radBasicPair',
    point: 'RAD',
    meta: { title: '配对信息', icon: 'example' },
    children: [
      {
        path: 'RadBasicPair',
        name: 'RadBasicPair',
        component: () => import('@/views/rad/radBasicPair/DrugPair/index'),
        meta: { title: '药品配对', icon: 'spot' }
      },
      {
        path: 'Routematch',
        name: 'Routematch',
        component: () => import('@/views/rad/radBasicPair/RouteMatch/index'),
        meta: { title: '给药途径配对', icon: 'spot' }
      },
      {
        path: 'Diseasematch',
        name: 'Diseasematch',
        component: () => import('@/views/rad/radBasicPair/DiseaseMatch/index'),
        meta: { title: '疾病配对', icon: 'spot' }
      },
      {
        path: 'Allergenicpair',
        name: 'Allergenicpair',
        component: () =>
          import('@/views/rad/radBasicPair/AllergenicPair/index'),
        meta: { title: '过敏源配对', icon: 'spot' }
      },
      {
        path: 'FrequencySet',
        name: 'FrequencySet',
        component: () => import('@/views/rad/radBasicPair/FrequencySet/index'),
        meta: { title: '频次设置', icon: 'spot' }
      }
    ]
  },
  // {
  //   path: '/rad/radCustSetting',
  //   component: Layout,
  //   redirect: '/rad/radCustSetting',
  //   name: 'radCustSetting',
  //   point: 'RAD',
  //   meta: { title: '信息维护', icon: 'example' },
  //   children: [
  //     {
  //       path: 'Instruction',
  //       name: 'Instruction',
  //       component: () => import('@/views/rad/radCustSetting/Instruction/index'),
  //       meta: { title: '说明书', icon: 'spot' }
  //     },
  //     {
  //       path: 'KeyTip',
  //       name: 'KeyTip',
  //       component: () => import('@/views/rad/radCustSetting/KeyTip/index'),
  //       meta: { title: '重点提示', icon: 'spot' }
  //     }
  //   ]
  // },
  // {
  //   path: '/rad/radClassifiedmanage',
  //   component: Layout,
  //   redirect: '/rad/radClassifiedmanage',
  //   name: 'radClassifiedmanage',
  //   point: 'RAD',
  //   meta: { title: '分级管理', icon: 'example' },
  //   children: [
  //     {
  //       path: 'AntimicrobialSet',
  //       name: 'AntimicrobialSet',
  //       component: () =>
  //         import('@/views/rad/radClassifiedmanage/AntimicrobialSet/index'),
  //       meta: { title: '抗菌药物设置', icon: 'spot' }
  //     },
  //     {
  //       path: 'SectionList',
  //       name: 'SectionList',
  //       component: () =>
  //         import('@/views/rad/radClassifiedmanage/SectionList/index'),
  //       meta: { title: '科室列表', icon: 'spot' }
  //     },
  //     {
  //       path: 'DoctorList',
  //       name: 'DoctorList',
  //       component: () =>
  //         import('@/views/rad/radClassifiedmanage/DoctorList/index'),
  //       meta: { title: '医生列表', icon: 'spot' }
  //     },
  //     {
  //       path: 'DepartDrugCust',
  //       name: 'DepartDrugCust',
  //       component: () =>
  //         import('@/views/rad/radClassifiedmanage/DepartDrugCust/index'),
  //       meta: { title: '科室药品自定义', icon: 'spot' }
  //     },
  //     {
  //       path: 'DoctorMedicineCust',
  //       name: 'DoctorMedicineCust',
  //       component: () =>
  //         import('@/views/rad/radClassifiedmanage/DoctorMedicineCust/index'),
  //       meta: { title: '医生药品自定义', icon: 'spot' }
  //     },
  //     {
  //       path: 'OperationList',
  //       name: 'OperationList',
  //       component: () =>
  //         import('@/views/rad/radClassifiedmanage/OperationList/index'),
  //       meta: { title: '手术列表', icon: 'spot' }
  //     },
  //     {
  //       path: 'ResistanceRateList',
  //       name: 'ResistanceRateList',
  //       component: () =>
  //         import('@/views/rad/radClassifiedmanage/ResistanceRateList/index'),
  //       meta: { title: '耐药率列表', icon: 'spot' }
  //     }
  //   ]
  // },

  // {
  //   path: '/rad/radReviewManage',
  //   component: Layout,
  //   redirect: '/rad/radReviewManage',
  //   name: 'radReviewManage',
  //   point: 'RAD',
  //   meta: { title: '审查管理', icon: 'example' },
  //   children: [
  //     {
  //       path: 'ReviewFilter',
  //       name: 'ReviewFilter',
  //       component: () =>
  //         import('@/views/rad/radReviewManage/ReviewFilter/index'),
  //       meta: { title: '审查滤镜', icon: 'spot' }
  //     },
  //     {
  //       path: 'ResultsReview',
  //       name: 'ResultsReview',
  //       component: () =>
  //         import('@/views/rad/radReviewManage/ResultsReview/index'),
  //       meta: { title: '审查结果列表', icon: 'spot' }
  //     }
  //   ]
  // },
  // {
  //   path: '/rad/radPersonalizedCustom',
  //   component: Layout,
  //   redirect: '/rad/radPersonalizedCustom',
  //   name: 'radPersonalizedCustom',
  //   point: 'RAD',
  //   meta: { title: '自定义规则', icon: 'example' },
  //   children: [
  //     {
  //       path: 'GeneralCust',
  //       name: 'GeneralCust',
  //       component: () =>
  //         import('@/views/rad/radPersonalizedCustom/GeneralCust/index'),
  //       meta: { title: '通用自定义', icon: 'spot' }
  //     },
  //     {
  //       path: 'MultiConditionalCust',
  //       name: 'MultiConditionalCust',
  //       component: () =>
  //         import('@/views/rad/radPersonalizedCustom/MultiConditionalCust/index'),
  //       meta: { title: '多条件自定义', icon: 'spot' }
  //     },
  //     {
  //       path: 'PerioperativeDrug',
  //       name: 'PerioperativeDrug',
  //       component: () =>
  //         import('@/views/rad/radPersonalizedCustom/PerioperativeDrug/index'),
  //       meta: { title: '围术期用药', icon: 'spot' }
  //     }
  //   ]
  // },
  {
    path: '/rad/radSystemInfo',
    component: Layout,
    redirect: '/rad/radSystemInfo',
    name: 'radSystemInfo',
    point: 'RAD',
    meta: { title: '系统信息', icon: 'example' },
    children: [
      {
        path: 'OuterPack',
        name: 'OuterPack',
        component: () =>
          import('@/views/rad/radSystemInfo/OuterPack/index'),
        meta: { title: '外包装图片', icon: 'spot' }
      },
      {
        path: 'Guidelines',
        name: 'Guidelines',
        component: () =>
          import('@/views/rad/radSystemInfo/Guidelines/index'),
        meta: { title: '用药指导单', icon: 'spot' }
      },
      {
        path: 'Instruction',
        name: 'Instruction',
        component: () =>
          import('@/views/rad/radSystemInfo/Instruction/index'),
        meta: { title: '说明书', icon: 'spot' }
      },
      {
        path: 'KeyTip',
        name: 'KeyTip',
        component: () =>
          import('@/views/rad/radSystemInfo/KeyTip/index'),
        meta: {
          title: '重要提示', icon: 'spot'
        }
      }
    ]
  },
  {
    path: '/rad/radSystemData',
    component: Layout,
    redirect: '/rad/radSystemData',
    name: 'radReviewPair',
    point: 'RAD',
    meta: { title: '系统字典', icon: 'example' },
    children: [
      {
        path: 'DrugDictionary',
        name: 'DrugDictionary',
        component: () => import('@/views/rad/radSystemData/DrugDictionary'),
        meta: { title: '药品字典', icon: 'example' }
      },
      {
        path: 'RouteDictionary',
        name: 'RouteDictionary',
        component: () => import('@/views/rad/radSystemData/RouteDictionary'),
        meta: { title: '给药途径字典', icon: 'example' }
      },
      {
        path: 'DiagnoDictionary',
        name: 'DiagnoDictionary',
        component: () => import('@/views/rad/radSystemData/DiagnoDictionary'),
        meta: { title: '诊断字典', icon: 'example' }
      },
      {
        path: 'AllerDictionary',
        name: 'AllerDictionary',
        component: () => import('@/views/rad/radSystemData/AllerDictionary'),
        meta: { title: '过敏源字典', icon: 'example' }
      },
      {
        path: 'SurgeryDictionary',
        name: 'SurgeryDictionary',
        component: () => import('@/views/rad/radSystemData/SurgeryDictionary'),
        meta: { title: '手术字典', icon: 'example' }
      },
      {
        path: 'ApplyProject',
        name: 'ApplyProject',
        component: () => import('@/views/rad/radSystemData/ApplyProject'),
        meta: { title: '检验申请项目', icon: 'example' }
      },
      {
        path: 'ResultProject',
        name: 'ResultProject',
        component: () => import('@/views/rad/radSystemData/ResultProject'),
        meta: { title: '检验结果项目', icon: 'example' }
      },
      {
        path: 'CheckApplyProject',
        name: 'CheckApplyProject',
        component: () => import('@/views/rad/radSystemData/CheckApplyProject'),
        meta: { title: '检查申请项目', icon: 'example' }
      }
    ]
  },
  {
    path: '/rad/radDictionDefend',
    component: Layout,
    redirect: '/rad/radDictionDefend',
    name: 'radDictionDefend',
    point: 'RAD',
    meta: { title: '字典维护', icon: 'example' },
    children: [
      {
        path: 'DoctorDiction',
        name: 'DoctorDiction',
        component: () =>
          import('@/views/rad/radDictionDefend/DoctorDiction'),
        meta: { title: '医生字典信息', icon: 'example' }
      },
      {
        path: 'OperationDiction',
        name: 'OperationDiction',
        component: () =>
          import('@/views/rad/radDictionDefend/OperationDiction'),
        meta: { title: '手术字典信息', icon: 'example' }
      },
      {
        path: 'ChargeDiction',
        name: 'ChargeDiction',
        component: () =>
          import('@/views/rad/radDictionDefend/ChargeDiction'),
        meta: { title: '收费字典信息', icon: 'example' }
      },
      {
        path: 'CheckApplyDiction',
        name: 'CheckApplyDiction',
        component: () =>
          import('@/views/rad/radDictionDefend/CheckApplyDiction'),
        meta: { title: '检查申请项目', icon: 'example' }
      },
      {
        path: 'InspectApplyDiction',
        name: 'InspectApplyDiction',
        component: () =>
          import('@/views/rad/radDictionDefend/InspectApplyDiction'),
        meta: { title: '检验申请项目', icon: 'example' }
      },
      {
        path: 'InspecTresult',
        name: 'InspecTresult',
        component: () =>
          import('@/views/rad/radDictionDefend/InspecTresult'),
        meta: { title: '检验结果项目', icon: 'example' }
      },
      {
        path: 'DepartmentDiction',
        name: 'DepartmentDiction',
        component: () =>
          import('@/views/rad/radDictionDefend/DepartmentDiction'),
        meta: { title: '科室字典信息', icon: 'example' }
      },
      {
        path: 'RouteDiction',
        name: 'RouteDiction',
        component: () =>
          import('@/views/rad/radDictionDefend/RouteDiction'),
        meta: { title: '给药途径信息', icon: 'example' }
      },
      {
        path: 'FrequenDiction',
        name: 'FrequenDiction',
        component: () =>
          import('@/views/rad/radDictionDefend/FrequenDiction'),
        meta: { title: '给药频次信息', icon: 'example' }
      },
      {
        path: 'DrugDiction',
        name: 'DrugDiction',
        component: () =>
          import('@/views/rad/radDictionDefend/DrugDiction'),
        meta: { title: '药品字典信息', icon: 'example' }
      },
      {
        path: 'Diagnostic',
        name: 'Diagnostic',
        component: () =>
          import('@/views/rad/radDictionDefend/Diagnostic'),
        meta: { title: '诊断信息', icon: 'example' }
      }
    ]
  },
  {
    path: '/rad/radSystemRules',
    component: Layout,
    redirect: '/rad/radSystemRules',
    name: 'radSystemRules',
    point: 'RAD',
    meta: { title: '系统规则', icon: 'example' },
    children: [
      {
        path: 'Reactions',
        name: 'Reactions',
        component: () =>
          import('@/views/rad/radSystemRules/Reactions/Reactions'),
        meta: { title: '不良反应', icon: 'spot' }
      },
      {
        path: 'InVitroCompati',
        name: 'InVitroCompati',
        component: () =>
          import('@/views/rad/radSystemRules/InVitroCompati'),
        meta: { title: '体外配伍', icon: 'spot' }
      },
      {
        path: 'ChildrenMedication',
        name: 'ChildrenMedication',
        component: () =>
          import('@/views/rad/radSystemRules/ChildrenMedication'),
        meta: { title: '儿童用药', icon: 'spot' }
      },
      {
        path: 'Dosage',
        name: 'Dosage',
        component: () =>
          import('@/views/rad/radSystemRules/Dosage'),
        meta: { title: '剂量', icon: 'spot' }
      },
      {
        path: 'SuckleDrug',
        name: 'SuckleDrug',
        component: () =>
          import('@/views/rad/radSystemRules/SuckleDrug'),
        meta: { title: '哺乳用药', icon: 'spot' }
      },
      {
        path: 'PregnancyMedica',
        name: 'PregnancyMedica',
        component: () =>
          import('@/views/rad/radSystemRules/PregnancyMedica'),
        meta: { title: '妊娠用药', icon: 'spot' }
      },
      {
        path: 'SexMedica',
        name: 'SexMedica',
        component: () =>
          import('@/views/rad/radSystemRules/SexMedica'),
        meta: { title: '性别用药', icon: 'spot' }
      },
      {
        path: 'AdultMedica',
        name: 'AdultMedica',
        component: () =>
          import('@/views/rad/radSystemRules/AdultMedica'),
        meta: { title: '成人用药', icon: 'spot' }
      },
      {
        path: 'Interaction',
        name: 'Interaction',
        component: () =>
          import('@/views/rad/radSystemRules/Interaction'),
        meta: { title: '相互作用', icon: 'spot' }
      },
      {
        path: 'AdministraRoute',
        name: 'AdministraRoute',
        component: () =>
          import('@/views/rad/radSystemRules/AdministraRoute'),
        meta: { title: '给药途径', icon: 'spot' }
      },
      {
        path: 'ElderlyMedica',
        name: 'ElderlyMedica',
        component: () =>
          import('@/views/rad/radSystemRules/ElderlyMedica'),
        meta: { title: '老人用药', icon: 'spot' }
      },
      {
        path: 'LiverFunction',
        name: 'LiverFunction',
        component: () =>
          import('@/views/rad/radSystemRules/LiverFunction'),
        meta: { title: '肝功能规则', icon: 'spot' }
      },
      {
        path: 'RenalFunction',
        name: 'RenalFunction',
        component: () =>
          import('@/views/rad/radSystemRules/RenalFunction'),
        meta: { title: '肾功能规则', icon: 'spot' }
      },
      {
        path: 'DrugTaboo',
        name: 'DrugTaboo',
        component: () =>
          import('@/views/rad/radSystemRules/DrugTaboo'),
        meta: { title: '药物禁忌症', icon: 'spot' }
      },
      {
        path: 'AllergyInfo',
        name: 'AllergyInfo',
        component: () =>
          import('@/views/rad/radSystemRules/AllergyInfo'),
        meta: { title: '过敏信息', icon: 'spot' }
      },
      {
        path: 'Indication',
        name: 'Indication',
        component: () =>
          import('@/views/rad/radSystemRules/Indication'),
        meta: { title: '适应症', icon: 'spot' }
      },
      {
        path: 'Concentration',
        name: 'Concentration',
        component: () =>
          import('@/views/rad/radSystemRules/Concentration'),
        meta: { title: '配伍浓度', icon: 'spot' }
      },
      {
        path: 'RepeatedUserDrug',
        name: 'RepeatedUserDrug',
        component: () =>
          import('@/views/rad/radSystemRules/RepeatedUserDrug'),
        meta: { title: '重复用药', icon: 'spot' }
      },
      {
        path: 'ConfirmRule',
        name: 'ConfirmRule',
        component: () =>
          import('@/views/rad/radSystemRules/ConfirmRule/ConfirmRule'),
        meta: { title: '确认规则', icon: 'spot' }
      }
    ]
  },
  // {
  //   path: '/rad/radParameterSetting',
  //   component: Layout,
  //   redirect: '/rad/radParameterSetting',
  //   name: 'radParameterSetting',
  //   point: 'RAD',
  //   meta: { title: '参数设置', icon: 'example' },
  //   children: [
  //     {
  //       path: 'ReviewParameterSet',
  //       name: 'ReviewParameterSet',
  //       component: () =>
  //         import('@/views/rad/radParameterSetting/ReviewParameterSet/index'),
  //       meta: { title: '审查参数设置', icon: 'example' }
  //     }
  //   ]
  // },
  {
    path: '/rad/radReviewPair',
    component: Layout,
    redirect: '/rad/radReviewPair',
    name: 'radReviewPair',
    point: 'RAD',
    meta: { title: '审查配对', icon: 'example' },
    children: [
      {
        path: 'SafeDrugMatch',
        name: 'SafeDrugMatch',
        component: () =>
          import('@/views/rad/radReviewPair/SafeDrugMatch/index'),
        meta: { title: '安全用药同步配对', icon: 'example' }
      }
    ]
  },

  // 点评中心 信息查看
  // {
  //   path: '/prc/prcinformationsviewer',
  //   component: Layout,
  //   redirect: '/prc/prcinformationsviewer/outpatientcases',
  //   point: 'PRC',
  //   name: 'PrcInformationsViewer',
  //   meta: { title: '信息查看', icon: 'example' },
  //   children: [
  //     {
  //       path: 'outpatientcases',
  //       name: 'OutpatientCases',
  //       component: () =>
  //         import('@/views/prc/prcInformationsViewer/outpatientCases/OutpatientCases'),
  //       meta: { title: '门诊病历', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'emergencycases',
  //       name: 'EmergencyCases',
  //       component: () =>
  //         import('@/views/prc/prcInformationsViewer/emergencyCases/EmergencyCases'),
  //       meta: { title: '急症病历', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'incourtcases',
  //       name: 'IncourtCases',
  //       component: () =>
  //         import('@/views/prc/prcInformationsViewer/incourtCases/IncourtCases'),
  //       meta: { title: '在院病历', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'thehospitalcases',
  //       name: 'TheHospitalCases',
  //       component: () =>
  //         import('@/views/prc/prcInformationsViewer/theHospitalCases/TheHospitalCases'),
  //       meta: { title: '出院病历', icon: 'spot', background: '#e6edf5' }
  //     }
  //   ]
  // },

  // 点评中心 门诊点评
  {
    path: '/prc/prcoutpatientreviews',
    component: Layout,
    redirect: '/prc/prcoutpatientreviews/prescription',
    point: 'PRC',
    name: 'PrcOutpatientReviews',
    meta: { title: '门诊点评', icon: 'example' },
    children: [
      {
        path: 'prescription',
        name: 'Prescription',
        component: () =>
          import('@/views/prc/prcOutpatientReviews/prescription/Prescription'),
        meta: { title: '门诊处方点评', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'outpatientreview',
        name: 'OutpatientReview',
        component: () =>
          import('@/views/prc/prcOutpatientReviews/outpatientReview/OutpatientReview'),
        meta: { title: '门诊病人点评', icon: 'spot', background: '#e6edf5' },
      },
      {
        path: 'antibacterial ',
        name: 'Antibacterial ',
        component: () =>
          import('@/views/prc/prcOutpatientReviews/antibacterial/Antibacterial'),
        meta: {
          title: '门诊抗菌药物处方点评',
          icon: 'spot',
          background: '#e6edf5'
        }
      },
      {
        path: 'prescpatlist',
        name: 'PrescPatList',
        component: () => import('@/views/prc/prcOutpatientReviews/PrescPatList'),
        hidden: true,
        meta: { title: '筛选结果', icon: 'example', background: '#e6edf5' }
      }
    ]
  },

  // 点评中心 住院点评
  {
    path: '/prc/prchospitalreviews',
    component: Layout,
    redirect: '/prc/prchospitalreviews/inpatientreview',
    point: 'PRC',
    name: 'PrcHospitalReviews',
    meta: { title: '住院点评', icon: 'example' },
    children: [
      {
        path: 'inpatientreview',
        name: 'InpatientReview',
        component: () =>
          import('@/views/prc/prcHospitalReviews/inpatientReview/InpatientReview'),
        meta: { title: '住院病人点评', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'antibioticpatientreview',
        name: 'AntibioticPatientReview',
        component: () =>
          import('@/views/prc/prcHospitalReviews/antibioticPatientReview/AntibioticPatientReview'),
        meta: {
          title: '住院抗菌药物点评',
          icon: 'spot',
          background: '#e6edf5'
        }
      },

      {
        path: 'perioperativeperiodreview',
        name: 'PerioperativePeriodReview ',
        component: () =>
          import('@/views/prc/prcHospitalReviews/perioperativePeriodReview/PerioperativePeriodReview'),
        meta: { title: '围术期用药点评', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'prescpatlist',
        name: 'PrescPatList',
        component: () => import('@/views/prc/prcHospitalReviews/PrescPatList'),
        hidden: true,
        meta: { title: '筛选结果', icon: 'example', background: '#e6edf5' }
      }
    ]
  },

  // // 点评中心 电子药历
  // {
  //   path: '/prc/prcelectronicmedicine',
  //   component: Layout,
  //   redirect: '/prc/prcelectronicmedicine/electronicmedicine',
  //   point: 'PRC',
  //   name: 'prcElectronicMedicine',
  //   meta: { title: '电子药历', icon: 'example' },
  //   children: [
  //     {
  //       path: 'electronicmedicine',
  //       name: 'ElectronicMedicine',
  //       component: () =>
  //         import('@/views/prc/prcElectronicMedicine/ElectronicMedicine/'),
  //       meta: { title: '电子药历', icon: 'example', background: '#e6edf5' }
  //     }
  //   ]
  // },

  // 点评中心 数据中心
  {
    path: '/prc/prcDataCenter',
    component: Layout,
    redirect: '/prc/prcDataCenter/rationalityIndex',
    point: 'PRC',
    name: 'RrcDataCenter',
    meta: { title: '数据中心', icon: 'example' },
    children: [
      {
        path: 'antimicrobialStatistics',
        name: 'AntimicrobialStatistics',
        component: () =>
          import('@/views/prc/prcDataCenter/antimicrobialStatistics/AntimicrobialStatistics'),
        meta: { title: '抗菌药物统计', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'drugUseRanking',
        name: 'DrugUseRanking',
        component: () =>
          import('@/views/prc/prcDataCenter/drugUseRanking/DrugUseRanking'),
        meta: { title: '药品使用排名', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'otherRelated',
        name: 'OtherRelated',
        component: () =>
          import('@/views/prc/prcDataCenter/otherRelated/OtherRelated'),
        meta: { title: '其他相关', icon: 'spot', background: '#e6edf5' }
      }
      // {
      //   path: 'RationalityIndex',
      //   name: 'RationalityIndex',
      //   component: () =>
      //     import('@/views/prc/prcDataCenter/rationalityIndex/RationalityIndex'),
      //   meta: { title: '合理性指标', icon: 'spot', background: '#e6edf5' }
      // }
    ]
  },

  // // 点评中心 考核指标
  // {
  //   path: '/prc/prcevaluation',
  //   component: Layout,
  //   redirect: '/prc/prcevaluation/departmentsetting',
  //   point: 'PRC',
  //   name: 'prcEvaluation',
  //   meta: { title: '考核指标', icon: 'example' },
  //   children: [
  //     {
  //       path: 'departmentsetting',
  //       name: 'DepartmentSetting',
  //       component: () =>
  //         import('@/views/prc/prcEvaluation/department/DepartmentSetting'),
  //       meta: { title: '科室目标值设置', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'doctorsetting',
  //       name: 'DoctorSetting',
  //       component: () =>
  //         import('@/views/prc/prcEvaluation/doctor/DoctorSetting'),
  //       meta: { title: '医生目标值设置', icon: 'spot', background: '#e6edf5' }
  //     }
  //   ]
  // },
  // // 点评中心 数据维护
  // {
  //   path: '/prc/prcdatamaintenance',
  //   component: Layout,
  //   redirect: '/prc/prcdatamaintenance/commentstemplate',
  //   point: 'PRC',
  //   name: 'prcEvaluation',
  //   meta: { title: '数据维护', icon: 'example' },
  //   children: [
  //     {
  //       path: 'commentstemplate',
  //       name: 'CommentsTemplate',
  //       component: () =>
  //         import('@/views/prc/prcDataMaintenance/commentsTemplate/CommentsTemplate'),
  //       meta: { title: '点评模板', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'censorshiprules',
  //       name: 'CensorshipRules',
  //       component: () =>
  //         import('@/views/prc/prcDataMaintenance/censorshipRules/CensorshipRules'),
  //       meta: { title: '审查规则', icon: 'spot', background: '#e6edf5' }
  //     }
  //   ]
  // },

  // 临床药师工作站
  {
    path: '/cpw/index',
    component: Layout,
    redirect: '/cpw/index/index',
    point: 'CPW',
    name: 'Index',
    meta: { title: '首页', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'index',
        name: 'Index',
        component: () => import('@/views/cpw/index/index'),
        meta: { title: '首页', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/cpw/guardianshiplist',
    component: Layout,
    redirect: '/cpw/guardianshiplist/guardianshiplist',
    point: 'CPW',
    name: 'GuardianshipList',
    meta: { title: '监护列表', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'guardianshiplist',
        name: 'GuardianshipList',
        component: () => import('@/views/cpw/guardianshipList/GuardianshipList'),
        meta: { title: '监护列表', icon: 'example', background: '#e6edf5' }
      },
      {
        path: 'patientdetail',
        name: 'PatientDetail',
        component: () => import('@/views/cpw/guardianshipList/patientDetail/patientDetail'),
        hidden: true,
        meta: { title: '病人详细信息', icon: 'example', background: '#e6edf5' }
      },
      {
        path: 'patientordersdata',
        name: 'PatientOrdersdata',
        component: () => import('@/views/cpw/guardianshipList/patientDetail/PatientOrdersdata'),
        hidden: true,
        meta: { title: '病人医嘱信息', icon: 'example', background: '#e6edf5' }
      }, {
        path: 'patientlablist',
        name: 'PatientLabList',
        component: () => import('@/views/cpw/guardianshipList/patientDetail/PatientLabList'),
        hidden: true,
        meta: { title: '检验信息', icon: 'example', background: '#e6edf5' }
      },
      {
        path: 'patientexamlist',
        name: 'PatientExamList',
        component: () => import('@/views/cpw/guardianshipList/patientDetail/PatientExamList'),
        hidden: true,
        meta: { title: '检查信息', icon: 'example', background: '#e6edf5' }
      },
      {
        path: 'historyinfo',
        name: 'HistoryInfo',
        component: () => import('@/views/cpw/guardianshipList/patientDetail/HistoryInfo'),
        hidden: true,
        meta: { title: '历史诊疗信息', icon: 'example', background: '#e6edf5' }
      }

    ]
  },
  {
    path: '/cpw/orderslist',
    component: Layout,
    redirect: '/cpw/orderslist/orderslist',
    point: 'CPW',
    name: 'OrdersList',
    meta: { title: '医嘱列表', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'orderslist',
        name: 'OrdersList',
        component: () => import('@/views/cpw/ordersList/OrdersList'),
        meta: { title: '医嘱列表', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/cpw/pharmaceuticalrecords',
    component: Layout,
    redirect: '/cpw/pharmaceuticalrecords/pharmaceuticalrecords',
    point: 'CPW',
    name: 'PharmaceuticalRecords',
    meta: { title: '药学记录', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'pharmaceuticalrecords',
        name: 'PharmaceuticalRecords',
        component: () => import('@/views/cpw/pharmaceuticalRecords/PharmaceuticalRecords'),
        meta: { title: '药学记录', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/cpw/wardroundremarkslist',
    component: Layout,
    redirect: '/cpw/wardroundremarkslist/wardroundremarkslist',
    point: 'CPW',
    name: 'WardRoundRemarksList',
    meta: { title: '查房备注', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'wardroundremarkslist',
        name: 'WardRoundRemarksList',
        component: () => import('@/views/cpw/wardRoundRemarksList/WardRoundRemarksList'),
        meta: { title: '查房备注', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/cpw/medicationconsultation',
    component: Layout,
    redirect: '/cpw/medicationconsultation/medicationconsultation',
    point: 'CPW',
    name: 'MedicationConsultation',
    meta: { title: '用药咨询', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'medicationconsultation',
        name: 'MedicationConsultation',
        component: () => import('@/views/cpw/medicationConsultation/MedicationConsultation'),
        meta: { title: '用药咨询', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  // {
  //   path: '/cpw/patientrounds',
  //   component: Layout,
  //   redirect: '/cpw/patientrounds/patientinfo',
  //   point: 'CPW',
  //   name: 'Patientrounds',
  //   meta: { title: '病人查房', icon: 'example' },
  //   children: [
  //     {
  //       path: 'patientinfo',
  //       name: 'PatientInfo',
  //       component: () => import('@/views/cpw/patientrounds/PatientInfo'),
  //       meta: {
  //         title: '患者信息正式查看',
  //         icon: 'example',
  //         background: '#e6edf5'
  //       }
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/patientviews',
  //   component: Layout,
  //   redirect: '/cpw/patientviews/patientviews',
  //   point: 'CPW',
  //   name: 'Patientrounds',
  //   meta: { title: '患者360视图', icon: 'example' },
  //   children: [
  //     {
  //       path: 'patientviews',
  //       name: 'PatientViews',
  //       component: () => import('@/views/cpw/patientViews/PatientViews'),
  //       meta: { title: '患者全部信息', icon: 'example', background: '#e6edf5' }
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/pharmaceuticalEvaluate',
  //   component: Layout,
  //   redirect: '/cpw/pharmaceuticalEvaluate/commonevaluate',
  //   point: 'CPW',
  //   name: 'PharmaceuticalEvaluate',
  //   meta: { title: '药学评估', icon: 'example' },
  //   children: [
  //     {
  //       path: 'commonevaluate',
  //       name: 'CommonEvaluate',
  //       component: () =>
  //         import(
  //           '@/views/cpw/pharmaceuticalEvaluate/commonEvaluate/CommonEvaluate'
  //         ),
  //       meta: { title: '通用评估', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'specialistevaluate',
  //       name: 'SpecialistEvaluate',
  //       component: () =>
  //         import(
  //           '@/views/cpw/pharmaceuticalEvaluate/specialistEvaluate/SpecialistEvaluate'
  //         ),
  //       meta: { title: '专科评估', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'assessmentscale',
  //       name: 'AssessmentScale',
  //       component: () =>
  //         import(
  //           '@/views/cpw/pharmaceuticalEvaluate/assessmentScale/AssessmentScale'
  //         ),
  //       meta: { title: '评估量表自定义', icon: 'spot', background: '#e6edf5' }
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/drugintervention',
  //   component: Layout,
  //   redirect: '/cpw/drugintervention/consultingaudit',
  //   point: 'CPW',
  //   name: 'DrugIntervention',
  //   meta: { title: '用药干预', icon: 'example' },
  //   children: [
  //     {
  //       path: 'consultation',
  //       name: 'Consultation',
  //       component: () =>
  //         import('@/views/cpw/drugIntervention/consultation/Consultation'),
  //       meta: { title: '用药咨询', icon: 'example' }
  //     },
  //     {
  //       path: 'consultingaudit',
  //       name: 'ConsultingAudit',
  //       redirect: '/cpw/drugintervention/consultingaudit/approvalconsultation',
  //       component: () =>
  //         import('@/views/cpw/drugIntervention/consultingAudit/index'),
  //       children: [
  //         {
  //           path: 'approvalconsultation',
  //           name: 'ApprovalConsultation',
  //           component: () =>
  //             import(
  //               '@/views/cpw/drugIntervention/consultingAudit/approvalConsultation/ApprovalConsultation'
  //             ),
  //           meta: { title: '咨询问答审核', icon: 'spot', background: '#e6edf5' }
  //         }
  //       ],
  //       meta: { title: '咨询审核', icon: 'example' }
  //     },
  //     {
  //       path: 'doctorConsult',
  //       name: 'DoctorConsult',
  //       meta: { title: '医生咨询', icon: 'example' },
  //       component: () =>
  //         import('@/views/cpw/drugIntervention/doctorConsult/index'),
  //       children: [
  //         {
  //           path: 'specialConsultation',
  //           name: 'SpecialConsultation',
  //           component: () =>
  //             import(
  //               '@/views/cpw/drugIntervention/doctorConsult/specialConsultation/SpecialConsultation'
  //             ),
  //           meta: { title: '专项咨询', icon: 'spot', background: '#e6edf5' }
  //         },
  //         {
  //           path: 'commonConsultation',
  //           name: 'CommonConsultation',
  //           component: () =>
  //             import(
  //               '@/views/cpw/drugIntervention/doctorConsult/commonConsultation/CommonConsultation'
  //             ),
  //           meta: { title: '通用咨询', icon: 'spot', background: '#e6edf5' }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'pharmacistReply',
  //       name: 'PharmacistReply',
  //       meta: { title: '药师回复', icon: 'example' },
  //       component: () =>
  //         import('@/views/cpw/drugIntervention/pharmacistReply/index'),
  //       children: [
  //         {
  //           path: 'specialReply',
  //           name: 'SpecialReply',
  //           component: () =>
  //             import(
  //               '@/views/cpw/drugIntervention/pharmacistReply/specialReply/SpecialReply'
  //             ),
  //           meta: { title: '专项回复', icon: 'spot', background: '#e6edf5' }
  //         },
  //         {
  //           path: 'commonReply',
  //           name: 'CommonReply',
  //           component: () =>
  //             import(
  //               '@/views/cpw/drugIntervention/pharmacistReply/commonReply/CommonReply'
  //             ),
  //           meta: { title: '通用回复', icon: 'spot', background: '#e6edf5' }
  //         }
  //       ]
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/medicationeducation',
  //   component: Layout,
  //   redirect: '/cpw/medicationeducation/medicationeducation',
  //   point: 'CPW',
  //   name: 'MedicationEducations',
  //   meta: { title: '用药教育', icon: 'example' },
  //   children: [
  //     {
  //       path: 'medicationeducation',
  //       name: 'MedicationEducation',
  //       component: () =>
  //         import(
  //           '@/views/cpw/medicationEducation/MedicationEducation'
  //           ),
  //       meta: { title: '用药教育', icon: 'spot', background: '#e6edf5' }
  //     }
  //   ]
  // },
  // // 药师工作站 知识管理
  // {
  //   path: '/cpw/knowledgemanagement',
  //   component: Layout,
  //   redirect: '/cpw/knowledgemanagement/publicknowledge',
  //   point: 'CPW',
  //   name: 'KnowledgeManagement',
  //   meta: { title: '知识管理', icon: 'example' },
  //   children: [
  //     {
  //       path: 'publicKnowledge',
  //       name: 'PublicKnowledge',
  //       component: () =>
  //         import(
  //           '@/views/cpw/knowledgeManagement/publicKnowledge/PublicKnowledge'
  //         ),
  //       meta: { title: '共有知识', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'privateknowledge',
  //       name: 'PrivateKnowledge',
  //       component: () =>
  //         import(
  //           '@/views/cpw/knowledgeManagement/privateKnowledge/PrivateKnowledge'
  //         ),
  //       meta: { title: '私有知识', icon: 'spot', background: '#e6edf5' }
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/electronicmedicine',
  //   component: Layout,
  //   redirect: '/cpw/electronicmedicine/medicinetemplate',
  //   point: 'CPW',
  //   name: 'ElectronicMedicine',
  //   meta: { title: '电子药历', icon: 'example' },
  //   children: [
  //     {
  //       path: 'medicinetemplate',
  //       name: 'MedicineTemplate',
  //       component: () =>
  //         import('@/views/cpw/electronicMedicine/MedicineTemplate'),
  //       meta: { title: '药历模板', icon: 'example', background: '#e6edf5' }
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/pharmaceuticalcare',
  //   component: Layout,
  //   redirect: '/cpw/pharmaceuticalcare/monitoringplan',
  //   point: 'CPW',
  //   name: 'PharmaceuticalCare',
  //   meta: { title: '药学监护', icon: 'example' },
  //   children: [
  //     {
  //       path: 'monitoringplan',
  //       name: 'MonitoringPlan',
  //       component: () =>
  //         import(
  //           '@/views/cpw/pharmaceuticalCare/monitoringPlan/MonitoringPlan'
  //         ),
  //       meta: { title: '监护计划', icon: 'example', background: '#e6edf5' }
  //     }
  //   ]
  // },
  // {
  //   path: '/cpw/useraction',
  //   component: Layout,
  //   redirect: '/cpw/useraction/usermanagement',
  //   point: 'CPW',
  //   name: 'UserAction',
  //   meta: { title: '用户操作', icon: 'example' },
  //   children: [
  //     {
  //       path: 'datatestpage',
  //       name: 'DataTestPage',
  //       component: () =>
  //         import('@/views/cpw/userAction/dataTestPage/DataTestPage'),
  //       meta: { title: '用户测试页面', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'datamaintenance',
  //       name: 'DataMaintenance',
  //       component: () =>
  //         import('@/views/cpw/userAction/dataMaintenance/DataMaintenance'),
  //       meta: { title: '数据维护', icon: 'spot', background: '#e6edf5' }
  //     },
  //     {
  //       path: 'usermanagement',
  //       name: 'UserManagement',
  //       component: () =>
  //         import('@/views/cpw/userAction/userManagement/UserManagement'),
  //       meta: { title: '用户管理', icon: 'spot', background: '#e6edf5' }
  //     }
  //   ]
  // },
  {
    path: '/ptc/outpatient',
    component: Layout,
    redirect: '/ptc/outpatient/check',
    point: 'PTC',
    name: 'Outpatient',
    meta: { title: '门诊审方', icon: 'example' },
    children: [
      {
        path: 'check',
        name: 'Check',
        component: () => import('@/views/ptc/outpatient/check'),
        meta: { title: '门诊监测', icon: 'spot' }
      },
      {
        path: 'query',
        name: 'Query',
        component: () => import('@/views/ptc/outpatient/query'),
        meta: { title: '门诊查询', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'department',
        name: 'Department',
        component: () => import('@/views/ptc/outpatient/department'),
        meta: { title: '门诊科室', icon: 'spot', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/ptc/inhospital',
    component: Layout,
    redirect: '/ptc/inhospital/survey',
    point: 'PTC',
    name: 'Inhospital',
    meta: { title: '住院审方', icon: 'example' },
    children: [
      {
        path: 'survey',
        name: 'Survey',
        component: () => import('@/views/ptc/inhospital/survey'),
        meta: { title: '住院监测', icon: 'spot' }
      },
      {
        path: 'query',
        name: 'Query',
        component: () => import('@/views/ptc/inhospital/query'),
        meta: { title: '住院查询', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'department',
        name: 'Department',
        component: () => import('@/views/ptc/inhospital/department'),
        meta: { title: '住院科室', icon: 'spot', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/ptc/parameter',
    component: Layout,
    redirect: '/ptc/parameter/outsystem',
    point: 'PTC',
    name: 'Parameter',
    meta: { title: '审方参数设置', icon: 'example' },
    children: [
      {
        path: 'outsystem',
        name: 'Outsystem',
        component: () => import('@/views/ptc/parameter/outsystem'),
        meta: { title: '门诊系统设置', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'outsurvey',
        name: 'Outsurvey',
        component: () => import('@/views/ptc/parameter/outsurvey'),
        meta: { title: '门诊监测标准', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'hospitalsystem',
        name: 'Hospitalsystem',
        component: () => import('@/views/ptc/parameter/hospitalsystem'),
        meta: { title: '住院系统设置', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'hospitalsurvey',
        name: 'Hospitalsurvey',
        component: () => import('@/views/ptc/parameter/hospitalsurvey'),
        meta: { title: '住院监测标准', icon: 'spot', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/ptc/report',
    component: Layout,
    redirect: '/ptc/report/today',
    point: 'PTC',
    name: 'Report',
    meta: { title: '审方报表', icon: 'example' },
    children: [
      {
        path: 'today',
        name: 'Today',
        component: () => import('@/views/ptc/report/today'),
        meta: { title: '今日动态监测', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'gy',
        name: 'Gy',
        component: () => import('@/views/ptc/report/gy'),
        meta: { title: '干预情况汇总表', icon: 'spot', background: '#e6edf5' }
      },
      {
        path: 'pass',
        name: 'Pass',
        component: () => import('@/views/ptc/report/pass'),
        meta: {
          title: '处方通过状态统计表',
          icon: 'spot',
          background: '#e6edf5'
        }
      }
    ]
  },
  {
    path: '/kb/instructions',
    component: Layout,
    redirect: '/kb/instructions/instructions',
    point: 'KB',
    name: 'Instructions',
    meta: { title: '药品说明书', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'instructions',
        name: 'Instructions',
        component: () => import('@/views/kb/instructions/instructions'),
        meta: { title: '药品说明书', icon: 'example', background: '#e6edf5' }
      },
      {
        path: 'drug',
        name: 'Drug',
        component: () => import('@/views/kb/drug/drug'),
        hidden: true,
        meta: { title: '药品厂家说明', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/kb/instructions',
    component: Layout,
    redirect: '/kb/instructions/instructions',
    point: 'KB',
    name: 'Instructions',
    meta: { title: '药品教育', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'education',
        name: 'Education',
        component: () => import('@/views/kb/education/education'),
        meta: { title: '药品教育', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/kb/chineserecip',
    component: Layout,
    redirect: '/kb/chineserecip/chineserecip',
    point: 'KB',
    name: 'Chineserecip',
    meta: { title: '中药材', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'chineserecip',
        name: 'Chineserecip',
        component: () => import('@/views/kb/chineserecip/chineserecip'),
        meta: { title: '中药材', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/kb/formula',
    component: Layout,
    redirect: '/kb/formula/formula',
    point: 'KB',
    name: 'Formula',
    meta: { title: '用药公式', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'formula',
        name: 'Formula',
        component: () => import('@/views/kb/formula/formula'),
        meta: { title: '用药公式', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/kb/chinesemedi',
    component: Layout,
    redirect: '/kb/chinesemedi/chinesemedi',
    point: 'KB',
    name: 'Chinesemedi',
    meta: { title: '中药方剂', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'chinesemedi',
        name: 'Chinesemedi',
        component: () => import('@/views/kb/chinesemedi/chinesemedi'),
        meta: { title: '中药方剂', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/kb/guide',
    component: Layout,
    redirect: '/kb/guide/guide',
    point: 'KB',
    name: 'Guide',
    meta: { title: '临床指南', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'guide',
        name: 'Guide',
        component: () => import('@/views/kb/guide/guide'),
        meta: { title: '临床指南', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/kb/druginfo',
    point: 'KB',
    name: 'druginfo',
    hidden: true,
    component: () => import('@/views/kb/druginfo/druginfo'),
    meta: { title: '药品', icon: 'example' }
  },
  {
    path: '/usercenter',
    component: Layout,
    redirect: '/usercenter/admin',
    point: 'UC',
    name: 'usercenter',
    meta: { title: '管理员管理', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'admin',
        name: 'Admin',
        component: () => import('@/views/usercenter/admin'),
        meta: { title: '管理员管理', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/usercenter',
    component: Layout,
    redirect: '/usercenter/role',
    point: 'UC',
    name: 'usercenter',
    meta: { title: '管理员管理', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'role',
        name: 'Role',
        component: () => import('@/views/usercenter/role'),
        meta: { title: '角色管理', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/usercenter',
    component: Layout,
    redirect: '/usercenter/database',
    point: 'UC',
    name: 'usercenter',
    meta: { title: '管理员管理', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'database',
        name: 'Database',
        component: () => import('@/views/usercenter/database'),
        meta: { title: '数据库管理', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/usercenter',
    component: Layout,
    redirect: '/usercenter/viewsyn',
    point: 'UC',
    name: 'usercenter',
    meta: { title: '管理员管理', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'viewsyn',
        name: 'Viewsyn',
        component: () => import('@/views/usercenter/viewsyn'),
        meta: { title: '视图同步结果', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/usercenter',
    component: Layout,
    redirect: '/usercenter/handData',
    point: 'UC',
    name: 'usercenter',
    meta: { title: '管理员管理', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'handData',
        name: 'HandData',
        component: () => import('@/views/usercenter/handData'),
        meta: { title: '手动数据管理', icon: 'example', background: '#e6edf5' }
      }
    ]
  },
  {
    path: '/usercenter',
    component: Layout,
    redirect: '/usercenter/link',
    point: 'UC',
    name: 'usercenter',
    meta: { title: '管理员管理', icon: 'example', background: '#e6edf5' },
    children: [
      {
        path: 'link',
        name: 'Link',
        component: () => import('@/views/usercenter/link'),
        meta: { title: '访问链接管理', icon: 'example', background: '#e6edf5' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  })

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
