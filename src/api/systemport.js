const kb = 16212 // 知识库
const ptc = 18086 // 审方中心
const rad = 18086 // 合理用药
const cpw = 15888 // 临床药师工作站
const prc17212 = 17212 // 点评中心
const prc19133 = 19133 // 点评中心
const uc = 18086 //用户中心
export { kb, ptc, rad, cpw, prc17212, prc19133, uc }
