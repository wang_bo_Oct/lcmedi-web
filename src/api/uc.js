import request from '@/utils/request'
import { uc } from "./systemport.js";
const port = uc;
export default {
	//登录
	login(params) {
		return request({
			url: '/admin/admin-user/login',
			method: 'post',
			params,
			port
		})
	},
}