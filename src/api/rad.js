import request from "@/utils/request";
import { rad } from "./systemport.js";
const port = rad;
//药品配对json
export default {
  //通用模块
  getdate(date) {
    if (date == null) return "";
    var now = new Date(date * 1000),
      y = now.getFullYear(),
      m = now.getMonth() + 1,
      d = now.getDate();
    return (
      y +
      "-" +
      (m < 10 ? "0" + m : m) +
      "-" +
      (d < 10 ? "0" + d : d) +
      " " +
      now.toTimeString().substr(0, 8)
    );
  },
  //药品配对
  getDrugPairList(params) {
    return request({
      url: "/core/match/drug/list-user-drug-json",
      method: "get",
      params,
      port
    });
  },
  //dialog药品配对详情
  getDrugEditList(params) {
    return request({
      url: "/core/match/drug/drug-edit",
      method: "get",
      params,
      port
    });
  },
  //dialog药品信息，药品厂家搜索
  getPairSearchList(params) {
    return request({
      url: "/core/match/drug/list-data-drug-json",
      method: "get",
      params,
      port
    });
  },
  //dialog查询药品的给药单位
  getDrugUnitList(params) {
    return request({
      url: "/core/match/drug/list-drug-unit-json",
      method: "get",
      params,
      port
    });
  },
  // 药品无法配对
  SavenoDrugmatch(params) {
    return request({
      url: "/core/match/drug/drug-cannot-match",
      method: "post",
      params,
      port
    });
  },
  // 保存药品配对途径
  SaveDrugPair(params) {
    return request({
      url: "/core/match/drug/drug-match",
      method: "post",
      params,
      port
    });
  },
  //dialog溶媒设置
  getDrugMenstrList(params) {
    return request({
      url: "/core/match/drug/drug-rm-edit",
      method: "get",
      params,
      port
    });
  },
  //dialog溶媒设置
  SaveRmSet(params) {
    return request({
      url: "/core/match/drug/save-cancel-rm",
      method: "post",
      params,
      port
    });
  },
  //给药途径配对//////////////////////////////////////////////////
  getDrugRouteList(params) {
    return request({
      url: "/core/match/route/hospital/list-user-route-json-hospital",
      method: "get",
      params,
      port
    });
  },
  // dialog给药途径配对
  getRouteMatchList(params) {
    return request({
      url: "/core/match/route/route-match",
      method: "get",
      params,
      port
    });
  },
  // 获取最适合的给药途径20条
  getDialogRouteSearchList(params) {
    return request({
      url: "/core/match/route/list-data-route-json",
      method: "get",
      params,
      port
    });
  },
  // 保存配对给药途径
  SaveEditMatch(params) {
    return request({
      url: "/core/match/route/edit-match",
      method: "post",
      params,
      port
    });
  },
  // 无法保存配对给药途径
  SaveNoEditMatch(params) {
    return request({
      url: "/core/match/route/edit-match-no",
      method: "post",
      params,
      port
    });
  },
  // 给药途径配对类型更改
  changesylx(params) {
    return request({
      url: "/core/match/route/edit-sylx",
      method: "post",
      params,
      port
    });
  },
  // 给药途径缩写更改
  changesx(params) {
    return request({
      url: "/core/match/route/edit-sx",
      method: "post",
      params,
      port
    });
  },
  // 给药途径是否皮试更改
  changesfps(params) {
    return request({
      url: "/core/match/route/edit-sfps",
      method: "post",
      params,
      port
    });
  },
  //疾病配对///////////////////////////////////////////////////////
  getDiseasematchList(params) {
    return request({
      url: "/core/match/disease/hospital/list-user-disease-json-hospital",
      method: "post",
      params,
      port
    });
  },
  // 疾病配对妊娠标记更改
  changeisHy(params) {
    return request({
      url: "/core/match/disease/edit-ishy",
      method: "post",
      params,
      port
    });
  },
  // 疾病配对感染类型
  changegrlx(params) {
    return request({
      url: "/core/match/disease/edit-grlx",
      method: "post",
      params,
      port
    });
  },
  // dialog疾病配对
  dialogDiseaseMatch(params) {
    return request({
      url: "/core/match/disease/disease-match",
      method: "get",
      params,
      port
    });
  },
  //dialog查询疾病配对信息
  dialogSearchDisease(params) {
    return request({
      url: "/core/match/disease/list-data-disease-json",
      method: "get",
      params,
      port
    });
  },
  // 保存疾病配对
  SaveEditDisease(params) {
    return request({
      url: "/core/match/disease/edit-match",
      method: "post",
      params,
      port
    });
  },
  //无法保持疾病配对
  SaveNoEditDisease(params) {
    return request({
      url: "/core/match/disease/edit-match-no",
      method: "post",
      params,
      port
    });
  },
  //过敏源配对//////////////////////////////////
  getAllergenicpairList(params) {
    return request({
      url: "/core/match/allergen/list-user-allergen-json",
      method: "get",
      params,
      port
    });
  },
  // dialog过敏源配对
  dialogAllerList(params) {
    return request({
      url: "/core/match/allergen/allergen-edit",
      method: "get",
      params,
      port
    });
  },
  // dialog过敏源配对搜索列表
  dialogSearchAllergen(params) {
    return request({
      url: "/core/match/allergen/list-data-allergen-json",
      method: "get",
      params,
      port
    });
  },
  // dialog过敏源配对保存配对
  SaveAllergenPair(params) {
    return request({
      url: "/core/match/allergen/allergen-match",
      method: "post",
      params,
      port
    });
  },
  // dialog过敏源配对无法保存配对
  SaveNoAllergenPair(params) {
    return request({
      url: "/core/match/allergen/allergen-match-no",
      method: "post",
      params,
      port
    });
  },
  //频次设置///////////////////////////////////////////////////////////
  getFrequencySetList(params) {
    return request({
      url: "/core/setting/frequency/list-json",
      method: "get",
      params,
      port
    });
  },
  // 频次设置编辑次数
  changestimes(params) {
    return request({
      url: "/core/setting/frequency/edit-times",
      method: "post",
      params,
      port
    });
  },
  // 频次设置编辑天数
  changesdays(params) {
    return request({
      url: "/core/setting/frequency/edit-days",
      method: "post",
      params,
      port
    });
  },
  // 信息维护//////////////////////////////////////////////////////
  // 说明书
  getInstructionList(params) {
    return request({
      url: "/core/match/datapi/list-user-drug-pi-json",
      method: "get",
      params,
      port
    });
  },
  // 查看系统说明书
  getInstructionData(params) {
    return request({
      url: "/core/match/datapi/data-drug-pi-detail",
      method: "get",
      params,
      port
    });
  },
  // 查看自定义
  getUserDetail(params) {
    return request({
      url: "core/match/datapi/user-drug-pi-detail",
      method: "get",
      params,
      port
    });
  },
  // dialog说明书信息
  dialogInstructionInfo(params) {
    return request({
      url: "/core/match/datapi/edit-pi",
      method: "get",
      params,
      port
    });
  },
  //编辑说明书获取信息
  EditInstructionInfo(params) {
    return request({
      url: "/core/match/datapi/edit-pi-list-json",
      method: "get",
      params,
      port
    });
  },
  //获取编辑说明书获取条目信息
  getInstructionEntry(params) {
    return request({
      url: "/core/match/datapi/edit-pi-parm",
      method: "get",
      params,
      port
    });
  },
  //编辑条目排序
  getEditSort(params) {
    return request({
      url: "/core/match/datapi/edit-pi-xh-json",
      method: "get",
      params,
      port
    });
  },
  // 保存编辑说明书
  saveEditInstruction(params) {
    return request({
      url: "/core/match/datapi/save-eidt-pi-param",
      method: "put",
      params,
      port
    });
  },
  // 复制说明书
  copyEditInstruction(params) {
    return request({
      url: "/core/match/datapi/copy-datapi",
      method: "post",
      params,
      port
    });
  },

  //查看编辑说明书
  EditInstructionList(params) {
    return request({
      url: "/core/match/datapi/delete-define",
      method: "delete",
      params,
      port
    });
  },
  // 删除自定义说明书
  deleteInstruction(params) {
    return request({
      url: "/core/match/datapi/delete-define",
      method: "delete",
      params,
      port
    });
  },
  // 保存排序
  SaveEditSort(params) {
    return request({
      url: "/core/match/datapi/edit-pi-sort",
      method: "post",
      params,
      port
    });
  },
  //重点提示
  getKeytipList(params) {
    return request({
      url: "/core/match/highrisk/list-json",
      method: "get",
      params,
      port
    });
  },
  //dialog重点提示信息
  dialogKeytipData(params) {
    return request({
      url: "/core/match/highrisk/detail",
      method: "get",
      params,
      port
    });
  },
  //dialog编辑重点提示信息
  dialogEditKeytip(params) {
    return request({
      url: "/core/match/highrisk/edit-high-risk",
      method: "post",
      params,
      port
    });
  },
  //dialog清除重点提示内容
  dialogClearKeytip(params) {
    return request({
      url: "/core/match/highrisk/clear-high-risk",
      method: "delete",
      params,
      port
    });
  },
  // 抗菌药物设置
  getAntimicrobialList(params) {
    return request({
      url: "/core/classification/list-drug-json",
      method: "get",
      params,
      port
    });
  },
  // 修改抗菌药物级别
  changeantilevel(params) {
    return request({
      url: "/core/classification/edit-antilevel",
      method: "post",
      params,
      port
    });
  },
  //科室列表>>>>>>>>>>>>>>>>>>修改门诊，住院，急诊有问题
  getSectionList(params) {
    return request({
      url: "/core/dept/list-dept-json",
      method: "get",
      params,
      port
    });
  },
  // 修改是否门诊科室
  changemzks(params) {
    return request({
      url: "/core/dept/edit-mzks",
      method: "post",
      params,
      port
    });
  },
  // 修改是否住院科室
  changezyks(params) {
    return request({
      url: "/core/dept/edit-zyks",
      method: "post",
      params,
      port
    });
  },
  // 修改是否急诊科室
  changejzks(params) {
    return request({
      url: "/core/dept/edit-jzks",
      method: "post",
      params,
      port
    });
  },
  changesfzy(params) {
    return request({
      url: "/core/dept/edit-sfzy",
      method: "post",
      params,
      port
    });
  },
  //医生列表
  getDoctorList(params) {
    return request({
      url: "/core/doctor/list-doctor-json",
      method: "get",
      params,
      port
    });
  },
  // 修改医师级别
  changeysjb(params) {
    return request({
      url: "/core/doctor/edit-ysjb",
      method: "put",
      params,
      port
    });
  },
  // 修改医师处方权
  changecfq(params) {
    return request({
      url: "/core/doctor/edit-cfq",
      method: "put",
      params,
      port
    });
  },
  // 修改医师抗菌药级别
  changesykjyjb(params) {
    return request({
      url: "/core/doctor/edit-sykjyjb",
      method: "put",
      params,
      port
    });
  },
  //科室药品自定义
  getDepartDrugList(params) {
    return request({
      url: "/core/dept/list-dept-json",
      method: "get",
      params,
      port
    });
  },
  //科室药品---选择禁用药品
  dialogUnselectedDrug(params) {
    return request({
      url: "/core/dept/list-cantuse-unselected-drug-json",
      method: "get",
      params,
      port
    });
  },
  // 医院不能使用的药品列表
  getCantuseDrugList(params) {
    return request({
      url: "/core/dept/list-cantuse-selected-drug-json",
      method: "get",
      params,
      port
    });
  },
  // 新增保存设置自定义药品警示信息
  saveBanDrug(params) {
    return request({
      url: "/core/dept/save-ban-drug",
      method: "post",
      params,
      port
    });
  },

  // 删除药品信息
  getDeleteDrugList(params) {
    return request({
      url: "/core/dept/delete-dept-ban-drug",
      method: "delete",
      params,
      port
    });
  },
  // 医生药品自定义
  getDoctorDrugList(params) {
    return request({
      url: "/core/dict/doctor/list-doctor-json",
      method: "get",
      params,
      port
    });
  },
  // 获取医生禁用药品列表
  getDoctorProhibitList(params) {
    return request({
      url: "/core/doctor/list-selected-drug-json",
      method: "get",
      params,
      port
    });
  },
  // 获取医生可禁用药品列表
  getDoctorUnselectedList(params) {
    return request({
      url: "/core/doctor/list-unselected-drug-json",
      method: "get",
      params,
      port
    });
  },
  // 新增保存医生设置自定义药品的警示信息
  SaveAddDoctorList(params) {
    return request({
      url: "/core/doctor/save-add-doctor-drug",
      method: "post",
      params,
      port
    });
  },
  // 手术列表
  getOperationList(params) {
    return request({
      url: "/core/operation/list-operation-json",
      method: "get",
      params,
      port
    });
  },
  // 耐药率列表
  getResistrateList(params) {
    return request({
      url: "/core/resistrate/list-resistrate-json",
      method: "get",
      params,
      port
    });
  },
  //  下载耐药率模板
  downloadExcel(params) {
    return request({
      url: "/core/resistrate/download-excel",
      method: "get",
      params,
      port
    });
  },
  // 清楚耐药率
  clearResistrate(params) {
    return request({
      url: "/core/resistrate/clear-resistrate-json",
      method: "delete",
      params,
      port
    });
  },
  // 审查滤镜
  getReviewFilterList(params) {
    return request({
      url: "/core/user-filter/list-json",
      method: "get",
      params,
      port
    });
  },
  // 获取审查结果详情
  getDialogFilter(params) {
    return request({
      url: "/core/user-filter/edit-filter",
      method: "get",
      params,
      port
    });
  },
  // 查看处方详情
  getDialogOrderDetail(params) {
    return request({
      url: "/core/user-exam/order-detail",
      method: "get",
      params,
      port
    });
  },
  // 编辑滤镜
  SaveEditFilter(params) {
    return request({
      url: "/core/user-filter/save-edit-filter",
      method: "put",
      params,
      port
    });
  },
  // 删除滤镜
  DeleteFilter(params) {
    return request({
      url: "/core/user-filter/delete-filter",
      method: "put",
      params,
      port
    });
  },
  // 审查结果列表
  getResultsReviewList(params) {
    return request({
      url: "/core/user-exam/list-json",
      method: "get",
      params,
      port
    });
  },
  // 查看审查结果列表
  getDialogReview(params) {
    return request({
      url: "/core/user-exam/exam-edit",
      method: "get",
      params,
      port
    });
  },
  // 保存或修改滤镜
  SaveExamEditFilter(params) {
    return request({
      url: "/core/user-exam/save-exam-edit",
      method: "post",
      params,
      port
    });
  },
  // 通用自定义列表
  getGeneralCustList(params) {
    return request({
      url: "/core/define/list-common-drug-json",
      method: "get",
      params,
      port
    });
  },
  // 多条件自定义
  getMultiConditionList(params) {
    return request({
      url: "/core/define/list-freedom-drug-json",
      method: "get",
      params,
      port
    });
  },
  // 同步药品
  drugSync(params) {
    return request({
      url: "/core/match/drug/drug-sync",
      method: "post",
      params,
      port
    });
  },
  // 同步给药方式
  routeSync(params) {
    return request({
      url: "/core/match/route/route-sync",
      method: "post",
      params,
      port
    });
  },
  // 同步诊断信息
  diseaseSync(params) {
    return request({
      url: "/core/match/disease/disease-sync",
      method: "post",
      params,
      port
    });
  },
  // 同步过敏源
  allergenSync(params) {
    return request({
      url: "/core/match/allergen/allergen-sync",
      method: "post",
      params,
      port
    });
  },
  // 同步频次信息
  frequencySync(params) {
    return request({
      url: "/core/setting/frequency/frequency-sync",
      method: "post",
      params,
      port
    });
  },
  // 同步科室
  deptSync(params) {
    return request({
      url: "/core/dept/dept-sync",
      method: "post",
      params,
      port
    });
  },
  // 同步科室
  doctorSync(params) {
    return request({
      url: "/core/doctor/doctor-sync",
      method: "post",
      params,
      port
    });
  },
  // 同步手术信息
  operationSync(params) {
    return request({
      url: "/core/operation/operation-sync",
      method: "post",
      params,
      port
    });
  },
  // 临床给药途径配对
  Route(params) {
    return request({
      url: "/admin/clinic/sync/route",
      method: "post",
      params,
      port
    });
  },
  // 药品配对
  drugMatch(params) {
    return request({
      url: "/admin/pairing/drug-match",
      method: "post",
      params,
      port
    });
  },
  // 诊断配对
  diseaseMatch(params) {
    return request({
      url: "/admin/pairing/disease-match",
      method: "post",
      params,
      port
    });
  },
  // 诊断配对
  allergenMatch(params) {
    return request({
      url: "/admin/pairing/allergen-match",
      method: "post",
      params,
      port
    });
  },
  // 给药途径配对
  routeMatch(params) {
    return request({
      url: "/admin/pairing/route-match",
      method: "post",
      params,
      port
    });
  },
  // 审查参数设置
  getParameterSet(params) {
    return request({
      url: "/core/setting/exam/detail",
      method: "get",
      params,
      port
    });
  },
  // 系统数据\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  // 药品字典
  getSysDrugList(params) {
    return request({
      url: "/core/sys/dict/drug-list",
      method: "get",
      params,
      port
    });
  },
  getSysDrugOneData(params) {
    return request({
      url: "/core/sys/dict/drug-data",
      method: "get",
      params,
      port
    });
  },
  getDosageOnelist(params) {
    return request({
      url: "/core/sys/dict/form-search",
      method: "get",
      params,
      port
    });
  },
  getCompOnelist(params) {
    return request({
      url: "/core/sys/dict/listChooseAllergen",
      method: "get",
      params,
      port
    });
  },
  updateDictDrugOne(params) {
    return request({
      url: "/core/sys/dict/updateSysDictDrugOne",
      method: "post",
      params,
      port
    });
  },
  addDictDrugOne(params) {
    return request({
      url: "/core/sys/dict/addSysDictDrugOne",
      method: "post",
      params,
      port
    });
  },
  deleteDictDrugOne(params) {
    return request({
      url: "/core/sys/dict/deleteSysDictDrugOne",
      method: "post",
      params,
      port
    });
  },
  // 删除药品信息
  deleteDrugOne(params) {
    return request({
      url: "/core/sys/dict/deleteSysDictDrugOne",
      method: "get",
      params,
      port
    });
  },
  // 获取疾病列表
  getdictDiseaseList(params) {
    return request({
      url: "/core/sys/dict/disease-list",
      method: "get",
      params,
      port
    });
  },
  // 新增疾病
  addDiseaseInfo(params) {
    return request({
      url: "/core/sys/dict/add-disease-info",
      method: "post",
      params,
      port
    });
  },
  // 展示疾病信息
  showDieaseList(params) {
    return request({
      url: "/core/sys/dict/showDiseaseOne",
      method: "get",
      params,
      port
    });
  },
  // 更新疾病
  UpdateDiseaseInfo(params) {
    return request({
      url: "/core/sys/dict/update-disease-info",
      method: "post",
      params,
      port
    });
  },
  // 删除疾病信息
  DeleteDiseaseInfo(params) {
    return request({
      url: "/core/sys/dict/delete-disease-info",
      method: "post",
      params,
      port
    });
  },
  // 给药途径字典///////////////////////////////
  // 查询给药途径记录
  showDictRouteAll(params) {
    return request({
      url: "/core/sys/dict/showSysDictRouteAll",
      method: "get",
      params,
      port
    });
  },
  showSysDictRouteOne(params) {
    return request({
      url: "/core/sys/dict/showSysDictRouteOne",
      method: "get",
      params,
      port
    });
  },
  // 更新给药途径
  updateSysDictRouteOne(params) {
    return request({
      url: "/core/sys/dict/updateSysDictRouteOne",
      method: "get",
      params,
      port
    });
  },
  // 新增给药途径
  addNewRouteOne(params) {
    return request({
      url: "/core/sys/dict/addNewSysDictRouteOne",
      method: "post",
      params,
      port
    });
  },
  // 删除给药途径
  deleteRouteOne(params) {
    return request({
      url: "/core/sys/dict/deleteSysDictRouteOne",
      method: "post",
      params,
      port
    });
  },
  addOldRouteOne(params) {
    return request({
      url: "/core/sys/dict/addOldSysDictRouteOne",
      method: "post",
      params,
      port
    });
  },
  // 获取过敏源字典数据列表
  getAllergenList(params) {
    return request({
      url: "/core/sys/dict/allergen-list",
      method: "get",
      params,
      port
    });
  },
  getAllergenSubClass(params) {
    return request({
      url: "/core/sys/dict/allergen-data-json",
      method: "get",
      params,
      port
    });
  },
  // 添加新药
  addNewDrug(params) {
    return request({
      url: "/core/sys/dict/addAllergen",
      method: "post",
      params,
      port
    });
  },
  // 更新成分
  updateAllergen(params) {
    return request({
      url: "/core/sys/dict/updateAllergen",
      method: "post",
      params,
      port
    });
  },
  // 获取手术信息列表
  getOperationAllList(params) {
    return request({
      url: "/core/sys/dict/showSysDictOperationAll",
      method: "get",
      params,
      port
    });
  },
  // 添加一条信息
  addOperationOne(params) {
    return request({
      url: "/core/sys/dict/addSysDictOperationOne",
      method: "post",
      params,
      port
    });
  },
  //系统规则/////////////////////////////////////////
  // 不良反应
  getReactionList(params) {
    return request({
      url: "/core/sys/rule/showRuleAdverseReactionDrugAllList",
      method: "get",
      params,
      port
    });
  },
  // 新增不良反应
  addAdvierseOne(params) {
    return request({
      url: "/core/sys/rule/addRuleAdverseReactionOne",
      method: "post",
      params,
      port
    });
  },
  // 获取药品编码
  getDrugCode(params) {
    return request({
      url: "/core/sys/rule/drug-json",
      method: "get",
      params,
      port
    });
  },
  // 获取给药途径
  getRoute(params) {
    return request({
      url: "/core/sys/rule/route-json",
      method: "get",
      params,
      port
    });
  },

  getDisease(params) {
    return request({
      url: "/core/phar/order/disease-test-json1",
      method: "get",
      params,
      port
    });
  },
  deleteAdverseRule(params) {
    return request({
      url: "/core/sys/rule/deleteRuleAdverseReactionOne",
      method: "delete",
      params,
      port
    });
  },
  // 查看一条不良反应
  showAdverseOne(params) {
    return request({
      url: "/core/sys/rule/showRuleAdverseReaction",
      method: "get",
      params,
      port
    });
  },
  getAdverseReactionDrugList(params) {
    return request({
      url: "/core/sys/rule/showRuleAdverseReactionDrugList",
      method: "get",
      params,
      port
    });
  },
  UpdataReacRuleone(params) {
    return request({
      url: "/core/sys/rule/updateRuleAdverseReactionOne",
      method: "put",
      params,
      port
    });
  },
  DeleteInvitOne(params) {
    return request({
      url: "/core/sys/rule/deleteRuleIvInfoOne",
      method: "delete",
      params,
      port
    });
  },
  // 体外配伍
  // 获取体外配伍列表
  getInVitroList(params) {
    return request({
      url: "/core/sys/rule/drug-Iv-json",
      method: "get",
      params,
      port
    });
  },
  getAllergen(params) {
    return request({
      url: "/core/sys/rule/showAllergen",
      method: "get",
      params,
      port
    });
  },
  addRuleIvInfo(params) {
    return request({
      url: "/core/sys/rule/addRuleIvInfo",
      method: "post",
      params,
      port
    });
  },
  ShowInvitOne(params) {
    return request({
      url: "/core/sys/rule/showRuleIvInfoOne",
      method: "get",
      params,
      port
    });
  },
  upDataInvitro(params) {
    return request({
      url: "/core/sys/rule/updateRuleIvInfoOne",
      method: "post",
      params,
      port
    });
  },
  //儿童用药
  //获取儿童用药列表
  getChildrenList(params) {
    return request({
      url: "/core/sys/rule/showChildrenDrugList",
      method: "get",
      params,
      port
    });
  },
  getRouteJson(params) {
    return request({
      url: "/core/sys/rule/route-json",
      method: "get",
      params,
      port
    });
  },
  getIcdCodeJson(params) {
    return request({
      url: "/core/phar/order/disease-test-json1",
      method: "get",
      params,
      port
    });
  },
  // 显示所有儿童审查
  getChildrenRuleAll(params) {
    return request({
      url: "/core/sys/rule/showRuleRouteTypeList",
      method: "get",
      params,
      port
    });
  },
  // 显示一条儿童用药
  getChildrenRuleOne(params) {
    return request({
      url: "/core/sys/rule/showRuleMedicationForChildren",
      method: "get",
      params,
      port
    });
  },
  // 修改儿童规则
  UpdateRuleone(params) {
    return request({
      url: "/core/sys/rule/updateRuleMCone",
      method: "post",
      params,
      port
    });
  },
  //删除儿童规则
  deleteRule(params) {
    return request({
      url: "/core/sys/rule/deleteRuleChildrenOne",
      method: "delete",
      params,
      port
    });
  },
  /////////////////////////////////////////////////////
  //剂量
  getRuleListDosa(params) {
    return request({
      url: "/core/sys/rule/drug-dosage-json",
      method: "get",
      params,
      port
    });
  },
  addRuleInfoDosa(params) {
    return request({
      url: "/core/sys/rule/addRuleDosageInfo",
      method: "post",
      params,
      port
    });
  },
  addRuleInfoDosaZY(params) {
    return request({
      url: "/core/sys/rule/addRuleDosagezy",
      method: "post",
      params,
      port
    });
  },
  showRuleAllDosaZY(params) {
    return request({
      url: "/core/sys/rule/showRuleDosagezy",
      method: "get",
      params,
      port
    });
  },
  showRuleAllDosa(params) {
    return request({
      url: "/core/sys/rule/showRuleDosageAll",
      method: "get",
      params,
      port
    });
  },
  showRuleOneDosa(params) {
    return request({
      url: "/core/sys/rule/showRuleDosageOne",
      method: "get",
      params,
      port
    });
  },
  updateRuleOneDosa(params) {
    return request({
      url: "/core/sys/rule/updateRuleDosageOne",
      method: "post",
      params,
      port
    });
  },
  updateRuleOneZY(params) {
    return request({
      url: "/core/sys/rule/updateRuleDosagezyOne",
      method: "post",
      params,
      port
    });
  },
  deleteRuleDosa(params) {
    return request({
      url: "/core/sys/rule/deleteRuleDosageOne",
      method: "delete",
      params,
      port
    });
  },
  deleteRuleDosaZY(params) {
    return request({
      url: "/core/sys/rule/deleteRuleDosagezyOne",
      method: "delete",
      params,
      port
    });
  },
  ///////////////////////////////////////////////
  // 新增规则
  addChildrenRule(params) {
    return request({
      url: "/core/sys/rule/addRuleMedicationForChildren",
      method: "post",
      params,
      port
    });
  },
  //剂量

  // 哺乳
  getLactationList(params) {
    return request({
      url: "/core/sys/rule/drug-lactation-json",
      method: "get",
      params,
      port
    });
  },
  addRuleLactInfo(params) {
    return request({
      url: "/core/sys/rule/addRuleLactationInfo",
      method: "post",
      params,
      port
    });
  },
  getLactaRouteList(params) {
    return request({
      url: "/core/sys/rule/route-json",
      method: "get",
      params,
      port
    });
  },
  // 获取一条
  getLactationInfoOne(params) {
    return request({
      url: "/core/sys/rule/showRuleLactationInfoOne",
      method: "get",
      params,
      port
    });
  },
  // 跟新
  updateLactInfo(params) {
    return request({
      url: "/core/sys/rule/updateRuleLactationInfoOne",
      method: "get",
      params,
      port
    });
  },
  // 删除
  DeleteRuleAdverse(params) {
    return request({
      url: "/core/sys/rule/deleteRuleLactationInfoOne",
      method: "get",
      params,
      port
    });
  },
  //妊娠用药
  //获取用药列表
  getRuleGestationList(params) {
    return request({
      url: "/core/sys/rule/showRuleGestationDrugList",
      method: "get",
      params,
      port
    });
  },
  // 获取全期单期妊娠
  showCount(params) {
    return request({
      url: "/core/sys/rule/showCount",
      method: "get",
      params,
      port
    });
  },
  // 新增单期妊娠
  addUpdateExamData(params) {
    return request({
      url: "/core/sys/rule/updateAddExamData",
      method: "post",
      params,
      port
    });
  },
  // 新增全期妊娠
  addUpdateExamDataAll(params) {
    return request({
      url: "/core/sys/rule/updateAddExamDataAllDay",
      method: "post",
      params,
      port
    });
  },
  // 查看单期妊娠
  showRUleAddExamData(params) {
    return request({
      url: "/core/sys/rule/showRUleAddExamData",
      method: "get",
      params,
      port
    });
  },
  // 查看全期妊娠
  showRUleAddExamAllData(params) {
    return request({
      url: "/core/sys/rule/showRuleAddExamDataAllDay",
      method: "get",
      params,
      port
    });
  },
  // 删除全期妊娠
  DeleteExamDataAllDayOne(params) {
    return request({
      url: "/core/sys/rule/deleteExamDataAllDay",
      method: "delete",
      params,
      port
    });
  },
  // 删除单期妊娠
  DeleteExamDataDayOne(params) {
    return request({
      url: "/core/sys/rule/deleteExamData",
      method: "delete",
      params,
      port
    });
  },
  // 查看一个单期
  showExamDataDayOne(params) {
    return request({
      url: "/core/sys/rule/showExamDataDayOne",
      method: "get",
      params,
      port
    });
  },

  // showRUleAddExamDataAll(params) {
  //   return request({
  //     url: "/core/sys/rule/showExamDataAllDayOne",
  //     method: "get",
  //     params,
  //     port
  //   });
  // },
  //查看一个全期
  showExamDataAllDayOne(params) {
    return request({
      url: "/core/sys/rule/showExamDataAllDayOne",
      method: "get",
      params,
      port
    });
  },
  //性别用药
  getRuleGenderList(params) {
    return request({
      url: "/core/sys/rule/showRuleGenderDrugList",
      method: "get",
      params,
      port
    });
  },



  // 新增规则
  addRuleGender(params) {
    return request({
      url: "/core/sys/rule/addRuleGender",
      method: "post",
      params,
      port
    });
  },
  getSexRuleAllList(params) {
    return request({
      url: "/core/sys/rule/showRuleSexList",
      method: "get",
      params,
      port
    });
  },
  getRuleGenderOne(params) {
    return request({
      url: "/core/sys/rule/showRuleGenderOne",
      method: "get",
      params,
      port
    });
  },
  updateRuleSexOne(params) {
    return request({
      url: "/core/sys/rule/updateRuleSexOne",
      method: "put",
      params,
      port
    });
  },
  deleteRuleSexOne(params) {
    return request({
      url: "/core/sys/rule/deleteRuleSexOne",
      method: "delete",
      params,
      port
    });
  },

  //适应症
  getIndicationList(params) {
    return request({
      url: "/core/sys/dict/drug-xylist",
      method: "get",
      params,
      port
    });
  },
  // 显示出一个drugId的疾病组
  showSuperAdaptZulist(params) {
    return request({
      url: "/core/sys/rule/showSuperAdaptZulist",
      method: "post",
      params,
      port
    });
  },

  // 成人用药列表
  getAdultMedicaList(params) {
    return request({
      url: "/core/sys/rule/showRuleAdultDrugList",
      method: "get",
      params,
      port
    });
  },
  // 新增成人用药
  addRuleAdult(params) {
    return request({
      url: "/core/sys/rule/addRuleAdult",
      method: "post",
      params,
      port
    });
  },
  // 所有成人用药规则
  getAdultRuleAllList(params) {
    return request({
      url: "/core/sys/rule/showRuleAdultList",
      method: "get",
      params,
      port
    });
  },
  // 显示一条规则
  showRuleAdultOne(params) {
    return request({
      url: "/core/sys/rule/showRuleAdultOne",
      method: "get",
      params,
      port
    });
  },
  // 更新规则
  updateRuleAdultOne(params) {
    return request({
      url: "/core/sys/rule/updateRuleAdultOne",
      method: "put",
      params,
      port
    });
  },
  // 删除一条规则
  deleteRuleAdult(params) {
    return request({
      url: "/core/sys/rule/deleteRuleAdultOne",
      method: "post",
      params,
      port
    });
  },

  //相互作用
  // 获得相互作用列表
  getRuleListInter(params) {
    return request({
      url: "/core/sys/rule/drug-inter-json",
      method: "get",
      params,
      port
    });
  },
  // 新增相互作用
  getRuleInterInfo(params) {
    return request({
      url: "/core/sys/rule/addRuleInterInfo",
      method: "post",
      params,
      port
    });
  },
  getRuleInterOne(params) {
    return request({
      url: "/core/sys/rule/showRuleInterOne",
      method: "get",
      params,
      port
    });
  },
  upDataOneInter(params) {
    return request({
      url: "/core/sys/rule/updateRuleInterOne",
      method: "put",
      params,
      port
    });
  },
  deleteInter(params) {
    return request({
      url: "/core/sys/rule/deleteRuleInterOne",
      method: "put",
      params,
      port
    });
  },
  /////////////////////////////////////////////////////
  //给药途径
  getRuleListRoute(params) {
    return request({
      url: "/core/sys/rule/drug-route-json",
      method: "get",
      params,
      port
    });
  },
  addRuleInfoRoute(params) {
    return request({
      url: "/core/sys/rule/addRuleRouteInfo",
      method: "post",
      params,
      port
    });
  },
  showRuleAllRoute(params) {
    return request({
      url: "/core/sys/rule/showRuleRouteAll",
      method: "get",
      params,
      port
    });
  },
  showRuleOneRoute(params) {
    return request({
      url: "/core/sys/rule/showRuleRouteOne",
      method: "get",
      params,
      port
    });
  },
  updateRuleOneRoute(params) {
    return request({
      url: "/core/sys/rule/updateRuleRouteOne",
      method: "post",
      params,
      port
    });
  },
  deleteRuleRoute(params) {
    return request({
      url: "/core/sys/rule/deleteRuleRoute",
      method: "delete",
      params,
      port
    });
  },
  ///////////////////////////////////////////////
  /////////////////////////////////////////////////////
  //老人用药
  getRuleListAge(params) {
    return request({
      url: "/core/sys/rule/showRuleAgeDrugList",
      method: "get",
      params,
      port
    });
  },
  addRuleInfoAge(params) {
    return request({
      url: "/core/sys/rule/addRuleAge",
      method: "post",
      params,
      port
    });
  },
  showRuleAllAge(params) {
    return request({
      url: "/core/sys/rule/showRuleAgeList",
      method: "get",
      params,
      port
    });
  },
  showRuleOneAge(params) {
    return request({
      url: "/core/sys/rule/showRuleAgeOne",
      method: "get",
      params,
      port
    });
  },
  updateRuleOneAge(params) {
    return request({
      url: "/core/sys/rule/updateRuleAgeOne",
      method: "post",
      params,
      port
    });
  },
  deleteRuleAge(params) {
    return request({
      url: "/core/sys/rule/deleteRuleAgeOne",
      method: "delete",
      params,
      port
    });
  },
  ///////////////////////////////////////////////

  /////////////////////////////////////////////////////
  //肝功能
  getRuleListLiver(params) {
    return request({
      url: "/core/sys/rule/showRuleLiverFunctionDrugList",
      method: "get",
      params,
      port
    });
  },
  addRuleInfoLiver(params) {
    return request({
      url: "/core/sys/rule/addRuleLiverFunctionOne",
      method: "post",
      params,
      port
    });
  },
  showRuleAllLiver(params) {
    return request({
      url: "/core/sys/rule/showRuleLiveFunctionList",
      method: "get",
      params,
      port
    });
  },
  showRuleOneLiver(params) {
    return request({
      url: "/core/sys/rule/showRuleLiveFunctionOne",
      method: "get",
      params,
      port
    });
  },
  updateRuleOneLiver(params) {
    return request({
      url: "/core/sys/rule/updateRuleLiverFunctionOne",
      method: "put",
      params,
      port
    });
  },
  deleteRuleLiver(params) {
    return request({
      url: "/core/sys/rule/deleteRuleLiverFunctionOne",
      method: "delete",
      params,
      port
    });
  },
  ///////////////////////////////////////////////
  showRuleDiseaseAge(params) {
    return request({
      url: "/core/phar/order/disease-test-json1",
      method: "get",
      params,
      port
    });
  },
  /////////////////////////////////////////////////////
  //肾功能
  getRuleListRenal(params) {
    return request({
      url: "/core/sys/rule/showRuleRenDosageDrugList",
      method: "get",
      params,
      port
    });
  },
  addRuleInfoRenal(params) {
    return request({
      url: "/core/sys/rule/addRuleRenDosageOne",
      method: "post",
      params,
      port
    });
  },
  showRuleAllRenal(params) {
    return request({
      url: "/core/sys/rule/showRuleRenDosageList",
      method: "get",
      params,
      port
    });
  },
  showRuleOneRenal(params) {
    return request({
      url: "/core/sys/rule/showRuleRenDosageOne",
      method: "get",
      params,
      port
    });
  },
  updateRuleOneRenal(params) {
    return request({
      url: "/core/sys/rule/updateRuleRenDosageOne",
      method: "put",
      params,
      port
    });
  },
  deleteRuleRenal(params) {
    return request({
      url: "/core/sys/rule/deleteRuleRenDosageOne",
      method: "delete",
      params,
      port
    });
  },
  ///////////////////////////////////////////////
  /////////////////////////////////////////////////////
  //药物禁忌症
  getRuleListContr(params) {
    return request({
      url: "/core/sys/rule/drug-contrScr-json",
      method: "get",
      params,
      port
    });
  },
  addRuleInfoContr(params) {
    return request({
      url: "/core/sys/rule/addRuleContraindicationInfo",
      method: "post",
      params,
      port
    });
  },
  showRuleAllContr(params) {
    return request({
      url: "/core/sys/rule/showRuleContraindicationAll",
      method: "get",
      params,
      port
    });
  },
  showRuleOneContr(params) {
    return request({
      url: "/core/sys/rule/showRuleContraindicationOne",
      method: "get",
      params,
      port
    });
  },
  updateRuleOneContr(params) {
    return request({
      url: "/core/sys/rule/updateRuleContraindicationOne",
      method: "put",
      params,
      port
    });
  },
  deleteRuleContr(params) {
    return request({
      url: "/core/sys/rule/deleteRuleContraindicationOne",
      method: "delete",
      params,
      port
    });
  },
  ///////////////////////////////////////////////
  /////////////////////////////////////////////////////
  //适应症
  getRuleListIndica(params) {
    return request({
      url: "/core/sys/rule/drug-indica-json",
      method: "get",
      params,
      port
    });
  },
  addRuleInfoIndica(params) {
    return request({
      url: "/core/sys/rule/addRuleIndicaInfo",
      method: "post",
      params,
      port
    });
  },
  showRuleAllIndica(params) {
    return request({
      url: "/core/sys/rule/showRuleContraindicationAll",
      method: "get",
      params,
      port
    });
  },
  showRuleOneIndica(params) {
    return request({
      url: "/core/sys/rule/showRuleIndicaInfoOne",
      method: "get",
      params,
      port
    });
  },
  updateRuleOneIndica(params) {
    return request({
      url: "/core/sys/rule/updateRuleIndicaInfoOne",
      method: "put",
      params,
      port
    });
  },
  deleteRuleIndica(params) {
    return request({
      url: "/core/sys/rule/deleteRuleIndica",
      method: "delete",
      params,
      port
    });
  },
  ///////////////////////////////////////////////
  /////////////////////////////////////////////////////
  //重复用药
  getRuleListRepea(params) {
    return request({
      url: "/core/sys/rule/showRuleRepeatedMedicationAll",
      method: "get",
      params,
      port
    });
  },
  addRuleInfoRepea(params) {
    return request({
      url: "/core/sys/rule/addRuleRepeatedMedicationOne",
      method: "post",
      params,
      port
    });
  },
  addRuleRepeatedGroup(params) {
    return request({
      url: "/core/sys/rule/addRuleRepeatedGroup",
      method: "post",
      params,
      port
    });
  },
  showRuleAllRepea(params) {
    return request({
      url: "/core/sys/rule/showRuleRepeatedMedicationList",
      method: "get",
      params,
      port
    });
  },
  showRuleOneRepea(params) {
    return request({
      url: "/core/sys/rule/showRuleRepeatedMedicationOne",
      method: "get",
      params,
      port
    });
  },
  updateRuleOneRepea(params) {
    return request({
      url: "/core/sys/rule/updateRuleRepeatedMedicationOne",
      method: "put",
      params,
      port
    });
  },
  deleteRuleRepea(params) {
    return request({
      url: "/core/sys/rule/deleteRuleRepeatedMedicationOne",
      method: "delete",
      params,
      port
    });
  },
  getRepeatJson(params) {
    return request({
      url: "/core/sys/rule/repeat-json",
      method: "get",
      params,
      port
    });
  },
  ///////////////////////////////////////////////
  // 确认规则
  getConfirmRuleList(params) {
    return request({
      url: "/drug/data/examine/selectAll",
      method: "get",
      params,
      port
    });
  },
  // 剂量
  getDosageConf(params) {
    return request({
      url: "/drug/data/examine/selectAllDosage",
      method: "get",
      params,
      port
    });
  },
  updateDosageState(params) {
    return request({
      url: "/drug/data/examine/updateDosageState",
      method: "post",
      params,
      port
    });
  },
  // 药物禁忌
  getContrScr(params) {
    return request({
      url: "/drug/data/examine/showContrScrDrugId",
      method: "get",
      params,
      port
    });
  },
  updateContrScrState(params) {
    return request({
      url: "/drug/data/examine/updateContrScrState",
      method: "post",
      params,
      port
    });
  },
  // 相互作用
  getInterDrugId(params) {
    return request({
      url: "/drug/data/examine/showInterDrugId",
      method: "get",
      params,
      port
    });
  },
  updateInterState(params) {
    return request({
      url: "/drug/data/examine/updateInterState",
      method: "post",
      params,
      port
    });
  },
  // 体外配伍
  getIvDrugId(params) {
    return request({
      url: "/drug/data/examine/showIvDrugId",
      method: "get",
      params,
      port
    });
  },
  updateIvState(params) {
    return request({
      url: "/drug/data/examine/updateIvState",
      method: "post",
      params,
      port
    });
  },
  // 重复用药
  getDupIngGenDrugId(params) {
    return request({
      url: "drug/data/examine/showDupIngGenDrugId",
      method: "get",
      params,
      port
    });
  },
  updateDupIngGen(params) {
    return request({
      url: "drug/data/examine/updateDupIngGen",
      method: "post",
      params,
      port
    });
  },
  // 成人用药确认
  getAdultDrugConf(params) {
    return request({
      url: "/drug/data/examine/showAdultDrugId",
      method: "get",
      params,
      port
    });
  },
  updateAdultState(params) {
    return request({
      url: "/drug/data/examine/updateAdultState",
      method: "post",
      params,
      port
    });
  },
  // 老人用药确认
  getElderlyDrugConf(params) {
    return request({
      url: "/drug/data/examine/showGeriatricDrugId",
      method: "get",
      params,
      port
    });
  },
  updateElderlyState(params) {
    return request({
      url: "/drug/data/examine/updateGeriatricState",
      method: "post",
      params,
      port
    });
  },
  // 妊娠用药单期确认
  getExamData(params) {
    return request({
      url: "/drug/data/examine/showRuleExamData",
      method: "get",
      params,
      port
    });
  },
  updateExamData(params) {
    return request({
      url: "/drug/data/examine/updateExamData",
      method: "post",
      params,
      port
    });
  },
  // 妊娠用药全期确认
  getExamAllData(params) {
    return request({
      url: "/drug/data/examine/showRuleExamDataAllDay",
      method: "get",
      params,
      port
    });
  },
  updateExamAllData(params) {
    return request({
      url: "/drug/data/examine/updateExamDataAllDay",
      method: "post",
      params,
      port
    });
  },
  //性别用药
  getSexSelectData(params) {
    return request({
      url: "/drug/data/examine/selectAllSex",
      method: "get",
      params,
      port
    });
  },
  updateSexState(params) {
    return request({
      url: "/drug/data/examine/updateSexState",
      method: "post",
      params,
      port
    });
  },
  getDrugPiInstrucData(params) {
    return request({
      url: "/drug/data/examine/data-drug-pi-detail-examine",
      method: "get",
      params,
      port
    });
  },
  //给药途径
  getRouteDrugIdData(params) {
    return request({
      url: "/drug/data/examine/showRouteDrugId",
      method: "get",
      params,
      port
    });
  },
  updateRouteState(params) {
    return request({
      url: "/drug/data/examine/updateRouteState",
      method: "post",
      params,
      port
    });
  },
  updateRouteDrugIdState(params) {
    return request({
      url: "/drug/data/examine/updateRouteState",
      method: "post",
      params,
      port
    });
  },
  //儿童用药确认
  getChildDrugConf(params) {
    return request({
      url: "/drug/data/examine/showPediatricDrugId",
      method: "get",
      params,
      port
    });
  },
  updatePediatricState(params) {
    return request({
      url: "/drug/data/examine/updatePediatricState",
      method: "post",
      params,
      port
    });
  },
  // 肝损害确认
  getHepdosageId(params) {
    return request({
      url: "/drug/data/examine/showHepdosageId",
      method: "get",
      params,
      port
    });
  },
  updateHepdosageState(params) {
    return request({
      url: "/drug/data/examine/updateHepdosageState",
      method: "post",
      params,
      port
    });
  },
  // 肾损害确认
  getdosageDrugId(params) {
    return request({
      url: "/drug/data/examine/showRendosageDrugId",
      method: "get",
      params,
      port
    });
  },
  updateRendosageState(params) {
    return request({
      url: "/drug/data/examine/updateRendosageState",
      method: "post",
      params,
      port
    });
  },
  // 重置剂量
  ResetDosageState(params) {
    return request({
      url: "/drug/data/examine/emptyDosage",
      method: "post",
      params,
      port
    });
  },
  // 重置药物禁忌
  ResetContrScrState(params) {
    return request({
      url: "/drug/data/examine/emptyContrScr",
      method: "post",
      params,
      port
    });
  },
  // 重置相互互作
  ResetInterState(params) {
    return request({
      url: "/drug/data/examine/emptyInter",
      method: "post",
      params,
      port
    });
  },
  // 重置体外配伍
  ResetemptyIvState(params) {
    return request({
      url: "/drug/data/examine/emptyIv",
      method: "post",
      params,
      port
    });
  },
  // 重置重复用药
  ResetemptyDupIngGen(params) {
    return request({
      url: "/drug/data/examine/emptyDupIngGen",
      method: "post",
      params,
      port
    });
  },
  // 重置儿童用药
  ResetemptyPediatric(params) {
    return request({
      url: "/drug/data/examine/emptyPediatric",
      method: "post",
      params,
      port
    });
  },
  // 重置成人用药
  ResetemptyAdult(params) {
    return request({
      url: "/drug/data/examine/emptyAdult",
      method: "post",
      params,
      port
    });
  },
  // 重置老人用药
  ResetemptyGeriatric(params) {
    return request({
      url: "/drug/data/examine/emptyGeriatric",
      method: "post",
      params,
      port
    });
  },
  // 重置单期妊娠
  Resetemptypregnancy(params) {
    return request({
      url: "/drug/data/examine/emptypregnancy",
      method: "post",
      params,
      port
    });
  },
  // 重置全期妊娠
  ResetemptypregnancyAllday(params) {
    return request({
      url: "/drug/data/examine/emptypregnancyAllday",
      method: "post",
      params,
      port
    });
  },
  // 重置性别用药
  ResetemptySex(params) {
    return request({
      url: "/drug/data/examine/emptySex",
      method: "post",
      params,
      port
    });
  },
  // 重置性别用药
  ResetemptyRoute(params) {
    return request({
      url: "/drug/data/examine/emptyRoute",
      method: "post",
      params,
      port
    });
  },
  // 重置性别用药
  ResetemptylactDrugId(params) {
    return request({
      url: "/drug/data/examine/emptylactDrugId",
      method: "post",
      params,
      port
    });
  },
  // 重置肝损害
  ResetemptyhepdosageId(params) {
    return request({
      url: "/drug/data/examine/emptyhepdosageId",
      method: "post",
      params,
      port
    });
  },
  // 重置肾损害
  ResetemptyRendosageDrugId(params) {
    return request({
      url: "/drug/data/examine/emptyRendosageDrugId",
      method: "post",
      params,
      port
    });
  },
  // 更新一条信息
  updateSurgery(params) {
    return request({
      url: "/core/sys/dict/updateSysDictOperationOne",
      method: "post",
      params,
      port
    });
  },
  // 删除一条信息
  deleteSurgery(params) {
    return request({
      url: "/core/sys/dict/deleteSysDictOperation",
      method: "post",
      params,
      port
    });
  },
  // 显示出所有的检验申请信息
  getDictLabAllList(params) {
    return request({
      url: "/core/sys/dict/showSysDictLabAll",
      method: "get",
      params,
      port
    });
  },
  // 删除一条信息
  addNewDictLab(params) {
    return request({
      url: "/core/sys/dict/addSysDictLabOne",
      method: "post",
      params,
      port
    });
  },
  // .显示一条
  showDictLabOne(params) {
    return request({
      url: "/core/sys/dict/showSysDictLabOne",
      method: "get",
      params,
      port
    });
  },
  // 检查申请项目
  getExamAllList(params) {
    return request({
      url: "/core/sys/dict/showSysDictExamAll",
      method: "get",
      params,
      port
    });
  },
  // 检查申请项目
  addNewDictExam(params) {
    return request({
      url: "/core/sys/dict/addSysDictExamOne",
      method: "get",
      params,
      port
    });
  },
  // 显示一条信息
  showExamOne(params) {
    return request({
      url: "/core/sys/dict/showSysDictExamOne",
      method: "get",
      params,
      port
    });
  },
  // 更新一条信息
  updateDictExam(params) {
    return request({
      url: "/core/sys/dict/updateSysDictExamOne",
      method: "post",
      params,
      port
    });
  },
  // 删除一个信息
  deleteDictExam(params) {
    return request({
      url: "/core/sys/dict/deleteSysDictExam",
      method: "post",
      params,
      port
    });
  },
  // 系统信息
  // 外包装图片
  getOutPictureList(params) {
    return request({
      url: "/core/sys/Information/outPicturelistDrug",
      method: "get",
      params,
      port
    });
  },
  //查看图片
  LoadOutPicture(params) {
    return request({
      url: "/core/sys/Information/loadOutPictureList",
      method: "get",
      // responseType: "blob",
      params,
      port
    });
  },
  // 用药指导单
  getInstruSheetDrugList(params) {
    return request({
      url: "/core/sys/Information/listInstruSheetDrugList",
      method: "get",
      params,
      port
    });
  },
  addInstruSheetOne(params) {
    return request({
      url: "/core/sys/Information/addInstruSheetOne",
      method: "post",
      params,
      port
    });
  },
  getShowGuideList(params) {
    return request({
      url: "/core/sys/Information/showInstruSheetList",
      method: "post",
      params,
      port
    });
  },
  updataGuide(params) {
    return request({
      url: "/core/sys/Information/updateInstruSheetOne",
      method: "post",
      params,
      port
    });
  },
  deleteGuide(params) {
    return request({
      url: "/core/sys/Information/deleteInstruSheet",
      method: "delete",
      params,
      port
    });
  },
  //说明书
  getInstructionXY(params) {
    return request({
      url: "/core/sys/dict/drug-xylist",
      method: "get",
      params,
      port
    });
  },
  getInstructionZY(params) {
    return request({
      url: "/core/sys/dict/drug-zylist",
      method: "get",
      params,
      port
    });
  },
  showInformationPixy(params) {
    return request({
      url: "/core/sys/Information/showInformationPiOne_xy",
      method: "get",
      params,
      port
    });
  },

  deleteInstrucXY(params) {
    return request({
      url: "/core/sys/Information/xydeletePi",
      method: "delete",
      params,
      port
    });
  },
  deleteInstrucZY(params) {
    return request({
      url: "/core/sys/Information/zydeletePi",
      method: "delete",
      params,
      port
    });
  },
  showInformationPizy(params) {
    return request({
      url: "/core/sys/Information/showInformationPiOne_zy",
      method: "get",
      params,
      port
    });
  },
  // 重要提示
  getHintMsg(params) {
    return request({
      url: "/core/sys/Information/messageHint",
      method: "get",
      params,
      port
    });
  },
  // 字典维护
  // 医生字典
  getDoctorDictList(params) {
    return request({
      url: "/core/doctor/list-doctor-json",
      method: "get",
      params,
      port
    });
  },
  // 编辑医生级别
  EditDoctorYSJB(params) {
    return request({
      url: "/core/doctor/edit-ysjb",
      method: "put",
      params,
      port
    });
  },
  // 编辑医生级别
  EditDoctorCFQ(params) {
    return request({
      url: "/core/doctor/edit-cfq",
      method: "put",
      params,
      port
    });
  },
  // 抗菌药用药级别
  EditDoctorKJYW(params) {
    return request({
      url: "/core/doctor/edit-sykjyjb",
      method: "put",
      params,
      port
    });
  },
  // 手术字典
  getOperationDictList(params) {
    return request({
      url: "/core/lcmedi/dict/operation-data",
      method: "get",
      params,
      port
    });
  },
  // 获取手术分类
  getOperationClass(params) {
    return request({
      url: "/core/lcmedi/dict/operation-fl",
      method: "get",
      params,
      port
    });
  },
  // 修改手术分类信息
  EditOperationInfo(params) {
    return request({
      url: "/core/lcmedi/dict/operation-fl-updata",
      method: "put",
      params,
      port
    });
  },
  UpdataOperationInfo(params) {
    return request({
      url: "/core/lcmedi/dict/operation-updata",
      method: "put",
      params,
      port
    });
  },
  // 查询当前手术设置的预防使用抗菌药物
  getPreventList(params) {
    return request({
      url: "/core/lcmedi/dict/list-drug-match-json",
      method: "get",
      params,
      port
    });
  },
  // 新增手术分类
  //收费项目字典
  getDictCharge(params) {
    return request({
      url: "/core/lcmedi/dict/cost_item-data",
      method: "get",
      params,
      port
    });
  },
  UpdataCharge(params) {
    return request({
      url: "/core/lcmedi/dict/cost_item-updata",
      method: "put",
      params,
      port
    });
  },
  // 给药途径信息
  getDictRoute(params) {
    return request({
      url: "/core/match/route/list-user-route-json",
      method: "get",
      params,
      port
    });
  },
  EditDictSylx(params) {
    return request({
      url: "/core/match/route/edit-sylx",
      method: "put",
      params,
      port
    });
  },
  EditDictSX(params) {
    return request({
      url: "/core/match/route/edit-sx",
      method: "put",
      params,
      port
    });
  },
  EditDictPS(params) {
    return request({
      url: "/core/match/route/edit-sfps",
      method: "put",
      params,
      port
    });
  },
  getDictSylxList(params) {
    return request({
      url: "/core/enums/list-sylx-json",
      method: "get",
      params,
      port
    });
  },
  getDictSXList(params) {
    return request({
      url: "/core/enums/list-routesx-json",
      method: "get",
      params,
      port
    });
  },
  getDictRouteData(params) {
    return request({
      url: "/core/match/route/route-match",
      method: "get",
      params,
      port
    });
  },
  getDictSearchList(params) {
    return request({
      url: "/core/match/route/list-data-route-json",
      method: "get",
      params,
      port
    });
  },
  SaveEditMatch(params) {
    return request({
      url: "/core/match/route/edit-match",
      method: "put",
      params,
      port
    });
  },
  SaveEditNoMatch(params) {
    return request({
      url: "/core/match/route/edit-match-no",
      method: "put",
      params,
      port
    });
  },
  //查询给药频次
  getFrequencyList(params) {
    return request({
      url: "/core/setting/frequency/list-json",
      method: "get",
      params,
      port
    });
  },
  EditFrequencyTime(params) {
    return request({
      url: "/core/setting/frequency/edit-times",
      method: "put",
      params,
      port
    });
  },
  EditFrequencyDay(params) {
    return request({
      url: "/core/setting/frequency/edit-days",
      method: "put",
      params,
      port
    });
  },
  getdictDrugList(params) {
    return request({
      url: "/core/lcmedi/dict/drug-list",
      method: "put",
      params,
      port
    });
  },
  getdictDrugData(params) {
    return request({
      url: "/core/lcmedi/dict/drug-data",
      method: "get",
      params,
      port
    });
  },
  getdictSfwhData(params) {
    return request({
      url: "/core/lcmedi/dict/drug-sfwh",
      method: "get",
      params,
      port
    });
  },
  // getdictDrugData(params) {
  //   return request({
  //     url: "/core/lcmedi/dict/drug-data",
  //     method: "get",
  //     params,
  //     port
  //   });
  // },
  getyllbList(params) {
    return request({
      url: "/core/dict/drug/list-yllb",
      method: "get",
      params,
      port
    });
  },
  getzdyList(params) {
    return request({
      url: "/core/dict/drug/list-zdyyp",
      method: "get",
      params,
      port
    });
  },
  UpdatadictDrugData(params) {
    return request({
      url: "/core/lcmedi/dict/drug-updata",
      method: "put",
      params,
      port
    });
  },
  getDiagList(params) {
    return request({
      url: "/core/match/disease/list-user-disease-json",
      method: "get",
      params,
      port
    });
  },
  EditDiagIshy(params) {
    return request({
      url: "/core/match/disease/edit-ishy",
      method: "get",
      params,
      port
    });
  },
  //诊断字典///////////////////////////////////////////////////////
  getDiagmatchList(params) {
    return request({
      url: "/core/match/disease/list-user-disease-json",
      method: "post",
      params,
      port
    });
  },
  // 疾病配对妊娠标记更改
  EditDiagisHy(params) {
    return request({
      url: "/core/match/disease/edit-ishy",
      method: "post",
      params,
      port
    });
  },
  // 疾病配对感染类型
  EditDiaggrlx(params) {
    return request({
      url: "/core/match/disease/edit-grlx",
      method: "post",
      params,
      port
    });
  },
  // dialog疾病配对
  EditDiagDiseaseMatch(params) {
    return request({
      url: "/core/match/disease/disease-match",
      method: "get",
      params,
      port
    });
  },
  //dialog查询疾病配对信息
  getDiagSearch(params) {
    return request({
      url: "/core/match/disease/list-data-disease-json",
      method: "get",
      params,
      port
    });
  },
  // 保存疾病配对
  SaveEditDiagDise(params) {
    return request({
      url: "/core/match/disease/edit-match",
      method: "post",
      params,
      port
    });
  },
  //无法保持疾病配对
  SaveNoEditDiagDise(params) {
    return request({
      url: "/core/match/disease/edit-match-no",
      method: "post",
      params,
      port
    });
  },
  SaveSetEdit(params) {
    return request({
      url: "/core/setting/exam/edit",
      method: "post",
      params,
      port
    });
  }
};
