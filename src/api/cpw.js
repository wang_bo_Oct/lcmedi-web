import request from '@/utils/request'
import { cpw } from './systemport.js'
const port = cpw
// 临床药师工作站

// 首页开始=======================================================
// 获取当前用户设定的监护科室
export function GetGuardianshipDeptJson(params) {
  return request({
    port,
    url: '/PatientManage/Index/GetGuardianshipDeptJson',
    method: 'get',
    params
  })
}
// 获取设定的科室出院人数 待出院人数 新入院人数
export function GetIndexPatientNum(params) {
  return request({
    port,
    url: '/PatientManage/Index/GetIndexPatientNum',
    method: 'get',
    params
  })
}
// 获取科室
export function GetDeptListJsonIndex(params) {
  return request({
    port,
    url: '/PatientManage/Index/GetDeptListJson',
    method: 'get',
    params
  })
}
// 保存选择的监护科室 单个科室就传单个科室 多个就传多个
export function SaveGuardianshipDept(params) {
  return request({
    port,
    url: '/PatientManage/Index/SaveGuardianshipDept',
    method: 'get',
    params
  })
}
// 获取监护的医嘱信息 里面今日 昨日 前日 就是传Date
export function GetIndexOrdersList(params) {
  return request({
    port,
    url: '/PatientManage/Index/GetIndexOrdersList',
    method: 'get',
    params
  })
}
// 获取药品信息
export function GetDrugList(params) {
  return request({
    port,
    url: '/PatientManage/Index/GetDrugList',
    method: 'get',
    params
  })
}

// 保存监护的药品
export function SaveDrugList(params) {
  return request({
    port,
    url: '/PatientManage/Index/SaveDrugList',
    method: 'get',
    params
  })
}
// 监护计划
export function GetGuardianshipPlandata(params) {
  return request({
    port,
    url: 'PatientManage/GuardianshipList/GetGuardianshipPlandata',
    method: 'get',
    params
  })
}
// 提醒事项
export function GetDeptReminddata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetDeptReminddata',
    method: 'get',
    params
  })
}
// 用药建议
export function GetMedicationSuggestiondata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetMedicationSuggestiondata',
    method: 'get',
    params
  })
}
// 删除提醒事项
export function SaveReminddata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/SaveReminddata',
    method: 'get',
    params
  })
}
// 首页结束=======================================================

// 监护列表开始===================================================
// 获取科室
export function gslGetDeptListJson(params) {
  return request({
    port,
    url: '/PatientManage/Index/GetDeptListJson',
    method: 'get',
    params
  })
}
// 获取主管医生
export function gslGetDictDoctorListJson(params) {
  return request({
    port,
    url: '/PatientManage/Maintenance/GetDictDoctorListJson',
    method: 'get',
    params
  })
}
// 药疗医嘱
export function gslGetDrugList(params) {
  return request({
    port,
    url: '/PatientManage/Index/GetDrugList',
    method: 'get',
    params
  })
}
// 给药途径
export function gslGetDictRouteListJson(params) {
  return request({
    port,
    url: '/PatientManage/Maintenance/GetDictRouteListJson',
    method: 'get',
    params
  })
}
// 获取监护列表
export function gslGetGuardianshipListJson(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetGuardianshipListJson',
    method: 'get',
    params
  })
}
// 监护列表单个病人 左边的详细信息
export function gslGetGuardianshipPatientDetail(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetGuardianshipPatientDetail',
    method: 'get',
    params
  })
}

// 保存用户设定的关注等级
export function gslSavePatientAttentionInfo(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/SavePatientAttentionInfo',
    method: 'get',
    params
  })
}

// 监护列表单个病人 右边的信息
export function gslGetPatientRiskdata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/getPatientRiskdata',
    method: 'get',
    params
  })
}
// 保存或者修改风险提示
export function gslSaveRiskwaringJson(params) {
  return request({
    port,
    url: '/PatientManage/Maintenance/SaveRiskwaringJson',
    method: 'get',
    params
  })
}
// 诊断
export function gslGetDictDiseaseListJson(params) {
  return request({
    port,
    url: '/PatientManage/Maintenance/GetDictDiseaseListJson',
    method: 'get',
    params
  })
}
// 获取单个病人的医嘱信息
export function gslGetPatientOrdersdata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetPatientOrdersdata',
    method: 'get',
    params
  })
}

// 获取个人用药建议
export function gslGetMedicationSuggestiondata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetMedicationSuggestiondata',
    method: 'get',
    params
  })
}

// 获取单个病人的监护计划
export function gslGetGuardianshipPlandata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetGuardianshipPlandata',
    method: 'get',
    params
  })
}
// 创建监护计划, 编辑，删除删除的时候传0  正常1，增加监护过程 Type  结束就传2
export function gslSaveGuardianshipPlandata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/SaveGuardianshipPlandata',
    method: 'get',
    params
  })
}
// 导入医嘱
export function gslGetCurrentPatientOrdersList(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetCurrentPatientOrdersList',
    method: 'get',
    params
  })
}
// 导入检验
export function gslGetCurrentPatientLabList(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetCurrentPatientLabList',
    method: 'get',
    params
  })
}
// 新增用药建议、修改用药建议、删除用药建议
export function gslSaveMedicationSuggestiondata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/SaveMedicationSuggestiondata',
    method: 'get',
    params
  })
}

// 保存科室提醒事项
export function gslSaveReminddata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/SaveReminddata',
    method: 'get',
    params
  })
}

// 获取简要记录列表 分页 单个病人的
export function gslGetSummaryRecorddata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetSummaryRecorddata',
    method: 'get',
    params
  })
}
// 新增简要记录保存 删除 修改
export function gslSaveSummaryRecorddata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/SaveSummaryRecorddata',
    method: 'get',
    params
  })
}

// 获取病人的检验列表
export function GetPatientLabList(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetPatientLabList',
    method: 'get',
    params
  })
}
// 获取药物重整信息
export function GetDrugRecombination(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetDrugRecombination',
    method: 'get',
    params
  })
}

// 获取非药物重整疾病接口
export function GetPatientDrugDisList(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetPatientDrugDisList',
    method: 'get',
    params
  })
}
// 获取非药物重整疾病接口
export function GetPatientDisList(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetPatientDisList',
    method: 'get',
    params
  })
}

// 保存药物重整

export function SaveDrugRecombination(data) {
  return request({
    headers: { 'Content-Type': 'multipart/form-data' },
    port,
    url: '/PatientManage/GuardianshipList/SaveDrugRecombination',
    method: 'post',
    data
  })
}
// // /YBS_OutpComment/OutpComment/ExcelPrescPatList
// export function ExcelPrescPatList(params) {
//   return request({
//     url: '/YBS_OutpComment/OutpComment/ExcelPrescPatList',
//     method: 'post',
//     data:{...params},
//     port
//   })
// }

// 获取检验详细信息
// 查看详细  获取这个病人（不考虑看过几次病）做过的所有这个名称检验的 时间 和 结果值  这边做个 折线图   x轴 -时间   y轴 -实际值

export function GetPatientLabDetailList(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetPatientLabDetailList',
    method: 'get',
    params
  })
}

// 获取病人的检查列表

export function GetPatientExamList(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetPatientExamList',
    method: 'get',
    params
  })
}

// 获取检查详细信息
export function GetPatientExamDetailList(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetPatientExamDetailList',
    method: 'get',
    params
  })
}

// 历史诊疗信息
// 获取用户的历史诊疗信息（根据身份证号idcard）
export function GetHistoryInfo(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetHistoryInfo',
    method: 'get',
    params
  })
}
// 根据用药时间 药品属性 药品名称 搜索历史药疗信息
export function GetHistoryDrugInfo(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetHistoryDrugInfo',
    method: 'get',
    params
  })
}
// 根据时间 申请、结果名称 搜索历史检验信息
export function GetHistoryLabInfo(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetHistoryLabInfo',
    method: 'get',
    params
  })
}

//  查询此病人未处理的备注信息
export function GetWardRoundRemarks(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetWardRoundRemarks',
    method: 'get',
    params
  })
}
//  保存病人备注信息
// 根据id 修改病人备注信息 下面有重复接口
// export function SaveWardRoundRemarks(params) {
//   return request({
//     port,
//     url: '/PatientManage/GuardianshipList/SaveWardRoundRemarks',
//     method: 'get',
//     params
//   })
// }

// 获取病人用药教育列表   获取的一个病人的用药教育列表 多个的话 就多条
// 获取单个病人用药情况列表   点击 单条  进去 看到整个的病人信息 加用药情况信息
export function GetMediEducationdata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetMediEducationdata',
    method: 'get',
    params
  })
}

// 保存病人用药教育信息  测试数据 新增     如果用药情况没有的话就不传  OrdersList
// 整体用药教育删除   传 实际的主键   id
// 修改   传 实际的主键   id     用药情况属性  status  1 有效  0无效 （等于删除）
export function SaveMedicationEducationndata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/SaveMedicationEducationndata',
    method: 'get',
    params
  })
}
// 监护列表结束===================================================

// 用药查询开始===================================================
// 获取分页数据
export function getMedicationConsultationdata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetMedicationConsultationdata',
    method: 'get',
    params
  })
}
// 用药查询增删改
export function saveMedicationConsultationdata(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/SaveMedicationConsultationdata',
    method: 'get',
    params
  })
}
// 用药查询结束===================================================

// 医嘱列表开始===================================================

export function GetOrdersListJson(params) {
  return request({
    port,
    url: '/PatientManage/OrdersList/GetOrdersListJson',
    method: 'get',
    params
  })
}
// 保存修改人工点评医嘱合理不合理

export function SaveOrdersComment(params) {
  return request({
    port,
    url: '/PatientManage/OrdersList/SaveOrdersComment',
    method: 'get',
    params
  })
}
export function GetCommentListJson(params) {
  return request({
    port,
    url: '/PatientManage/OrdersList/GetCommentListJson',
    method: 'get',
    params
  })
}

// 医嘱列表结束===================================================

// 药学记录开始===================================================
export function GetPharmaRecordsListJson(params) {
  return request({
    port,
    url: '/PatientManage/PharmaceuticalRecords/GetPharmaRecordsListJson',
    method: 'get',
    params
  })
}

export function GetPharmaRecordsDetailJson(params) {
  return request({
    port,
    url: '/PatientManage/PharmaceuticalRecords/GetPharmaRecordsDetailJson',
    method: 'get',
    params
  })
}

// 药学记录结束===================================================

// 查房备注开始===================================================
// 病人查房备注列表
export function GetWardRoundRemarksList(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/GetWardRoundRemarksList',
    method: 'get',
    params
  })
}
// 处理查房备注列表  弹出一个处理意见 填写的框子 Handlingopinions   这个字段接收
export function HandleWardRoundRemarks(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/HandleWardRoundRemarks',
    method: 'get',
    params
  })
}
// 删除
export function SaveWardRoundRemarks(params) {
  return request({
    port,
    url: '/PatientManage/GuardianshipList/SaveWardRoundRemarks',
    method: 'get',
    params
  })
}

// 查房备注结束===================================================

// // 临床药师工作站 / 病人查房 / 患者信息
// // 获取查房病人列表
// export function GetRoundPageListJson(params) {
//   return request({
//     port,
//     url: '/PatientManage/Patient/GetRoundPageListJson',
//     method: 'get',
//     params
//   })
// }
// // 关注等级
// export function AttentionLevDetail(params) {
//   return request({
//     port,
//     url: '/PatientManage/Patient/AttentionLevDetail',
//     method: 'get',
//     params
//   })
// }
// // 保存病人关注等级信息
// export function SaveAttentionDetail(params) {
//   return request({
//     port,
//     url: '/PatientManage/Patient/SaveAttentionDetail',
//     method: 'get',
//     params
//   })
// }
// // 获取科室列表list
// export function GetDeptListJson(params) {
//   return request({
//     port,
//     url: '/PatientManage/Patient/GetDeptListJson',
//     method: 'get',
//     params
//   })
// }
// // 病人详细信息
// export function PatientInfoDetail(params) {
//   return request({
//     port,
//     url: '/PatientManage/Patient/PatientInfoDetail',
//     method: 'get',
//     params
//   })
// }
// // 临床药师工作站 / 病人查房 / 患者信息====================结束============
//
// // 临床药师工作站 / 患者360视图 / 患者全部信息
// // 获取全部病人列表
// export function GetPatientAllInfoList(params) {
//   return request({
//     port,
//     url: '/PatientManage/Patient/GetPatientAllInfoList',
//     method: 'get',
//     params
//   })
// }
// //  查看病人全部详细信息
// export function PatientAllInfoDetail(params) {
//   return request({
//     port,
//     url: '/PatientManage/Patient/PatientAllInfoDetail',
//     method: 'get',
//     params
//   })
// }
// // 临床药师工作站 / 患者360视图 / 患者全部信息 =========结束======================
//
// // 临床药师工作站 / 药学评估 / 通用评估
// // 获取通用评估病人列表
// export function GetDrugEvaluationDataList(params) {
//   return request({
//     port,
//     url: '/PatientManage/Evaluation/GetDrugEvaluationDataList',
//     method: 'get',
//     params
//   })
// }
// // 获取单个病人的评估信息
// export function CurrencyEvaluationDetail(params) {
//   return request({
//     port,
//     url: '/PatientManage/Evaluation/CurrencyEvaluationDetail',
//     method: 'get',
//     params
//   })
// }
// // 保存通用评估
// export function SaveEvaluationInfo(params) {
//   return request({
//     port,
//     url: '/PatientManage/Evaluation/SaveEvaluationInfo',
//     method: 'get',
//     params
//   })
// }
// // 临床药师工作站 / 药学评估 / 通用评估=============结束==========================
//
// // 临床药师工作站 / 药学评估 / 专项评估
// // 获取专科评估病人列表
// export function GetDeptEvaluationDataList(params) {
//   return request({
//     port,
//     url: '/PatientManage/Evaluation/GetDeptEvaluationDataList',
//     method: 'get',
//     params
//   })
// }
// // 临床药师工作站 / 药学评估 / 专项评估=============结束==========================
//
// // 临床药师工作站 / 药学评估 / 评估自定义
// export function ScoreEvaluationInfo(params) {
//   return request({
//     port,
//     url: '/PatientManage/Evaluation/ScoreEvaluationInfo',
//     method: 'get',
//     params
//   })
// }
// // 保存自定义评估分数
// export function SaveScoreEvaluationInfo(params) {
//   return request({
//     port,
//     url: '/PatientManage/Evaluation/SaveScoreEvaluationInfo',
//     method: 'get',
//     params
//   })
// }
// // 临床药师工作站 / 药学评估 / 评估自定义=============结束==========================
//
// // 临床药师工作站 / 用药干预 / 用药咨询
//
// // 获取咨询列表
// export function GetConsultationListJson(params) {
//   return request({
//     port,
//     url: '/PatientManage/Consultation/GetConsultationListJson',
//     method: 'get',
//     params
//   })
// }
// // 咨询字典表获取
// export function GetConsultationItemdictList(params) {
//   return request({
//     port,
//     url: '/PatientManage/Consultation/GetConsultationItemdictList',
//     method: 'get',
//     params
//   })
// }
// // 获取单个咨询的详细
// export function GetConsultationDetail(params) {
//   return request({
//     port,
//     url: '/PatientManage/Consultation/GetConsultationDetail',
//     method: 'get',
//     params
//   })
// }
// // 新增 修改 保存
// export function SaveConsultationInfo(params) {
//   return request({
//     port,
//     url: '/PatientManage/Consultation/SaveConsultationInfo',
//     method: 'post',
//     params
//   })
// }
// // 临床药师工作站 / 用药干预 / 用药咨询 ==============结束===================
//
// // 临床药师工作站 / 用药教育 / 用药教育
// // 用药教育病人列表获取 界面跟病人查房一样 去掉 关注等级就好了
// export function GetInpPatientListJson(params) {
//   return request({
//     port,
//     url: '/PatientManage/MedicationEducation/GetInpPatientListJsonn',
//     method: 'get',
//     params
//   })
// }
// // 单个病人用药教育页面
// export function GetMedicationEducationJson(params) {
//   return request({
//     port,
//     url: '/PatientManage/MedicationEducation/GetMedicationEducationJson',
//     method: 'get',
//     params
//   })
// }
// // 保存用药教育
// export function SaveMedicationEducation(params) {
//   return request({
//     port,
//     url: '/PatientManage/MedicationEducation/SaveMedicationEducation',
//     method: 'get',
//     params
//   })
// }
// // 临床药师工作站 / 用药教育 / 用药教育=================结束=====================
