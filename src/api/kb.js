import request from "@/utils/request";
import {kb} from "./systemport.js";
const port = kb;
export default {
	//药品说明书左侧导航
	Drug(params) {
		return request({
			url: "/YBS_Draglnstmction/Drug/GetTree",
			method: "get",
			params,
			port
		})
	},
	//说明书右侧显示药品信息
	DrugMonograph(params) {
		return request({
			url: "/YBS_Draglnstmction/Drug/GetList",
			method: "get",
			params,
			port
		})
	},
	//说明书药品厂家
	DrugPharmDic(params) {
		return request({
			url: "/YBS_Draglnstmction/Drug/DrugPharmDic",
			method: "get",
			params,
			port
		})
	},
	//说明书显示HTML
	Draglnstmction(params) {
		return request({
			url: "/YBS_Draglnstmction/Drug/Draglnstmction",
			method: "get",
			params,
			port
		})
	},
	//用药教育左侧列表显示
	PharEduTree(params) {
		return request({
			url: "/YBS_PharmacyEducation/PharmacyEducation/GetTree",
			method: "get",
			params,
			port
		})
	},
	//用药教育左侧列表显示
	PharEduLists(params) {
		return request({
			url: "/YBS_PharmacyEducation/PharmacyEducation/GetList",
			method: "get",
			params,
			port
		})
	},
	//用药教育显示html
	PharEduHtml(params) {
		return request({
			url: "/YBS_PharmacyEducation/PharmacyEducation/Education",
			method: "get",
			params,
			port
		})
	},
	//医药公式左侧列表显示
	FormulaTree(params) {
		return request({
			url: "/YBS_MedicalFormula/Formula/GetTree",
			method: "get",
			params,
			port
		})
	},
	//医药公式左侧列表显示
	FormulaList(params) {
		return request({
			url: "/YBS_MedicalFormula/Formula/GetList",
			method: "get",
			params,
			port
		})
	},
	//中药材左侧列表显示
	HerbalMedicine(params) {
		return request({
			url: "/YBS_HerbalMedicine/HerbalMedicine/GetTree",
			method: "get",
			params,
			port
		})
	},
	//中药材右侧显示药品信息
	HerbalMedicineGetlist(params) {
		return request({
			url: "/YBS_HerbalMedicine/HerbalMedicine/Getlist",
			method: "get",
			params,
			port
		})
	},
	//中药材html显示
	HerbalMedicineContent(params) {
		return request({
			url: "/YBS_HerbalMedicine/HerbalMedicine/HerbalMedicineContent",
			method: "get",
			params,
			port
		})
	},
	//中方药剂左侧导航显示
	TXYFa(params) {
		return request({
			url: "/YBS_TXYFa/TXYFa/GetTree",
			method: "get",
			params,
			port
		})
	},
	//中方药剂右侧信息
	TXYFaGetlist(params) {
		return request({
			url: "/YBS_TXYFa/TXYFa/Getlist",
			method: "get",
			params,
			port
		})
	},
	//中方药剂html显示
	TXYFaContent(params) {
		return request({
			url: "/YBS_TXYFa/TXYFa/TXYFaContent",
			method: "get",
			params,
			port
		})
	},
	//临床医学左侧导航显示
	ClinicalGuidelines(params) {
		return request({
			url: "/YBS_ClinicalGuidelines/ClinicalGuidelines/GetTree",
			method: "get",
			params,
			port
		})
	},
	//临床医学右侧信息显示
	ClinicalGuidelinesGetList(params) {
		return request({
			url: "/YBS_ClinicalGuidelines/ClinicalGuidelines/GetList",
			method: "get",
			params,
			port
		})
	},
	//临床指南 根据is_pdf字段显示HTML
	Cguidelines(params) {
		return request({
			url: "/YBS_ClinicalGuidelines/ClinicalGuidelines/Cguidelines",
			method: "get",
			params,
			port
		})
	},
	//临床药学根据is_pdf返回PDF地址
	PdfCGuidelines(params) {
		return request({
			url: "/YBS_ClinicalGuidelines/ClinicalGuidelines/PdfCGuidelines",
			method: "get",
			params,
			port
		})
	}
};
