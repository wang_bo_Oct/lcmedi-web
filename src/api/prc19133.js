import request from '@/utils/request'
import { prc19133 } from './systemport.js'
const port = prc19133
// export default {
// // 点评中心
//   // 数据中心==============开始=======================
//   // 合理性指标
//   // 点评中心 / 数据中心 / 合理性指标 / 门(急)诊处方指标
//   ind_recip(data) {
//     return request({
//       url: '/comment/data/ind_recipe',
//       method: 'POST',
//       data,
//       port
//     })
//   }
// // 数据中心==============结束=======================
// }

export function deptMoneyRankingApi(data) {
  return request({
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    port,
    url: '/webroot/ReportServer?reportlet=deptMoneyRanking.cpt',
    method: 'post',
    data
  })
}

