import request from '@/utils/request'
import { ptc } from "./systemport.js";
const port = ptc;
export default {
	//登录
	login(params) {
		return request({
			url: '/admin/admin-user/login',
			method: 'post',
			params,
			port
		})
	},
	//医院列表
	hospital(params) {
		return request({
			url: '/core/hospital/all',
			method: 'post',
			params,
			port
		})
	},
	//注册
	regist(params) {
		return request({
			url: '/core/register',
			method: 'post',
			params,
			port
		})
	},
	//查询医生
	searchDoctor(params) {
		return request({
			url: '/core/phar/order/searchdoctor-json',
			method: 'post',
			params,
			port
		})
	},
	//查询药品
	searchDrug(params) {
		return request({
			url: '/core/phar/order/searchdrug-json',
			method: 'post',
			params,
			port
		})
	},
	//查询药师
	searchPhr(params) {
		return request({
			url: '/core/phar/order/searchphr-json',
			method: 'post',
			params,
			port
		})
	},
	//查询科室
	searchDept(params) {
		return request({
			url: '/core/phar/order/searchdept-json',
			method: 'post',
			params,
			port
		})
	},
	//门诊查询，住院查询列表
	outpatientSearch(params) {
		return request({
			url: '/core/phar/order/search-json',
			method: 'post',
			params,
			port
		})
	},
	//门诊科室列表,住院科室列表
	listPharJson(params) {
		return request({
			url: '/core/phar/order/list-phar-json',
			method: 'post',
			params,
			port
		})
	},
	//药师负责的科室列表
	pharDept(params) {
		return request({
			url: '/core/phar/order/list-phar-dept-json',
			method: 'post',
			params,
			port
		})
	},
	//给一个医师增加一个科室
	addDeptToPhar(params) {
		return request({
			url: '/core/phar/order/add-dept-to-phar',
			method: 'post',
			params,
			port
		})
	},
	//给一个医师删除一个科室
	delDeptToPhar(params) {
		return request({
			url: '/core/phar/order/cancel-dept-to-phar',
			method: 'post',
			params,
			port
		})
	},
	//给一个医师清空一个科室
	phrClean(params) {
		return request({
			url: '/core/phar/order/phrClean',
			method: 'post',
			params,
			port
		})
	},
	//编辑药师
	pharEdit(params) {
		return request({
			url: '/core/admin/userAdmin/edit-phar',
			method: 'post',
			params,
			port
		})
	},
	//编辑药师提交
	pharEditSave(params) {
		return request({
			url: '/core/admin/userAdmin/edit-phar-save',
			method: 'put',
			params,
			port
		})
	},
	//门诊科室分配他人-药师列表
	pharJsonOther(params) {
		return request({
			url: '/core/phar/order/list-phar-json-other',
			method: 'post',
			params,
			port
		})
	},
	//将本药师赋值给别的药师
	myToOther(params) {
		return request({
			url: '/core/phar/order/myToOther',
			method: 'post',
			params,
			port
		})
	},
	//将别的药师赋值给本药师
	otherToMy(params) {
		return request({
			url: '/core/phar/order/otherToMy',
			method: 'post',
			params,
			port
		})
	},
	//门诊科室分配他人————住院科室
	mzFenDeptOther(params) {
		return request({
			url: '/core/phar/order/mz_fen_dept_other',
			method: 'post',
			params,
			port
		})
	},
	//修改模块的值——门诊检测标准、住院检测标准
	modifyVal(params) {
		return request({
			url: '/core/phar/set/modifyVal',
			method: 'post',
			params,
			port
		})
	},
	//修改模块的自定义审查是否打开——门诊检测标准、住院检测标准
	modifyCustom(params) {
		return request({
			url: '/core/phar/set/modifyCustom',
			method: 'post',
			params,
			port
		})
	},
	//修改模块的系统审查是否打开——门诊检测标准、住院检测标准
	modifySystem(params) {
		return request({
			url: '/core/phar/set/modifySystem',
			method: 'post',
			params,
			port
		})
	},
	//检测标准列表——门诊检测标准、住院检测标准
	mzStandJson(params) {
		return request({
			url: '/core/phar/set/mz_stand-json',
			method: 'post',
			params,
			port
		})
	},
	//将监测的信息恢复默认——门诊检测标准、住院检测标准
	mzStandDeafult(params) {
		return request({
			url: '/core/phar/set/mz_stand-deafult',
			method: 'post',
			params,
			port
		})
	},
	//审方报表-今日动态监测列表
	phrReportToday(params) {
		return request({
			url: '/core/phar/phrReport/today_json',
			method: 'post',
			params,
			port
		})
	},
	//审方报表-今日动态监测-导出XLS
	phrReportExcel(params) {
		return request({
			url: '/core/phar/phrReport/download-excel',
			method: 'post',
			params,
			port
		})
	},
	//审方报表-今日动态监测-门诊实时动态监测数据
	phrReportTodayMZ(params) {
		return request({
			url: '/core/phar/phrReport/today_MZ',
			method: 'get',
			params,
			port
		})
	},
	//审方报表-今日动态监测-门诊实时动态监测数据
	phrReportTodayZY(params) {
		return request({
			url: '/core/phar/phrReport/today_ZY',
			method: 'get',
			params,
			port
		})
	},
	//审方报表-干预情况汇总表-列表
	phrReportGYjson(params) {
		return request({
			url: '/core/phar/phrReport/gy-json',
			method: 'get',
			params,
			port
		})
	},
	//审方报表-干预情况汇总表导出
	gyDownloadExcel(params) {
		return request({
			url: '/core/phar/phrReport/gy-download-excel',
			method: 'get',
			params,
			port
		})
	},
	//处方通过状态统计表-列表
	passJson(params) {
		return request({
			url: '/core/phar/phrReport/pass-json',
			method: 'get',
			params,
			port
		})
	},
	//处方通过状态统计表-导出通过状态情况汇总表
	passDownloadExcel(params) {
		return request({
			url: '/core/phar/phrReport/pass-download-excel',
			method: 'get',
			params,
			port
		})
	},
	//门诊系统设置列表
	setMzSys(params) {
		return request({
			url: '/core/phar/set/mz_sys',
			method: 'get',
			params,
			port
		})
	},
	//门诊设置 - 查出科室数据
	setDeptJson(params) {
		return request({
			url: '/core/phar/set/dept-json',
			method: 'get',
			params,
			port
		})
	},
	//门诊设置 - 查出药品数量
	setDrugJson(params) {
		return request({
			url: '/core/phar/set/drug-json',
			method: 'get',
			params,
			port
		})
	},
	//门诊设置 - 查出疾病诊断的数据
	setDiseaseJson(params) {
		return request({
			url: '/core/phar/set/disease-json',
			method: 'get',
			params,
			port
		})
	},
	//门诊设置 - 查出医生数据
	setDoctorJson(params) {
		return request({
			url: '/core/phar/set/doctor-json',
			method: 'get',
			params,
			port
		})
	},
	//门诊设置 - 科室超时设置查询
	setDeptoutJson(params) {
		return request({
			url: '/core/phar/set/deptout-json',
			method: 'get',
			params,
			port
		})
	},
	//门诊设置 - 科室超时设置编辑
	setDeptoutEdit(params) {
		return request({
			url: '/core/phar/set/deptout-edit',
			method: 'get',
			params,
			port
		})
	},
	//门诊设置 - 保存
	mzSetSave(params) {
		return request({
			url: '/core/phar/set/saveinfo',
			method: 'post',
			data: params,
			port
		})
	},
	//门诊设置、住院设置 - 恢复默认
	setSaveDefaut(params) {
		return request({
			url: '/core/phar/set/savedefaut',
			method: 'post',
			params,
			port
		})
	},
	//住院设置 - 列表
	setZySys(params) {
		return request({
			url: '/core/phar/set/zy_sys',
			method: 'get',
			params,
			port
		})
	},
	//住院设置 - 保存
	setZySave(params) {
		return request({
			url: '/core/phar/set/zy_sys',
			method: 'post',
			params,
			port
		})
	},
	//门诊审方-门诊查询-查看
	pharOrderDefaultSeek(params) {
		return request({
			url: '/core/all_phar/order/order_detail',
			method: 'get',
			params,
			port
		})
	},
	//审方-门诊查询-查看历史版本
	orderDetailBanben(params) {
		return request({
			url: '/core/all_phar/order/order-detail-banben',
			method: 'get',
			params,
			port
		})
	},
	//门诊审方-门诊监控-待审查处方列表详情
	noexamOrderJson(params) {
		return request({
			url: 'core/all_phar/order/list-noexam-order-json-mz',
			method: 'post',
			params,
			port
		})
	},
	//住院审方-住院监控-待审查处方列表详情
	noexamOrderJsonZY(params) {
		return request({
			url: 'core/all_phar/order/list-noexam-order-json-zy',
			method: 'post',
			params,
			port
		})
	},
	//门诊审方-门诊监控-等待医生双签处方列表
	waitDoctorDouble(params) {
		return request({
			url: '/core/all_phar/order/list-wait-doctor-double-json',
			method: 'get',
			params,
			port
		})
	},
	//门诊审方-门诊监控-已拒绝处方详情
	refuseOrderJson(params) {
		return request({
			url: '/core/all_phar/order/list-refuse-order-json',
			method: 'get',
			params,
			port
		})
	},
	//门诊审方-门诊监控-药师已审核处方
	pharPassOrderJson(params) {
		return request({
			url: '/core/all_phar/order/list-phar-pass-order-json',
			method: 'post',
			params,
			port
		})
	},
	//门诊审方-门诊监控-查看详情
	noexamOrderDetail(params) {
		return request({
			url: '/core/all_phar/order/noexam-order-detail',
			method: 'post',
			params,
			port
		})
	},
	//门诊审方-查询门诊检测权限
	panduanDept(params) {
		return request({
			url: '/core/phar/order/panduanDept',
			method: 'get',
			params,
			port
		})
	},
	//门诊审方-查询门诊tab数量
	orderallCount(params) {
		return request({
			url: '/core/all_phar/order/list-exam-order-json-allCount',
			method: 'get',
			params,
			port
		})
	},
}