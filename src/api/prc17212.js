import request from '@/utils/request'
import { prc17212 } from './systemport.js'
const port = prc17212

// 获取药品
export function GetDrug(params) {
  return request({
    port,
    url: '/YBS_OutpComment/OutpComment/GetDrug',
    method: 'get',
    params
  })
}

// 获取医生
export function GetDoctor(params) {
  return request({
    port,
    url: '/YBS_OutpComment/OutpComment/GetDoctor',
    method: 'get',
    params
  })
}

// 获取给药途径
export function GetRoute(params) {
  return request({
    port,
    url: '/YBS_OutpComment/OutpComment/GetRoute',
    method: 'get',
    params
  })
}

// 获取自定义属性
export function GetDrugType(params) {
  return request({
    port,
    url: '/YBS_OutpComment/OutpComment/GetDrugType',
    method: 'get',
    params
  })
}

// 获取科室
export function GetDept(params) {
  return request({
    port,
    url: '/YBS_OutpComment/OutpComment/GetDept',
    method: 'get',
    params
  })
}

// 获取疾病
export function GetDisease(params) {
  return request({
    port,
    url: '/YBS_OutpComment/OutpComment/GetDisease',
    method: 'get',
    params
  })
}

// 获取手术
export function GetOperation(params) {
  return request({
    port,
    url: '/YBS_OutpComment/OutpComment/GetOperation',
    method: 'get',
    params
  })
}

// 获取手术类型
export function GetOperationType(params) {
  return request({
    port,
    url: '/YBS_OutpComment/OutpComment/GetOperationType',
    method: 'get',
    params
  })
}

// 获取医疗组
export function GetMedgroup(params) {
  return request({
    port,
    url: '/YBS_OutpComment/OutpComment/GetMedgroup',
    method: 'get',
    params
  })
}

// 药品类别
export function GetDrugDnc(params) {
  return request({
    port,
    url: '/YBS_OutpComment/OutpComment/GetDrugDnc',
    method: 'get',
    params
  })
}

// 筛选
export function GetPrescPatList(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/GetPrescPatList',
    method: 'post',
    params,
    port
  })
}

// 门诊点评 获取病人信息
export function GetPatient(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/GetPatient',
    method: 'get',
    params,
    port
  })
}

// 门诊点评 获取医嘱
export function GetOrders(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/GetOrders',
    method: 'get',
    params,
    port
  })
}

// 门诊点评 获取检验
export function GetLab(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/GetLab',
    method: 'get',
    params,
    port
  })
}

// 门诊点评 检查
export function GetExam(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/GetExam',
    method: 'get',
    params,
    port
  })
}

// 费用	get
export function GetCost(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/GetCost',
    method: 'get',
    params,
    port
  })
}

// 点评问题
export function GetProblemList(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/GetProblemList',
    method: 'get',
    params,
    port
  })
}

// 清空点评	get
export function DeleteProblem(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/DeleteProblem',
    method: 'get',
    params,
    port
  })
}

// 新增点评	get
export function GetRules(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/GetRules',
    method: 'get',
    params,
    port
  })
}

// 点评问题序列号id
export function GetCommentsID(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/GetCommentsID',
    method: 'get',
    params,
    port
  })
}
// 保存问题	post
export function SaveProblemList(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/SaveProblemList',
    method: 'post',
    params,
    port
  })
}
// 修改问题
export function UpProblemList(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/UpProblemList',
    method: 'get',
    params,
    port
  })
}

// 删除问题	get
export function DeleteProblemList(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/DeleteProblemList',
    method: 'get',
    params,
    port
  })
}

// 任务计划
export function GetSaveTaskList(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/GetSaveTaskList',
    method: 'get',
    params,
    port
  })
}

// 保存任务计划
export function SaveTaskList(params) {
  return request({
    method: 'post',
    url: '/YBS_OutpComment/OutpComment/SaveTaskList',
    data: { ...params },
    port
  })
}
// 删除任务计划
export function DeleteTaskList(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/DeleteTaskList',
    method: 'get',
    params,
    port
  })
}
// 获取任务计划值
export function GetTaskData(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/GetTaskData',
    method: 'get',
    params,
    port
  })
}
// 门诊点评 导出表格
// /YBS_OutpComment/OutpComment/ExcelPrescPatList
export function ExcelPrescPatList(params) {
  return request({
    url: '/YBS_OutpComment/OutpComment/ExcelPrescPatList',
    method: 'post',
    data: { ...params },
    port
  })
}

// 住院点评 筛选
export function GetOrdersPatList(params) {
  return request({
    url: '/YBS_InpComment/InpComment/GetOrdersPatList',
    method: 'post',
    params,
    port
  })
}

// 住院点评 病人
export function GetPatientInpComment(params) {
  return request({
    url: '/YBS_InpComment/InpComment/GetPatient',
    method: 'get',
    params,
    port
  })
}

// 住院点评 医嘱
export function GetOrdersInpComment(params) {
  return request({
    url: '/YBS_InpComment/InpComment/GetOrders',
    method: 'get',
    params,
    port
  })
}

// 住院点评 检验
export function GetLabInpComment(params) {
  return request({
    url: '/YBS_InpComment/InpComment/GetLab',
    method: 'get',
    params,
    port
  })
}
// 住院点评 检查
export function GetExamInpComment(params) {
  return request({
    url: '/YBS_InpComment/InpComment/GetExam',
    method: 'get',
    params,
    port
  })
}

// 住院点评 费用
export function GetCostInpComment(params) {
  return request({
    url: '/YBS_InpComment/InpComment/GetCost',
    method: 'get',
    params,
    port
  })
}

// 住院点评 点评问题
export function GetProblemListInpComment(params) {
  return request({
    url: '/YBS_InpComment/InpComment/GetProblemList',
    method: 'get',
    params,
    port
  })
}

//

// 保存任务计划
export function SaveInpTaskList(params) {
  return request({
    method: 'post',
    url: '/YBS_InpComment/InpComment/SaveTaskList',
    data: { ...params },
    port
  })
}

// 获取任务计划值
export function GetInpTaskData(params) {
  return request({
    url: '/YBS_OutpComment/InpComment/GetTaskData',
    method: 'get',
    params,
    port
  })
}
