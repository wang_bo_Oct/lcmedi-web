import request from "@/utils/request";
import { rad } from "./systemport.js";
const port = rad;
export default {
	//给药途径列表
	listRoute(params) {
		return request({
			url: "/core/dict/route/list-route",
			method: "get",
			params,
			port
		})
	},
	//给药途径列表
	saveDosageCommon(params) {
		return request({
			url: "/core/define-common/dosage/save-edit-common",
			method: "post",
			params,
			port
		})
	},
	//通用剂量自定义历史列表详情
	listDosageCommon(params) {
		return request({
			url: "/core/define-common/dosage/list-common-json",
			method: "post",
			params,
			port
		})
	},
	//通用量剂自定义历史列表删除操作
	delDosageCommon(params) {
		return request({
			url: "/core/define-common/dosage/delete-common",
			method: "post",
			params,
			port
		})
	},
	//查询未设置的药品数据
	listInter(params) {
		return request({
			url: "/core/define-common/inter/list-unselect-json",
			method: "post",
			params,
			port
		})
	},
	//查询已设置的相互作用药品信息
	listInterSelected(params) {
		return request({
			url: "/core/define-common/inter/list-selected-json",
			method: "post",
			params,
			port
		})
	},
	//保存设置的药品相互作用信息
	listInterSave(params) {
		return request({
			url: "/core/define-common/inter/save",
			method: "post",
			params,
			port
		})
	},
	//删除相互作用自定义药品
	listInterDel(params) {
		return request({
			url: "/core/define-common/inter/delete-common",
			method: "post",
			params,
			port
		})
	},
	//体外配伍自定义中药品查询
	listIv(params) {
		return request({
			url: "/core/define-common/iv/list-unselect-json",
			method: "post",
			params,
			port
		})
	},
	//查询已设置的体外配伍药品信息
	listIvSelected(params) {
		return request({
			url: "/core/define-common/iv/list-selected-json",
			method: "post",
			params,
			port
		})
	},
	//删除体外配伍自定义药品警示信息
	listIvDel(params) {
		return request({
			url: "/core/define-common/iv/delete",
			method: "post",
			params,
			port
		})
	},
	//保存设置的药品体外配伍信息
	listIvSave(params) {
		return request({
			url: "/core/define-common/iv/save",
			method: "post",
			params,
			port
		})
	},
	//(超适应症自定义)获取未设置的疾病列表数据
	listIndica(params) {
		return request({
			url: "/core/define-common/indica/list-disease-json",
			method: "post",
			params,
			port
		})
	},
	//（超适应自定义）查询已设置的疾病列表数据
	listIndicaSelected(params) {
		return request({
			url: "/core/define-common/indica/selected-json",
			method: "post",
			params,
			port
		})
	},
	//删除已设置的用超适应症自定义
	listIndicaDel(params) {
		return request({
			url: "/core/define-common/indica/delete",
			method: "post",
			params,
			port
		})
	},
	//（超适应症） 批量保存通用超适应症自定义信息
	listIndicaSave(params) {
		return request({
			url: "/core/define-common/indica/batch-save",
			method: "post",
			params,
			port
		})
	},
	//查询通用儿童用药自定义历史列表
	listPediatricSelected(params) {
		return request({
			url: "/core/define-common/pediatric/list-common-json",
			method: "post",
			params,
			port
		})
	},
	//批量删除通用儿童用药自定义
	listPediatricDel(params) {
		return request({
			url: "/core/define-common/pediatric/delete",
			method: "post",
			params,
			port
		})
	},
	//保存通用儿童用药自定义
	listPediatricSave(params) {
		return request({
			url: "/core/define-common/pediatric/save",
			method: "post",
			params,
			port
		})
	},
	//查询通用成人用药自定义历史列表
	listAdultSelected(params) {
		return request({
			url: "/core/define-common/adult/list-common-json",
			method: "get",
			params,
			port
		})
	},
	//批量删除通用成人用药自定义
	listAdultDel(params) {
		return request({
			url: "/core/define-common/adult/delete",
			method: "post",
			params,
			port
		})
	},
	//保存通用成人用药自定义
	listAdultSave(params) {
		return request({
			url: "/core/define-common/adult/save",
			method: "post",
			params,
			port
		})
	},
	//查询通用老人 妊娠 用药自定义历史列表
	listUnagespSelected(params) {
		return request({
			url: "/core/define-common/unagesp/list-common-json",
			method: "get",
			params,
			port
		})
	},
	//批量删除通用老人 妊娠 药物禁忌症 用药自定义
	listUnagespDel(params) {
		return request({
			url: "/core/define-common/unagesp/delete",
			method: "post",
			params,
			port
		})
	},
	//保存通用老人 妊娠 药物禁忌症 用药自定义
	listUnagespSave(params) {
		return request({
			url: "/core/define-common/unagesp/save",
			method: "post",
			params,
			port
		})
	},
	//查询通用性别用药自定义历史列表
	listSexSelected(params) {
		return request({
			url: "/core/define-common/sex/list-common-json",
			method: "get",
			params,
			port
		})
	},
	//批量删除通用性别用药自定义
	listSexDel(params) {
		return request({
			url: "/core/define-common/sex/delete",
			method: "post",
			params,
			port
		})
	},
	//保存通用性别用药自定义
	listSexSave(params) {
		return request({
			url: "/core/define-common/sex/save",
			method: "post",
			params,
			port
		})
	},
	//查询通用重复用药自定义列表
	listDuptherapySelected(params) {
		return request({
			url: "/core/define-common/duptherapy/list-selected-json",
			method: "get",
			params,
			port
		})
	},
	//批量删除通用重复用药自定义
	listDuptherapyDel(params) {
		return request({
			url: "/core/define-common/duptherapy/batch-delete",
			method: "post",
			params,
			port
		})
	},
	//查询通用不良反应自定义列表
	listModuleSelected(params) {
		return request({
			url: "/core/define-common/module/list-selected-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用不良反应 药物禁忌症 自定义列表
	listModuleSave(params) {
		return request({
			url: "/core/define-common/module/save",
			method: "get",
			params,
			port
		})
	},
	//批量删除通用不良反应自定义
	listModuleDel(params) {
		return request({
			url: "/core/define-common/module/delete-reaction",
			method: "post",
			params,
			port
		})
	},
	
	//批量删除通用给药途径自定义
	listDrugrouteDel(params) {
		return request({
			url: "/core/define-common/drugroute/delete",
			method: "post",
			params,
			port
		})
	},
	//保存通用给药途径自定义
	listDrugrouteSave(params) {
		return request({
			url: "/core/define-common/drugroute/save",
			method: "post",
			params,
			port
		})
	},
	//给药途径自定义中 选择给药途径
	listDrugrouteDrugroute(params) {
		return request({
			url: "/core/define-common/drugroute/list-unselect-json",
			method: "get",
			params,
			port
		})
	},
	//查询通用配伍浓度自定义列表
	listCompatibilitySelected(params) {
		return request({
			url: "/core/define-common/compatibility/list-common-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用配伍浓度自定义列表
	listCompatibilitySave(params) {
		return request({
			url: "/core/define-common/compatibility/save",
			method: "post",
			params,
			port
		})
	},
	//查询通用配伍浓度自定义列表 - 查询信息
	listCompatibilitySelectedEdit(params) {
		return request({
			url: "/core/define-common/compatibility/edit",
			method: "get",
			params,
			port
		})
	},
	//查询通用配伍浓度自定义列表 - 删除
	listCompatibilityDel(params) {
		return request({
			url: "/core/define-common/compatibility/delete",
			method: "get",
			params,
			port
		})
	},
	//查询通用抗菌药物三联自定义列表
	listAntiduptherapySelected(params) {
		return request({
			url: "/core/define-common/antiduptherapy/list-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用抗菌药物三联自定义列表
	listAntiduptherapySave(params) {
		return request({
			url: "/core/define-common/antiduptherapy/save",
			method: "post",
			params,
			port
		})
	},
	//查询通用围术期用药自定义列表
	listPreOperationSelected(params) {
		return request({
			url: "/core/define-common/preOperation/listFull_operate",
			method: "get",
			params,
			port
		})
	},
	//保存通用围术期用药自定义列表
	listPreOperationSave(params) {
		return request({
			url: "/core/define-common/preOperation/saveOperate",
			method: "post",
			params,
			port
		})
	},
	//查询通用哺乳用药自定义历史列表
	listBreastFeedingSelected(params) {
		return request({
			url: "/core/define-common/breastFeeding/listFull-breastFeed",
			method: "get",
			params,
			port
		})
	},
	//批量删除通用哺乳用药自定义
	listBreastFeedingDel(params) {
		return request({
			url: "/core/define-common/breastFeeding/delete",
			method: "post",
			params,
			port
		})
	},
	//保存通用哺乳用药自定义
	listBreastFeedingSave(params) {
		return request({
			url: "/core/define-common/breastFeeding/save",
			method: "post",
			params,
			port
		})
	},
	//查询通用给药途径自定义历史列表
	listDrugrouteSelected(params) {
		return request({
			url: "/core/define-common/drugroute/list-selected-json",
			method: "get",
			params,
			port
		})
	},
	//重复用药自定义中 选择药品名称
	listDuptherapyDrugroute(params) {
		return request({
			url: "/core/define-common/duptherapy/list-unselect-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用重复用药自定义
	listDuptherapyFeedingSave(params) {
		return request({
			url: "/core/define-common/duptherapy/save",
			method: "post",
			params,
			port
		})
	},
	//药物禁忌症自定义中 选择诊断名称  不良反应 选择诊断名称
	listModuleDrugroute(params) {
		return request({
			url: "/core/define-common/module/list-unselect-json",
			method: "get",
			params,
			port
		})
	},
	//药物禁忌症自定义中 列表
	listModuleSelected(params) {
		return request({
			url: "/core/define-common/module/list-selected-json",
			method: "get",
			params,
			port
		})
	},
	//药物禁忌症自定义中 删除
	listModuleDel(params) {
		return request({
			url: "/core/define-common/module/delete-taboo",
			method: "get",
			params,
			port
		})
	},
	//查询抗菌药品信息
	listDrugMatch(params) {
		return request({
			url: "/core/match/drug/list-drug-match-json",
			method: "get",
			params,
			port
		})
	},
	//刪除抗菌三联重复用药信息
	listAntiduptherapyDel(params) {
		return request({
			url: "/core/define-common/antiduptherapy/delete",
			method: "get",
			params,
			port
		})
	},
	//获取手术名称
	listPreOperation(params) {
		return request({
			url: "/core/define-common/preOperation/list-operation",
			method: "get",
			params,
			port
		})
	},
	//围术期用药删除操作
	listPreOperationDel(params) {
		return request({
			url: "/core/define-common/preOperation/delete_common_operate",
			method: "get",
			params,
			port
		})
	},
	//查询通用肝损害剂量自定义列表
	listliverDamageListFull(params) {
		return request({
			url: "/core/define-common/liverDamage/listFull",
			method: "get",
			params,
			port
		})
	},
	//保存通用肝损害剂量自定义
	listliverDamageSave(params) {
		return request({
			url: "/core/define-common/liverDamage/save",
			method: "post",
			params,
			port
		})
	},
	// 删除通用肝损害剂量自定义
	listliverDamageDelete(params) {
		return request({
			url: "/core/define-common/liverDamage/delete",
			method: "post",
			params,
			port
		})
	},
	// 编辑通用肝损害剂量自定义
	listliverDamageEdit(params) {
		return request({
			url: "/core/define-common/liverDamage/updateView",
			method: "post",
			params,
			port
		})
	},
	//查询通用肾损害剂量自定义列表
	listrenalDamageListFull(params) {
		return request({
			url: "/core/define-common/renalDamage/listFull",
			method: "get",
			params,
			port
		})
	},
	//保存通用肾损害剂量自定义
	listrenalDamageSave(params) {
		return request({
			url: "/core/define-common/renalDamage/save",
			method: "post",
			params,
			port
		})
	},
	// 删除通用肾损害剂量自定义
	listrenalDamageDelete(params) {
		return request({
			url: "/core/define-common/renalDamage/delete",
			method: "post",
			params,
			port
		})
	},
	// 编辑通用肾损害剂量自定义
	listrenalDamageEdit(params) {
		return request({
			url: "/core/define-common/renalDamage/updateView",
			method: "post",
			params,
			port
		})
	},
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//多样化-查询规则列表-剂量
	selectDosageList(params) {
		return request({
			url: "/core/define-freedom/dosage/list-dosage",
			method: "get",
			params,
			port
		})
	},
	//多样化-查询规则列表-相互作用
	selectInterList(params) {
		return request({
			url: "/core/define-freedom/inter/list-inter",
			method: "get",
			params,
			port
		})
	},
	//体外配伍-查询规则信息
	selectIvList(params) {
		return request({
			url: "/core/define-freedom/iv/list-iv",
			method: "get",
			params,
			port
		})
	},
	//超适应症-查询规则信息
	selectIndicaList(params) {
		return request({
			url: "/core/define-freedom/indica/list-indica",
			method: "get",
			params,
			port
		})
	},
	//儿童-查询规则信息
	selectPediatricList(params) {
		return request({
			url: "/core/define-freedom/pediatric/list-pediatric",
			method: "get",
			params,
			port
		})
	},
	//成人-查询规则信息
	selectAdultList(params) {
		return request({
			url: "/core/define-freedom/adult/list-adult",
			method: "get",
			params,
			port
		})
	},
	//老人、妊娠-查询规则信息
	selectUnagespList(params) {
		return request({
			url: "/core/define-freedom/unagesp/list-unagesp",
			method: "get",
			params,
			port
		})
	},
	//性别-查询规则信息
	selectSexList(params) {
		return request({
			url: "core/define-freedom/sex/list-sex",
			method: "get",
			params,
			port
		})
	},
	//给药途径-查询规则信息
	selectDrugrouteList(params) {
		return request({
			url: "/core/define-freedom/drugroute/list-drugroute",
			method: "get",
			params,
			port
		})
	},
	//重复用药-查询规则信息
	selectDuptherapyList(params) {
		return request({
			url: "/core/define-freedom/duptherapy/list-duptherapy",
			method: "get",
			params,
			port
		})
	},
	//药物禁忌症、不良反应-查询规则信息
	selectModuleList(params) {
		return request({
			url: "/core/define-freedom/module/list-module",
			method: "get",
			params,
			port
		})
	},
	//配物浓度-查询规则信息
	selectCompatibilityList(params) {
		return request({
			url: "/core/define-freedom/compatibility/list-compatibility",
			method: "get",
			params,
			port
		})
	},
	//抗菌药物-查询规则信息
	selectAntiduptherapyList(params) {
		return request({
			url: "/core/define-freedom/antiduptherapy/list-antiduptherapy",
			method: "get",
			params,
			port
		})
	},
	//哺乳-查询规则信息
	selectBreastFeedingList(params) {
		return request({
			url: "/core/define-freedom/breastFeeding/list-breast",
			method: "get",
			params,
			port
		})
	},
	//围术期-查询规则信息
	selectOperationList(params) {
		return request({
			url: "/core/define-freedom/Operation/list-operate",
			method: "get",
			params,
			port
		})
	},
	//肝损害-查询规则信息
	selectLiverDamageList(params) {
		return request({
			url: "/core/define-freedom/liverDamage/list-liver",
			method: "get",
			params,
			port
		})
	},
	//肾损害-查询规则信息
	selectRenalDamageList(params) {
		return request({
			url: "/core/define-freedom/renalDamage/list-renal",
			method: "get",
			params,
			port
		})
	},
	//多样化-查询所有科室
	selectDeptList(params) {
		return request({
			url: "/core/dict/dept/list-dept",
			method: "get",
			params,
			port
		})
	},
	//多样化-查询所有医生
	selectDoctorList(params) {
		return request({
			url: "/core/dict/doctor/list-doctor",
			method: "get",
			params,
			port
		})
	},
	//多样化-查询所有诊断信息
	selectDiseaseList(params) {
		return request({
			url: "/core/dict/disease/list-disease",
			method: "get",
			params,
			port
		})
	},
	//多样化-查看所有过敏源信息
	selectAllergenList(params) {
		return request({
			url: "/core/dict/allergen/list-json",
			method: "get",
			params,
			port
		})
	},
	//多样化-查看所有手术信息
	selectOperationList(params) {
		return request({
			url: "/core/dict/operation/list-operation",
			method: "get",
			params,
			port
		})
	},
	//多样化-查询所有检验
	selectLabitemList(params) {
		return request({
			url: "/core/dict/lab-item/list-lab-item",
			method: "get",
			params,
			port
		})
	},
	//多样化-查询所有给药途径
	selectRouteList(params) {
		return request({
			url: "/core/dict/route/list-route",
			method: "get",
			params,
			port
		})
	},
	//（剂量）多条件剂量自定义
	listDosageFreedom(params) {
		return request({
			url: "/core/define-freedom/dosage/list-freedom-json",
			method: "get",
			params,
			port
		})
	},
	//删除多条件剂量自定义历史信息
	delDosageFreedom(params) {
		return request({
			url: "/core/define-freedom/dosage/delete-freedom",
			method: "get",
			params,
			port
		})
	},
	//自由自定义详情
	detailDosageFreedom(params) {
		return request({
			url: "/core/define-freedom/dosage/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//新增多条件剂量自定设置信息
	saveDosageFreedom(params) {
		return request({
			url: "/core/define-freedom/dosage/save-edit-freedom",
			method: "get",
			params,
			port
		})
	},
	//多条件相互作用自定义历史列表
	listInterFreedom(params) {
		return request({
			url: "/core/define-freedom/inter/list-freedom-json",
			method: "get",
			params,
			port
		})
	},
	//删除多条件相互作用自定义历史信息
	delInterFreedom(params) {
		return request({
			url: "/core/define-freedom/inter/delete",
			method: "get",
			params,
			port
		})
	},
	//相互作用详情
	detailInterFreedom(params) {
		return request({
			url: "/core/define-freedom/inter/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//保存设置的药品相互作用信息
	saveInterFreedom(params) {
		return request({
			url: "/core/define-freedom/inter/save-freedom",
			method: "get",
			params,
			port
		})
	},
	//多条件体外配伍自定义列表
	listIvFreedom(params) {
		return request({
			url: "/core/define-freedom/iv/list-json",
			method: "get",
			params,
			port
		})
	},
	//新增多条件体外配伍自定义
	saveIvFreedom(params) {
		return request({
			url: "/core/define-freedom/iv/save",
			method: "get",
			params,
			port
		})
	},
	//删除体外配伍多条件自定义
	delIvFreedom(params) {
		return request({
			url: "/core/define-freedom/iv/delete",
			method: "get",
			params,
			port
		})
	},
	//体外配伍详情
	detailIvFreedom(params) {
		return request({
			url: "/core/define-freedom/iv/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//多条件查询已设置的疾病列表数据
	listIndicaFreedom(params) {
		return request({
			url: "/core/define-freedom/indica/list-json",
			method: "get",
			params,
			port
		})
	},
	//批量保存通用超适应症自定义信息
	saveIndicaFreedom(params) {
		return request({
			url: "/core/define-freedom/indica/save",
			method: "post",
			params,
			port
		})
	},
	//删除多条件超适应症信息
	delIndicaFreedom(params) {
		return request({
			url: "/core/define-freedom/indica/delete",
			method: "get",
			params,
			port
		})
	},
	//超适应症详情
	detailIndicaFreedom(params) {
		return request({
			url: "/core/define-freedom/indica/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//查询通用儿童用药自定义历史列表
	listPediatricFreedom(params) {
		return request({
			url: "/core/define-freedom/pediatric/list-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用儿童用药自定义
	savePediatricFreedom(params) {
		return request({
			url: "/core/define-freedom/pediatric/save",
			method: "post",
			params,
			port
		})
	},
	//删除多条件儿童用药自定义历史信息
	delPediatricFreedom(params) {
		return request({
			url: "/core/define-freedom/pediatric/delete",
			method: "get",
			params,
			port
		})
	},
	//儿童详情
	detailPediatricFreedom(params) {
		return request({
			url: "/core/define-freedom/pediatric/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//查询通用成人用药自定义历史列表
	listAdultFreedom(params) {
		return request({
			url: "/core/define-freedom/adult/list-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用成人用药自定义
	saveAdultFreedom(params) {
		return request({
			url: "/core/define-freedom/adult/save",
			method: "post",
			params,
			port
		})
	},
	//删除成人用药自定义
	delAdultFreedom(params) {
		return request({
			url: "/core/define-freedom/adult/delete",
			method: "get",
			params,
			port
		})
	},
	//成人详情
	detailAdultFreedom(params) {
		return request({
			url: "/core/define-freedom/adult/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//查询通用老人用药自定义历史列表
	listUnagespFreedom(params) {
		return request({
			url: "/core/define-freedom/unagesp/list-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用老人用药自定义
	saveUnagespFreedom(params) {
		return request({
			url: "/core/define-freedom/unagesp/save",
			method: "post",
			params,
			port
		})
	},
	//删除老人用药自定义
	delUnagespFreedom(params) {
		return request({
			url: "/core/define-freedom/unagesp/delete",
			method: "get",
			params,
			port
		})
	},
	//老人详情
	detailUnagespFreedom(params) {
		return request({
			url: "/core/define-freedom/unagesp/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//查询通用老人用药自定义历史列表
	listUnagespFreedom(params) {
		return request({
			url: "/core/define-freedom/unagesp/list-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用老人用药自定义
	saveUnagespFreedom(params) {
		return request({
			url: "/core/define-freedom/unagesp/save",
			method: "post",
			params,
			port
		})
	},
	//删除老人用药自定义
	delUnagespFreedom(params) {
		return request({
			url: "/core/define-freedom/unagesp/delete",
			method: "get",
			params,
			port
		})
	},
	//老人详情
	detailUnagespFreedom(params) {
		return request({
			url: "/core/define-freedom/unagesp/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//查询通用性别用药自定义历史列表
	listSexFreedom(params) {
		return request({
			url: "/core/define-freedom/sex/list-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用性别用药自定义
	saveSexFreedom(params) {
		return request({
			url: "/core/define-freedom/sex/save",
			method: "post",
			params,
			port
		})
	},
	//删除性别用药自定义
	delSexFreedom(params) {
		return request({
			url: "/core/define-freedom/sex/delete",
			method: "get",
			params,
			port
		})
	},
	//性别详情
	detailSexFreedom(params) {
		return request({
			url: "/core/define-freedom/sex/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//查询通用给药途径自定义历史列表
	listDrugrouteFreedom(params) {
		return request({
			url: "/core/define-freedom/drugroute/list-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用给药途径自定义
	saveDrugrouteFreedom(params) {
		return request({
			url: "/core/define-freedom/drugroute/save",
			method: "post",
			params,
			port
		})
	},
	//删除给药途径自定义
	delDrugrouteFreedom(params) {
		return request({
			url: "/core/define-freedom/drugroute/delete",
			method: "get",
			params,
			port
		})
	},
	//给药途径详情
	detailDrugrouteFreedom(params) {
		return request({
			url: "/core/define-freedom/drugroute/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//查询通用重复用药自定义历史列表
	listDuptherapyFreedom(params) {
		return request({
			url: "/core/define-freedom/duptherapy/list-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用重复用药自定义
	saveDuptherapyFreedom(params) {
		return request({
			url: "/core/define-freedom/duptherapy/save",
			method: "post",
			params,
			port
		})
	},
	//删除重复用药自定义
	delDuptherapyFreedom(params) {
		return request({
			url: "/core/define-freedom/duptherapy/delete",
			method: "get",
			params,
			port
		})
	},
	//重复用药详情
	detailDuptherapyFreedom(params) {
		return request({
			url: "/core/define-freedom/duptherapy/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//查询通用药物禁忌症自定义历史列表
	listModuleFreedom(params) {
		return request({
			url: "/core/define-freedom/module/list-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用药物禁忌症自定义
	saveModuleFreedom(params) {
		return request({
			url: "/core/define-freedom/module/save",
			method: "post",
			params,
			port
		})
	},
	//删除药物禁忌症自定义
	delModuleFreedom(params) {
		return request({
			url: "/core/define-freedom/module/delete",
			method: "get",
			params,
			port
		})
	},
	//药物禁忌症详情
	detailModuleFreedom(params) {
		return request({
			url: "/core/define-freedom/module/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//查询通用配物浓度自定义历史列表
	listCompatibilityFreedom(params) {
		return request({
			url: "/core/define-freedom/compatibility/list-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用配物浓度自定义
	saveCompatibilityFreedom(params) {
		return request({
			url: "/core/define-freedom/compatibility/save",
			method: "post",
			params,
			port
		})
	},
	//删除配物浓度自定义
	delCompatibilityFreedom(params) {
		return request({
			url: "/core/define-freedom/compatibility/delete",
			method: "get",
			params,
			port
		})
	},
	//配物浓度详情
	detailCompatibilityFreedom(params) {
		return request({
			url: "/core/define-freedom/compatibility/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//查询抗菌药三联自定义历史列表
	listAntiduptherapyFreedom(params) {
		return request({
			url: "/core/define-freedom/antiduptherapy/list-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用抗菌药三联自定义
	saveAntiduptherapyFreedom(params) {
		return request({
			url: "/core/define-freedom/antiduptherapy/save",
			method: "post",
			params,
			port
		})
	},
	//删除抗菌药三联自定义
	delAntiduptherapyFreedom(params) {
		return request({
			url: "/core/define-freedom/antiduptherapy/delete",
			method: "get",
			params,
			port
		})
	},
	//抗菌药三联详情
	detailAntiduptherapyFreedom(params) {
		return request({
			url: "/core/define-freedom/antiduptherapy/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//查询哺乳用药自定义历史列表
	listBreastFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/breastFeeding/listFull-breastFeed",
			method: "get",
			params,
			port
		})
	},
	//保存通用哺乳用药自定义
	saveBreastFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/breastFeeding/save",
			method: "post",
			params,
			port
		})
	},
	//删除哺乳用药自定义
	delBreastFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/breastFeeding/delete",
			method: "get",
			params,
			port
		})
	},
	//哺乳用药详情
	detailBreastFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/breastFeeding/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//查询围术期用药自定义历史列表
	listOperationFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/Operation/list-json",
			method: "get",
			params,
			port
		})
	},
	//保存通用围术期用药自定义
	saveOperationFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/Operation/save",
			method: "post",
			params,
			port
		})
	},
	//删除围术期用药自定义
	delOperationFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/Operation/delete",
			method: "get",
			params,
			port
		})
	},
	//围术期用药详情
	detailOperationFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/Operation/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//疾病列表
	liverDamageList(params) {
		return request({
			url: "/core/define-common/liverDamage/list-liverDamage",
			method: "get",
			params,
			port
		})
	},
	//查询肝损害自定义历史列表
	listLiverDamageFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/liverDamage/listFull",
			method: "get",
			params,
			port
		})
	},
	//保存通用肝损害自定义
	saveLiverDamageFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/liverDamage/save",
			method: "post",
			params,
			port
		})
	},
	//删除肝损害自定义
	delLiverDamageFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/liverDamage/delete",
			method: "get",
			params,
			port
		})
	},
	//肝损害详情
	detailLiverDamageFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/liverDamage/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//查询肾损害自定义历史列表
	listRenalDamageFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/renalDamage/listFull",
			method: "get",
			params,
			port
		})
	},
	//保存通用肾损害自定义
	saveRenalDamageFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/renalDamage/save",
			method: "post",
			params,
			port
		})
	},
	//删除肾损害自定义
	delRenalDamageFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/renalDamage/delete",
			method: "get",
			params,
			port
		})
	},
	//肾损害详情
	detailRenalDamageFeedingFreedom(params) {
		return request({
			url: "/core/define-freedom/renalDamage/freedom-detail",
			method: "get",
			params,
			port
		})
	},
	//肾损害--肾功能检验指标
	listRenalDamage(params) {
		return request({
			url: "/core/define-freedom/renalDamage/list-renalDamage",
			method: "get",
			params,
			port
		})
	},
	
	
	
	
	
	
	
	
	
	
	//查询检验申请项目信息 分页
	dictExamData(params) {
		return request({
			url: "/core/lcmedi/dict/exam-data",
			method: "get",
			params,
			port
		})
	},
	//分页查询检验申请信息
	dictLabData(params) {
		return request({
			url: "/core/lcmedi/dict/lab-data",
			method: "get",
			params,
			port
		})
	},
	//查看检验结果项目信息 分页
	dictLabItemData(params) {
		return request({
			url: "/core/lcmedi/dict/lab_item-data",
			method: "get",
			params,
			port
		})
	},
	//查询所有科室名称，分页
	dictListDeptData(params) {
		return request({
			url: "/core/dept/list-dept-json",
			method: "get",
			params,
			port
		})
	},
	//编辑是否门诊科室
	editMzks(params) {
		return request({
			url: "/core/dept/edit-mzks",
			method: "post",
			params,
			port
		})
	},
	//编辑是否住院科室
	editZyks(params) {
		return request({
			url: "/core/dept/edit-zyks",
			method: "post",
			params,
			port
		})
	},
	//编辑是否急诊科室
	editJzks(params) {
		return request({
			url: "/core/dept/edit-jzks",
			method: "post",
			params,
			port
		})
	},
	//编辑是否在用
	editSfzy(params) {
		return request({
			url: "/core/dept/edit-sfzy",
			method: "post",
			params,
			port
		})
	},
	//更新检验申请项目信息
	dictExamUpdata(params) {
	  return request({
	    url: "/core/lcmedi/dict/exam-updata",
	    method: "post",
	    params,
	    port
	  });
	},
	//编辑检验申请项目信息
	dictLabUpdata(params) {
	  return request({
	    url: "/core/lcmedi/dict/lab-updata",
	    method: "post",
	    params,
	    port
	  });
	},
	//检验项目结果数据修改
	dictLabItemUpdata(params) {
	  return request({
	    url: "/core/lcmedi/dict/lab_item-updata",
	    method: "post",
	    params,
	    port
	  });
	},
};
