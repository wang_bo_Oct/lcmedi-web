import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/zh-CN'

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

import Pagination from '@/components/Pagination'
Vue.component('Pagination', Pagination)

import Bluetable from '@/components/Bluetable'
Vue.component('Bluetable', Bluetable)

import Graytable from '@/components/Graytable'
Vue.component('Graytable', Graytable)

// 审方中心API
import PTC from '@/api/ptc'
Vue.prototype.$ptc = PTC

// 知识库API
import KB from '@/api/kb'
Vue.prototype.$kb = KB

// 合理用药API
import RAD from '@/api/rad'
Vue.prototype.$rad = RAD

// 临床药师工作站API
import CPW from '@/api/cpw'
Vue.prototype.$cpw = CPW

// 点评中心API
import PRC from '@/api/prc'
Vue.prototype.$prc = PRC

// 临床药师工作站API
import CUSTOM from '@/api/custom'
Vue.prototype.$custom = CUSTOM

// 用户中心API
import UC from '@/api/uc'
Vue.prototype.$uc = UC

import onlyNumber from '@/components/el-input'
Vue.use(onlyNumber);

// Vue.use(ElementUI, { locale })
// // 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI)

// 引入echart
import echarts from 'echarts'

Vue.use(echarts)

import $utils from '@/utils/common.js'
Vue.prototype.$utils = $utils

Vue.config.productionTip = true

//编辑表格
import XEUtils from 'xe-utils'
import VXETable from 'vxe-table'
import 'vxe-table/lib/index.css'
VXETable.formats.mixin({
  // 格式化下拉选项
  formatSelect({ cellValue }, list) {
    const item = list.find(item => item.value === cellValue)
    return item ? item.label : ''
  },
  // 格式化日期，默认 yyyy-MM-dd HH:mm:ss
  formatDate({ cellValue }, format) {
    return XEUtils.toDateString(cellValue, format || 'yyyy-MM-dd HH:mm:ss')
  },
  // 格式金额，默认2位数
  formatAmount({ cellValue }, digits) {
    return XEUtils.commafy(cellValue, { digits: digits || 2 })
  },
  // 格式化银行卡，默认每4位隔开
  formatBankcard({ cellValue }) {
    return XEUtils.commafy(cellValue, { spaceNumber: 4, separator: ' ' })
  },
  // 四舍五入,默认两位数
  formatFixedNumber({ cellValue }, digits) {
    return XEUtils.toNumber(cellValue).toFixed(digits || 2)
  },
  // 截取小数,默认两位数
  formatCutNumber({ cellValue }, digits) {
    return XEUtils.toFixedString(cellValue, digits || 2)
  },
  // 转换 moment 类型为字符串
  toMomentString({ cellValue }, format) {
    return cellValue ? cellValue.format(format) : ''
  }
})

Vue.use(VXETable)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
