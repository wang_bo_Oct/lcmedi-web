export default {
	sexFormat(id){
		if(id == 1){
			return "男"
		}else if(id== 2){
			return "女"
		}
	},
	timeFormat(date){
		if(date.toString().length == 10){
			date=date*1000
		}
		const nDate=new Date(date);
		const year=nDate.getFullYear();
		const month=(nDate.getMonth()+1)>10?(nDate.getMonth()+1):("0"+(nDate.getMonth()+1));
		const day=nDate.getDate()>10?nDate.getDate():("0"+nDate.getDate());
		const hour=nDate.getHours()>10?nDate.getHours():("0"+nDate.getHours());
		const min=nDate.getMinutes()>10?nDate.getMinutes():("0"+nDate.getMinutes());
		const sec=nDate.getSeconds()>10?nDate.getSeconds():("0"+nDate.getSeconds());
		return  year+"-"+month+"-"+day+" "+hour+":"+min+":"+sec;
	},
	departSFormat(data){//门诊科室 状态转化
		if(data == 0){
			return "禁止使用"
		}else if(data== 1){
			return "正常使用"
		}
	},
	prescriptionStatus(){
		return [
			{value:20,label:"药师直接通过"},
			{value:21,label:"药师审核拒绝医生编辑后通过"},
			{value:22,label:"药师双签不复审医生编辑通过"},
			{value:23,label:"药师双签复审医生编辑通过"},
			{value:24,label:"双签复审通过"},
			{value:25,label:"双签不复审通过"},
			{value:30,label:"药师超时自动审核通过"},
			{value:40,label:"医生超时自动审核通过"},
			{value:49,label:"系统审查通过"},
			{value:61,label:"自动干预通过"},
			{value:62,label:"干预功能关闭通过"}
		]
	},
	isNumber(num){
	    var regPos = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/; // 非负整数
	    var regNeg = /^((-\d+(\.\d+)?)|(0+(\.0+)?))$/; // 负整数
	    if(regPos.test(num) || regNeg.test(num)){
	        return true;
	    }else{
	        return false;
	    }
	}
};